import path from 'path';
import ejs from 'ejs';
import { IConfigEmailData, IEmail, IMailTemplate, TConfigEmail } from '@type/email';
import sgMail from '@sendgrid/mail';

const configDataMail = ({ mailDir, mailContent }: { mailDir: string, mailContent: Record<string, string | number> }): IMailTemplate<Record<string, string | number>> => {
    return {
        dir: mailDir,
        fields: mailContent,
    }
};

const mailTemplate = (dataConfig: IConfigEmailData): any => {
    const { mailDir, mailContent } = dataConfig;

    const mailConfig = configDataMail({ mailDir, mailContent });

    const { dir, fields } = mailConfig;

    return ejs.renderFile(dir, { image: process.env.URL_LOGO, ...fields });
};

const emailConfig = async (data: IConfigEmailData): Promise<TConfigEmail> => {
    const { subject, user } = data;
    const template = await mailTemplate(data);

    return {
        to: user.email,
        subject: subject,
        text: subject,
        html: template,
    }
};

export const sendMail = async (data: IConfigEmailData): Promise<void> => {
    try {
        const configEmail: TConfigEmail = await emailConfig(data);

        const { to, subject, html, text } = configEmail;
        const from = process.env.MAIL_USERNAME ?? '';
        const msg: IEmail = { to, from, subject, text, html };

        sgMail.setApiKey(process.env.SENDGRID_API_KEY ?? '');

        await sgMail.send(msg);
    } catch (e) {
        console.log(e);
        //console.log(e.response.body);
    }
};
