import app from './app';
const fs = require('fs');
const PATH_SYSTEM_CONFIG = `./assets/config/system_config.json`;;


let rawdata = fs.readFileSync(PATH_SYSTEM_CONFIG);
const config = JSON.parse(rawdata);
app.listen(config.port);
console.log('Server is running on ', config.port);