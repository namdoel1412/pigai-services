import { IFarm } from "@components/farm/farm.types";
import Farm from './../components/farm/farm.model';

class FarmRepo{
    static createDefaultFarm = async function(data: IFarm, session?: any) {
        return await new Farm(data).save({session});
    }
}

export default FarmRepo;