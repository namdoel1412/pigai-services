import express from 'express';
//import routes from './routes';
import fs from 'fs';
import './components/user/user.controller';
import './components/pig/pig.controller';
import './components/farm/farm.controller';
import './components/eventType/evenType.controller';
import './components/event/event.controller';
import './components/healthStatus/healthStatus.controller';
import './components/pen/pen.controller';
import './components/weightType/weightType.controller';
import './components/behaviour/behaviour.controller';
import './components/camera/camera.controller';
import './components/food/foodSupplier/foodSupplier.controller';
import './components/food/foodType/foodType.controller';
import './components/food/foodUnit/foodUnit.controller';
import './components/food/food/food.controller';
import './components/food/foodOrder/foodOrder.controller';
import './components/food/foodOrderItems/foodOrderItem.controller';
import './components/food/foodConsume/foodConsume.controller';
import './components/food/foodConsumeItem/foodConsumeItem.controller';
import './components/food/penFoodConsume/penFoodConsume.controller';
import './components/food/penFoodConsumeItem/penFoodConsumeItem.controller';
import './components/food/foodExportation/foodExportation.controller';
import './components/food/foodInventory/foodInventory.controller';
import { RegisterRoutes } from './routes';
import swaggerUi from 'swagger-ui-express';
import mongoose from 'mongoose';
import cors from 'cors';
import path from 'path';
import passport from 'passport';
import handleError from './middleware/error';
import Logger from './middleware/logger';
import { authentication } from './middleware/authentication';


const app = express();
mongoose.connect(process.env.MONGO_DB ?? '', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
    user: process.env.MONGO_USER,
    pass: process.env.MONGO_PASS,
    dbName: process.env.MONGO_DB_NAME,
}).then().catch(err => { console.log({ err }) });
app.use(cors());

const isProductionMode = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';
const publicDir = isProductionMode ? '/v2/public' : '/public';
const staticDir = isProductionMode ? './public' : '../public';

/* Swagger files start */
const swaggerFile: any = (process.cwd()+"/swagger.json");
const swaggerData: any = fs.readFileSync(swaggerFile, 'utf8');
const swaggerDocument = JSON.parse(swaggerData);
/* Swagger files end */

app.use(publicDir, express.static(path.join(__dirname, staticDir)));
//app.set('view engine', ejs);

app.use(passport.initialize());
app.use(passport.session());

app.use(express.json());
// app.use(routers);

// app.use(handleError);
// app.use(Logger);
// app.use(authentication);

RegisterRoutes(app);
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

export default app;