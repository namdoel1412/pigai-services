import mongoose, { Model, Document } from 'mongoose';
import { IPigWeightType } from './weightType.types';

interface WeightTypeDocument extends IPigWeightType, Document { }
interface WeightTypeModel extends Model<WeightTypeDocument> { }

const weightTypeSchema = new mongoose.Schema<WeightTypeDocument, WeightTypeModel>({
    name: {
        type: String,
        required: true
    },
    min: Number,
    max: Number,
})

weightTypeSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {   delete ret._id  }
});

export default mongoose.model<WeightTypeDocument, WeightTypeModel>('WeightType', weightTypeSchema);
