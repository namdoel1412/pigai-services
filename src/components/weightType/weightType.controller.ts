import { failedResponse, successResponse } from '../../utils/http';
import WeightType from './weightType.model';
import { getAllWeightTypeQuery } from './weightType.queries';
import {Body, Controller, Get, Post, Put, Query, Route, Tags} from 'tsoa';
import {IPigWeightType} from './weightType.types';

@Route('weightTypes')
@Tags('WeightType')
export class WeightTypeController extends Controller{
    @Get('?')
    public async getListWeighType(@Query() name?: string): Promise<any>{
        try{
            const result = await WeightType.aggregate(getAllWeightTypeQuery(name));
            return successResponse(result);
        }
        catch (err) {
            this.setStatus(500);
            return failedResponse(`Error ${err}`, 'ServiceException');
        }
    }

    /**
     * Create weight type
     * @param data
     */
    @Post('/')
    public async create(@Body() data: IPigWeightType): Promise<any>{
        try{
            if (await isExistWeightType(data)) {
                this.setStatus(400);
                return failedResponse('Tên loại heo đã tồn tại', 'UniqueName');
            }
            await new WeightType(data).save();
            return successResponse('insertSuccess');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Update weight type
     * @param weightTypeId
     * @param data
     */
    @Put('/{weightTypeId}')
    public async update(weightTypeId: string, @Body() data: IPigWeightType): Promise<any>{
        try{
            if (await isExistWeightType(data)) {
                this.setStatus(400);
                return failedResponse('Tên loại heo đã tồn tại', 'UniqueName');
            }
            const weightType = await WeightType.findByIdAndUpdate(weightTypeId, data);
            if (!weightType) {
                this.setStatus(404);
                return failedResponse('Không tìm thấy loại heo', 'WeightTypeNotFound');
            }
            return successResponse('updateSuccess');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }
}

const isExistWeightType = async (input: IPigWeightType): Promise<boolean> => {
    const { name } = input;
    return (await WeightType.find({ name })).length > 0;
}