export enum EPigWeight {
    UNDER_30 = 'Từ khi nhập chuồng đến 30 kg',
    FROM_30_T0_60 = 'Từ 30kg đến 60kg',
    OVER_60 = 'Trên 60 kg'
}

export interface IPigWeightType {
    name: string
    min?: number
    max?: number
}
