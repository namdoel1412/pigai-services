export const getAllWeightTypeQuery = (name: string): any => [
    {
        $match: {
            name: new RegExp(name, 'g'),
        }
    },
    {
        $sort: { name: 1 },
    },
    {
        $project: {
            _id: 0,
            id: '$_id',
            name: 1,
            min: 1,
            max: 1,
        }
    }
];

export const lookupObject = (collectionName: string, foreignName: string, customPipline: any): any => ({
    $lookup: {
        from: collectionName,
        let: { [foreignName]: `$${ foreignName }` },
        as: collectionName,
        customPipline
    }
});

export const lookupObjectWeightTypes = (collectionName: string, foreignName: string): any => {
    let pipeline = {
        pipeline: [
            {
                $match: { $expr: { $in: [ '$_id', `$$${ foreignName }` ] } }
            },
            dataResponseProject
        ]
    }
    lookupObject(collectionName, foreignName, pipeline);
};

export const lookupSingleObjectWeightType = (): any => (
    [
        {
            $lookup: {
                from: 'weighttypes',
                let: { weightTypeId: '$weightTypeId' },
                as: 'weighttype',
                pipeline: [
                    {
                        $match: {
                            $expr: { $eq: [ '$_id', '$$weightTypeId' ] }
                        }
                    },
                    dataResponseProject
                ]
            }
        },
        {
            $unwind: {
                path: '$weighttype',
                preserveNullAndEmptyArrays: true
            } 
        }
    ]
);

const dataResponseProject = {
    $project: {
        _id: 0,
        id: '$_id',
        name: 1
    }
};
