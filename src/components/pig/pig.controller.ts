import Pig from './pig.model';
import { successResponse, failedResponse, CustomRequest, BaseResponseType } from '../../utils/http';
import {
    ICreatePigDTO,
    IGetPigDTO,
    ILstPigPagination,
    IUpdatePig,
    IUpdatePigs
} from './pig.types';
import { Controller, Route, Get, Post, BodyProp, Put, Delete, Tags, Body, Security, Header, Query } from 'tsoa';
// import {insertEventsWhenUpdatePigs} from "../event/event.controller";
import HealthStatus from '../healthStatus/healthStatus.model';
import Behaviour from '../behaviour/behaviour.model';
import WeightType from '../weightType/weightType.model';
import Farm from '../farm/farm.model';
import Pen from '../pen/pen.model';
import {
    detailPigQueryByFarmId,
    detailPigQueryByPenId,
    detailPigQueryByPigId,
} from './pig.queries';
import Event from '../event/event.model';
import EventType from '../eventType/eventType.model'
import {detailEventByPigId, getEventByNameEventType} from '../event/event.queries';
import {EHealthStatus} from '../healthStatus/healthStatus.types';
import mongoose from 'mongoose';
import {EEventType} from "../eventType/eventType.types";
import {EEventStatus} from "../event/event.types";
import {searchByArrayName} from "../eventType/evenType.controller";
import {convertFormatDate} from "../../utils/collection";

@Route('/pigs')
@Tags('Pig')
export class PigController extends Controller{

    /**
     * Get all pigs
     */
    // @Security("jwt")
    @Get('/')
    public async getAll(): Promise<any>{
        try {
            let items = await Pig.find({}).exec();
            return successResponse(items);
        } catch (err) {
            return failedResponse(`Caught error ${err}`, '500')
        }
    }

    /**
     * Get pig's detail by pigId
     */
    // @Security("jwt")
    @Get('/{pigId}')
    public async getPigDetailByPigId(pigId: string): Promise<any>{
        try {
            const result = await Pig.aggregate(detailPigQueryByPigId(pigId));
            if(!result){
                this.setStatus(404);
                return failedResponse(`Pig not found`, 'NotFound');
            }
            const behaviour = await Behaviour.findOne({})
            .where('pigId').equals(pigId)
            .exec();
            // const tmp: IGetPigDTO = {
            //     id: result[0].id,
            //     dateOfBirth: result[0].dateOfBirth,
            //     healthStatus: result[0].healthStatus,
            //     pigAIId: result[0].pigAIId,
            //     penId: result[0].penId,
            //     weightType: result[0].weightType,
            //     size: result[0].size,
            //     weight: result[0].weight,
            //     temperature: result[0].temperature,
            //     farmId: result[0]?.farmId
            // };
            const events = await Event.aggregate(detailEventByPigId(mongoose.Types.ObjectId(result[0].id)));
            const res = { ...result[0], behaviour: behaviour, events: events}
            return successResponse(res);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Caught error ${err}`, '500');
        }
    }   

     /**
     * Get pig's detail by pigId
     */
    // @Security("jwt")
    @Get('/pigAIId/{pigAIId}')
    public async getPigDetailByPigAIId(pigAIId: string): Promise<any>{
        try {
            const pig = await Pig.findOne({})
            .where('pigAIId').equals(pigAIId)
            .exec();
            if(!pig){
                this.setStatus(404);
                return failedResponse('Không tìm thấy dữ liệu', 'PigNotFound');
            }
            let healthStatus, weightType, farm, pen;
            pig.healthStatus && (healthStatus = await HealthStatus.findOne({}).where('_id').equals(pig.healthStatus).exec());
            pig.weightTypeId && (weightType = await WeightType.findOne({}).where('_id').equals(pig.weightTypeId).exec());
            pig.farmId && (farm = await Farm.findOne({}).where('_id').equals(pig.farmId).exec());
            pig.penId && (pen = await Pen.findOne({}).where('_id').equals(pig.penId).exec());
            const behaviour = await Behaviour.findOne({})
            .where('pigId').equals(pig.id)
            .exec();
            const tmp: IGetPigDTO = {
                id: pig.id,
                dateOfBirth: pig.dateOfBirth,
                healthStatus: healthStatus,
                pigAIId: pig.pigAIId,
                penId: pen,
                weightTypeId: weightType,
                size: pig.size,
                weight: pig.weight,
                temperature: pig.temperature,
                farmId: farm
            };
            const res: IGetPigDTO = { ...tmp, behaviour: behaviour}
            return successResponse(res);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Caught error ${err}`, '500');
        }
    }  

    /**
     * Create pig
     * @param pigAIIds
     */
    // @Security("jwt")
    @Post()
    public async insertPigs(@Body() data: ICreatePigDTO): Promise<any>{
        try {
            const { pigAIIds, penId, farmId } = data;
            const dataInsert = pigAIIds.map(v => ({ pigAIId: v, penId: penId, farmId: farmId }));
            const pigAIIdExist = await validPigAiId(pigAIIds, penId);
            if (pigAIIdExist.length !== 0 ){
                return failedResponse(pigAIIdExist, 'PigAIIdExist')
            }
            await Pig.insertMany( dataInsert);
            return successResponse('insertSuccess');
        }
        catch (err){
            this.setStatus(500);
            return failedResponse(`Caught error ${err}`, '500');
        }
    }

    /**
     * Update Pig
     * @param lstPigs
     */
    // @Security("jwt")
    @Put()
    public async updatePigs(@Body() lstPigs: IUpdatePigs): Promise<BaseResponseType<any>>{
        const session = await Pig.startSession();
        session.startTransaction();
        try {
            const { pigAIIds, userId, farmId, data } = lstPigs;
            const { penId } = lstPigs.data;
            data.dateOfBirth = convertFormatDate(data.dateOfBirth);
            data.dateIn = convertFormatDate(data.dateIn);
            data.pigOutPrediction = convertFormatDate(data.pigOutPrediction);
            data.changePenPrediction = convertFormatDate(data.changePenPrediction);
            if (data.dateOfBirth.length === 0 || data.dateIn.length === 0 || data.pigOutPrediction.length === 0 || data.changePenPrediction.length === 0){
                return failedResponse('Không đúng định dạng dd/mm/yyyy hoặc dd-mm-yyyy hoặc yyyy-mm-dd', 'IncorrectDateFormat');
            }

            const healthStatusData = await HealthStatus.find({ name: EHealthStatus.NORMAL});
            data.healthStatus = healthStatusData[0].id;

            if (!pigAIIds || pigAIIds.length === 0 ) {
                return failedResponse('Trường này bắt buộc', 'RequiredField');
            }

            const pen = await Pen.findById(penId);
            if (!pen){
                return failedResponse('Không tìm thấy chuồng', 'PenNotFound');
            }
            await Pen.updateOne({ _id: penId }, data, {session});

            const pigs = await Pig.find({ pigAIId: { $in: pigAIIds }, farmId: farmId });
            if (!pigs[0]){
                return failedResponse('Không tìm thấy cá thể', 'PigNotFound');
            }
            const pigIds = pigs.map(v => v._id);
            await Pig.updateMany({ pigAIId: { $in: pigAIIds }, farmId: farmId }, data, { upsert: true, session });

            const evenTypes = await searchByArrayName([ EEventType.pigIn ]);
            const dataEvent = {
                farmIds: farmId,
                pigIds,
                penIds: penId,
                startDate: data.dateIn,
                endDate: data.dateIn,
                eventTypeId: evenTypes[0].id,
                name: evenTypes[0].name,
                status: EEventStatus.Done,
                userId
            }
            await new Event(dataEvent).save({session});

            // await insertEventsWhenUpdatePigs({ farmId, pigIds, pigOutPrediction, changePenPrediction, penId, dateIn, userId });
            await session.commitTransaction();
            session.endSession();
            return successResponse('update success');
        }
        catch (err){
            await session.abortTransaction();
            session.endSession();
            this.setStatus(500)
            return failedResponse(`Caught error ${err}`, '500')
        }
    }

    /**
     * Delete pig
     */
    // @Security("jwt")
    @Delete("")
    public async removePigs(@Body() lstPigs: string[]): Promise<any>{
        try{
            await Pig.deleteMany({ pigAIId: { $in: lstPigs } });
            return successResponse(true);
        }
        catch (err){
            this.setStatus(500);
            return failedResponse(`Caught error ${err}`, '500');
        }
    }

    /**
     * Get pigs in a pen by penId.
     */
    // @Security("jwt")
    @Get('penId/{penId}')
    public async getPigsByPenId(penId: string, @Query() offset?: number, @Query() size?: number): Promise<any>{
        try {
            let data: ILstPigPagination = {
                offset : offset || 0,
                size: size || 10
            }
            const result = await Pig.aggregate(detailPigQueryByPenId(penId, data));
            return successResponse(result[0]);
        }
        catch (err) {
            this.setStatus(500);
            return failedResponse(`Caught error ${err}`, '500');
        }
    }

    @Get('farmId/{farmId}')
    public async getPigsByfarmId(farmId: string, @Query() offset?: number, @Query() size?: number, @Query() isIdentityConfirm: boolean = true): Promise<any>{
        try {
            let data: ILstPigPagination = {
                offset : offset || 0,
                size: size || 10
            }
            const result = await Pig.aggregate(detailPigQueryByFarmId(farmId, data, isIdentityConfirm));
            let res: any[] = [];
            let len = result[0].items.length;
            for(let i = 0; i < len; i++){
                const element = result[0].items[i];
                const events = await Event.aggregate(detailEventByPigId(element.id));
                const nhapChuongEvent = events.find((item) => item?.eventType?.name === 'Nhập chuồng');
                res.push({...element, event: nhapChuongEvent});
            }
            return successResponse({items: res, total: result[0].total});
        }
        catch (err) {
            this.setStatus(500);
            return failedResponse(`Caught error ${err}`, '500');
        }
    }


    /**
     * update a pig
     * @param id
     * @param data
     */
    @Put('/{id}')
    public async updatePig (id: string, @Body() data: IUpdatePig): Promise<any> {
        const session = await Pig.startSession();
        session.startTransaction();
        try {
            const pig = await Pig.findById(id);
            if (!pig){
                return failedResponse('Không tìm thấy cá thể', 'PigNotFound');
            }
            if (data.dateOfBirth){
                data.dateOfBirth = convertFormatDate(data.dateOfBirth);
                    if (data.dateOfBirth.length === 0 ){
                        return failedResponse('Không đúng định dạng dd/mm/yyyy hoặc dd-mm-yyyy hoặc yyyy-mm-dd', 'IncorrectDateFormat');
                    }
            }

            await Pig.updateOne({ _id: id }, data, { session });

            if (data.dateIn){
                data.dateIn = convertFormatDate(data.dateIn);
                if (data.dateIn.length === 0 ){
                    return failedResponse('Không đúng định dạng dd/mm/yyyy hoặc dd-mm-yyyy hoặc yyyy-mm-dd', 'IncorrectDateFormat');
                }
                const event = await Event.aggregate(getEventByNameEventType(mongoose.Types.ObjectId(id), EEventType.pigIn));
                if (event.length !== 0){
                    await Event.updateOne({_id: event[0].id},{ $pull: { pigIds: mongoose.Types.ObjectId(id) }});
                }
                const dataInsertEvent = {
                    userId: event[0].userId,
                    name: event[0].name,
                    farmIds: pig.farmId,
                    penIds: pig.penId,
                    pigIds: id,
                    eventTypeId: event[0].eventType.id,
                    startDate: data.dateIn,
                    endDate: data.dateIn,
                    status: EEventStatus.Done,
                }

                await new Event(dataInsertEvent).save({session});
            }
            await session.commitTransaction();
            session.endSession();
            return successResponse('update success');
        } catch (err) {
            await session.abortTransaction();
            session.endSession();
            this.setStatus(500);
            return failedResponse(`Caught error ${err}`, '500');
        }
    }
}


export const validPigAiId = async (pigAIIds: string[], penId: string): Promise<string[]> => {
    if(!penId) return [];
    const pigs = await Pig.find({ pigAIId: { $in: pigAIIds }, penId: penId });

    return pigs.map(pig => pig.pigAIId);
}