import mongoose, { Model, Document } from 'mongoose';
import { IPig } from './pig.types';

interface PigDocument extends IPig, Document { }
interface PigModel extends Model<PigDocument> { }

const pigSchema = new mongoose.Schema<PigDocument, PigModel>({
    pigAIId: String,
    healthStatus: {
        type: mongoose.Types.ObjectId,
        ref: 'HealthStatus',
    },
    size: String,
    weight: String,
    dateOfBirth: String,
    temperature: String,
    origin: String,
    penId: {
        type: mongoose.Types.ObjectId,
        ref: 'Pen',
    },
    weightTypeId: {
        type: mongoose.Types.ObjectId,
        ref: 'WeightType',
    },
    farmId: {
        type: mongoose.Types.ObjectId,
        ref: 'Farm',
        require: true
    }
})

pigSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {   delete ret._id  }
});

export default mongoose.model<PigDocument, PigModel>('Pig', pigSchema);
