import { BehaviourDocument } from '../behaviour/behaviour.model';
import { IPaginationCommon } from '../../types/common';
import { EventDocument } from '../event/event.model';

export interface IPig {
    pigAIId: string
    penId: string
    weightTypeId?: string
    healthStatus?: string
    size?: number
    weight?: number
    dateOfBirth?: string
    temperature?: string
    farmId: string
}

export interface ICreatePigDTO{
    pigAIIds: string[],
    penId?: string,
    farmId: string
}

export interface IGetPigDTO {
    id: string
    pigAIId?: string
    penId: any
    weightTypeId?: any
    healthStatus?: any
    size?: number
    weight?: number
    dateOfBirth?: string
    temperature?: string,
    farmId?: any,
    behaviour?: BehaviourDocument,
    events?: EventDocument[]
}

export interface IListPig extends IPaginationCommon {
    penId: string
    weightTypeId?: string
    healStatus?: string
    dateIn?: string
}

export interface ILstPigPagination extends IPaginationCommon{}

export interface IUpdatePigs {
    pigAIIds: string[]
    userId: string
    farmId: string
    data: {
        penId: string
        dateOfBirth?: string
        size?: number
        weight?: number
        healthStatus?: string
        changePenPrediction: string
        pigOutPrediction: string
        dateIn: string
        origin?: string
    }
}

export interface IUpdatePig {
    dateOfBirth?: string
    changePenPrediction?: string
    pigOutPrediction?: string
    dateIn?: string
    origin?: string
}

export type BaseResponseType<D> = SuccessResponseType<D> | FailedResponseType<string | string[]>;

export interface IBaseResponse{
    statusCode: string
}

interface SuccessResponseType<D> extends IBaseResponse {
    data: D
}

export interface FailedResponseType<E> extends IBaseResponse {
    message: E
}

