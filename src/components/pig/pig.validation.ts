import { body, check } from 'express-validator';

export const insertPigsValidation = [
    body('pigAIIds')
        .exists().withMessage('Trường này bắt buộc')
];

export const updatePigsValidation = [
    body('pigAIIds')
        .exists().withMessage('Trường này bắt buộc'),
    body('userId')
        .exists().withMessage('Trường này bắt buộc'),
    body('data.penId')
        .exists().withMessage('Trường này bắt buộc'),
    body('data.dateIn')
        .exists().withMessage('Trường này bắt buộc'),
];

export const deletePigsValidation = [
    body('lstPigs')
        .exists().withMessage('Trường này bắt buộc')
];

