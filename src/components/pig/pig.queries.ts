import { IListPig, ILstPigPagination } from './pig.types';
import { DEFAULT_OFFSET_PAGINATION, DEFAULT_SIZE_PAGINATION } from '../../utils/constants';
import mongoose from 'mongoose';
import { lookupSingleObjectHealthStatus } from '../healthStatus/healthStatus.queries';
// import { lookupObjectWeightType } from '../weightType/weightType.queries';
import { lookupObjectPens, lookupSingleObjectPen } from '../pen/pen.queries';
import { lookupSingleObjectFarm } from '../farm/farm.queries';
import { lookupSingleObjectWeightType } from '../weightType/weightType.queries';

export const lookupObjectPig = (collectionName: string, foreignName: string): any => ({
    $lookup: {
        from: collectionName,
        let: { [foreignName]: `$${ foreignName }` },
        as: collectionName,
        pipeline: [
            {
                $match: { $expr: { $in: [ '$_id', `$$${ foreignName }` ] } }
            },
            dataResponseProject
        ],
    }
});

const dataResponseProject = {
    $project: {
        _id: 0,
        id: '$_id',
        pigAIId: 1,
        // pens: '$pens',
        // weighttypes: '$weighttypes',
        // healthstatuses: '$healthstatuses',
        size: 10,
        weight: 1,
        dateOfBirth: 1,
        temperature: 1,
        // farms: '$farms'
    }
};

const dataDetailResponseProject = {
    $project: {
        _id: 0,
        id: '$_id',
        pigAIId: 1,
        pen: '$pen',
        weightType: '$weighttype',
        healthStatus: '$healthstatus',
        size: 10,
        weight: 1,
        dateOfBirth: 1,
        temperature: 1,
        farm: '$farm',
        origin: 1
    }
};

export const lookupObject = (collectionName: string, foreignName: string): any => ({
    $lookup: {
        from: collectionName,
        let: { [foreignName]: `$${ foreignName }` },
        as: collectionName,
        pipeline: [
            {
                $match: { $expr: { $in: [ '$_id', `$$${ foreignName }` ] } }
            },
        ]
    }
});

export const ListPigQuery = (params: IListPig): Array<Record<string, any>> => {
    const { offset = DEFAULT_OFFSET_PAGINATION, size = DEFAULT_SIZE_PAGINATION, penId } = params;

    return [
        // {
        //     $lookup: {
        //         from: 'pens',
        //         let: { penId: penId },
        //         pipeline: [
        //             {
        //                 $match: [
        //                     { $expr: [ { $eq: [ '$_id', '$$penId' ] } ] }
        //                 ]
        //             }
        //         ],
        //         as: 'pen'
        //     }
        // },
        {
            $facet: {
                count: [ { $count: 'total' } ],
                items: [
                    { $skip: +offset },
                    { $limit: +size },
                ],
            },
        },
        {
            $project: {
                items: 1,
                total: {
                    $cond: {
                        if: { $eq: [ { $size: '$count' }, 0 ] },
                        then: 0,
                        else: { $arrayElemAt: [ '$count.total', 0 ] }
                    },
                },
            },
        },
    ]
};

// export const detailPigQueryByPigAIId = (id: string, lstCond: any[]): Array<Record<string, any>> => [
//     {
//         $match: {
//             _id: { $eq: id }
//         }
//     },
//     lstCond && [...lstCond],
//     dataResponseProject
//     // lookupObject('healthstatuses', 'healthStatus'),
//     // lookupObject('weighttypes', 'weightTypeId'),
//     // lookupObject('farms', 'farmId'),
//     // lookupObject('pens', 'penId')
// ]

export const  detailPigQueryByPenId = (penId: string, data: ILstPigPagination): Array<Record<string, any>> => [
    {
        $match: {
            penId: { $eq: mongoose.Types.ObjectId(penId)}
        },
    },
    ...lookupSingleObjectHealthStatus(),
    ...lookupSingleObjectWeightType(),
    ...lookupSingleObjectFarm(),
    ...lookupSingleObjectPen(),
    dataDetailResponseProject,
    {
        $facet: {
            count: [ { $count: 'total' } ],
            items: [
                { $skip: +data.offset },
                { $limit: +data.size },
            ],
        },
    },
    {
        $project: {
            items: 1,
            total: {
                $cond: {
                    if: { $eq: [ { $size: '$count' }, 0 ] },
                    then: 0,
                    else: { $arrayElemAt: [ '$count.total', 0 ] }
                },
            },
        },
    },
]

export const  detailPigQueryByPigId = (pigId: string): Array<Record<string, any>> => [
    {
        $match: {
            _id: { $eq: mongoose.Types.ObjectId(pigId)}
        },
    },
    ...lookupSingleObjectHealthStatus(),
    ...lookupSingleObjectWeightType(),
    ...lookupSingleObjectFarm(),
    ...lookupSingleObjectPen(),
    dataDetailResponseProject
]

export const  detailPigQueryByFarmId = (farmId: string, data: ILstPigPagination, isIdentityConfirm: boolean): Array<Record<string, any>> => [
    {
        $match: {
            farmId: { $eq: mongoose.Types.ObjectId(farmId)},
            penId: isIdentityConfirm ? { $ne: null } : { $eq: null }
        },
    },
    ...lookupSingleObjectHealthStatus(),
    ...lookupSingleObjectWeightType(),
    ...lookupSingleObjectFarm(),
    ...lookupSingleObjectPen(),
    dataDetailResponseProject,
    {
        $facet: {
            count: [ { $count: 'total' } ],
            items: [
                { $skip: +data.offset },
                { $limit: +data.size },
            ],
        },
    },
    {
        $project: {
            items: 1,
            total: {
                $cond: {
                    if: { $eq: [ { $size: '$count' }, 0 ] },
                    then: 0,
                    else: { $arrayElemAt: [ '$count.total', 0 ] }
                },
            },
        },
    },
]
