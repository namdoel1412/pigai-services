import {Body, Controller, Delete, Get, Post, Put, Query, Route, Tags} from "tsoa";
import PenFoodConsume from '../penFoodConsume/penFoodConsume.model';
import {failedResponse, successResponse} from '../../../utils/http';
import { IPenFoodConsumeItem } from "../penFoodConsumeItem/penFoodConsumeItem.types";
import PenFoodConsumeItemRepository from "../penFoodConsumeItem/penFoodConsumeItemRepository";
import { IPenFoodConsume, IPenFoodConsumeCreateDTO, IPenFoodConsumeDetailUpdateDTO, IPenFoodConsumeReq, IPenFoodConsumeUpdateDTO } from "./penFoodConsume.types";
import { getPenFoodConsumeById, getPenFoodConsumeQuery } from "./penFoodConsume.query";
import FoodExportationRepository from "../foodExportation/foodExportationRepository";
import FoodConsumeRepository from "../foodConsume/foodConsumeRepository";
import FoodInventoryRepository from "../foodInventory/foodInventoryRepository";
import { session } from "passport";

@Route('penFoodConsume')
@Tags('PenFoodConsume')
export class PenFoodConsumeController extends Controller {

    /**
     * get PenFoodConsume Query
     */
    @Get('')
    public async getAllPenFoodConsume(@Query() name?: string, @Query() foodConsumeId?: string, @Query() penId?: string, @Query() startDate?: Date, @Query() endDate?: Date, @Query() offset?: number, @Query() size?: number): Promise<any>{
        try {
            let queryParams: IPenFoodConsumeReq = {
                name: name ?? "",
                startDate: startDate || new Date('1990-01-26T09:26:31.399+00:00'),
                endDate: endDate || new Date('2050-01-26T09:26:31.399+00:00'),
                offset: offset ?? 0,
                size: size ?? 10,
                foodConsumeId: foodConsumeId,
                penId: penId
            }
            let filter = getPenFoodConsumeQuery(queryParams);
            if (queryParams.penId == null){
                filter[0].$match.penId = {$ne: ""}
            }
            if (queryParams.foodConsumeId == null){
                filter[0].$match.foodConsumeId = {$ne: ""}
            }
            const foods = await PenFoodConsume.aggregate(filter);
            if (foods == null){
                this.setStatus(404);
                return failedResponse('Food is not found', 'FoodNotFound');
            }
            return successResponse(foods[0]);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * get PenFoodConsume by PenFoodConsume
     * @param penFoodConsumeId
     */
    @Get('/{penFoodConsumeId}')
    public async getPenFoodConsumeById(penFoodConsumeId: string): Promise<any>{
        try {
            const penFoodConsumes = await PenFoodConsume.findOne({})
            .where('_id').equals(penFoodConsumeId)
            .exec();
            if (penFoodConsumes == null){
                this.setStatus(404);
                return failedResponse('Food is not found', 'FoodNotFound');
            }
            let penFoodConsumeRes = await PenFoodConsume.aggregate(getPenFoodConsumeById(penFoodConsumeId));
            return successResponse(penFoodConsumeRes[0]);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Create penFoodConsume
     * @param data
     */
    @Post()
    public async createPenFoodConsume(@Body() data: IPenFoodConsumeCreateDTO): Promise<any>{
        try{
            const session = await PenFoodConsume.startSession();
            session.startTransaction();
            let penFoodConsumeData: IPenFoodConsume = {
                date: data.date,
                name: data.name,
                penId: data.penId,
                foodConsumeId: data.foodConsumeId
            }
            const foodConsume = await FoodConsumeRepository.GetFoodConsumesById(data.foodConsumeId);
            let result = await new PenFoodConsume(penFoodConsumeData).save();
            let lstConsumeItem: IPenFoodConsumeItem[] = [];
            if (result == null){
                await session.abortTransaction();
                return failedResponse('One of orders is invalid', 'OutOfStock');
            }
            for(let item of data.penFoodConsumeItems){
                const tmp = await PenFoodConsumeItemRepository.CreatePenFoodConsumeItem({
                    foodId: item.foodId,
                    quantity: item.quantity,
                    penFoodConsumeId: result.id,
                    foodSupplierId: item.foodSupplierId
                })
                // if(tmp == false ){
                //     await session.abortTransaction();
                //     return failedResponse('One of orders is invalid', 'OutOfStock');
                // }
                tmp && await FoodExportationRepository.UpdateFoodExportation({
                    farmId: foodConsume.farmId,
                    foodId: item.foodId,
                    foodSupplierId: item.foodSupplierId,
                    penId: data.penId,
                    quantity: item.quantity
                })
                let subRes = tmp && await FoodInventoryRepository.ExportFoodInventory({
                    farmId: foodConsume.farmId,
                    foodId: item.foodId,
                    foodSupplierId: item.foodSupplierId,
                    quantity: item.quantity
                })
                if (subRes == false){
                    await session.abortTransaction();
                    return failedResponse('One of orders is invalid', 'OutOfStock');
                }
            }
            await session.commitTransaction();
            //await PenFoodConsumeItemRepository.CreateManyPenFoodConsumeItem(lstConsumeItem);
            return successResponse(result);
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Update PenFoodConsume
     * @param penFoodConsumeId
     * @param data
     */
    @Put('/{penFoodConsumeId}')
    public async updatePenFoodConsume(penFoodConsumeId: string, @Body() data: IPenFoodConsumeDetailUpdateDTO): Promise<any>{
        try{
            if (await isExistPenFoodConsumes(penFoodConsumeId)) {
                await PenFoodConsume.findByIdAndUpdate(penFoodConsumeId, data);
                const penFoodConsume = await PenFoodConsume.findById(penFoodConsumeId)
                let tmp: IPenFoodConsumeUpdateDTO = {
                    date: data.date,
                    name: data.name,
                    foodConsumeId: data.foodConsumeId,
                    penId: data.penId
                }
                await PenFoodConsume.findByIdAndUpdate(penFoodConsumeId, tmp);
                if(data.penFoodConsumeItemsUpdate != null){
                    data.penFoodConsumeItemsUpdate.forEach(async element => {
                        PenFoodConsumeItemRepository.UpdatePenFoodConsumeItem(element.id, 
                            {
                                foodId: element.foodId,
                                penFoodConsumeId: element.penFoodConsumeId,
                                quantity: element.quantity
                            }) 
                    });
                }

                if(data.penFoodConsumeItemsCreate != null){
                    PenFoodConsumeItemRepository.CreateManyPenFoodConsumeItem(data.penFoodConsumeItemsCreate);
                }
                return successResponse(penFoodConsume);
            }
            this.setStatus(404);
            return failedResponse('PenFoodConsume is not found', 'PenFoodConsumeNotFound');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Delete penFoodConsume
     * @param penFoodConsumeId
     */
    @Delete('/{penFoodConsumeId}')
    public async deletePenFoodConsume(penFoodConsumeId: string): Promise<any>{
        try{
            if (await isExistPenFoodConsumes(penFoodConsumeId)) {
                let res = await PenFoodConsume.findByIdAndDelete(penFoodConsumeId);
                return successResponse(res);
            }
            this.setStatus(404);
            return failedResponse('PenFoodConsume is not found', 'PenFoodConsumeNotFound');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }
}

const isExistPenFoodConsumes = async (penFoodConsumeId: string): Promise<boolean> => {
    const food = await PenFoodConsume.find({})
            .where('_id').equals(penFoodConsumeId)
            .exec();
    return food.length > 0;
}