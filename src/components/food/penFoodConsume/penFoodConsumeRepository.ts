import { getPenFoodConsumeQuery } from "./penFoodConsume.query";
import { IPenFoodConsumeReq } from "./penFoodConsume.types";
import PenFoodConsume from '../penFoodConsume/penFoodConsume.model';

class PenFoodConsumeRepository{
    // static GetfoodInventoryById = async (farmId: string): Promise<IfoodInventoryGetDTO> => {
    //     let foodInventory = await foodInventory.findOne({}).where('_id').equals(foodInventoryId).exec();
    //     let foodRes = {
    //         id: foodInventory.id,
    //         foodConsumeId: foodInventory.foodConsumeId,
    //         quantity: foodInventory.quantity,
    //         foodId: foodInventory.foodId
    //     }
    //     return foodRes;
    // }

    static getPenFoodConsumeByFoodConsumeId = async (data: IPenFoodConsumeReq): Promise<any> => {
        let queryParams: IPenFoodConsumeReq = {
            name: data.name ?? "",
            startDate: data.startDate || new Date('1990-01-26T09:26:31.399+00:00'),
            endDate: data.endDate || new Date('2050-01-26T09:26:31.399+00:00'),
            offset: data.offset ?? 0,
            size: data.size ?? 10,
            foodConsumeId: data.foodConsumeId,
            penId: data.penId
        }
        let filter = getPenFoodConsumeQuery(queryParams);
        if (queryParams.penId == null){
            filter[0].$match.penId = {$ne: ""}
        }
        if (queryParams.foodConsumeId == null){
            filter[0].$match.foodConsumeId = {$ne: ""}
        }
        const foods = await PenFoodConsume.aggregate(filter);
        return foods;
    }
}

export default PenFoodConsumeRepository;