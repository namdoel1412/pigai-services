import { lookupSingleObjectFoodConsume, lookupSingleObjectPen } from "./../../../utils/aggregation";
import mongoose from "mongoose";
import { IPenFoodConsumeReq } from "./penFoodConsume.types";

const dataDetailResponseProjectPenFoodConsume = {
    $project: {
        id: '$_id',
        _id: 0,
        name: 1,
        date: 1,
        foodConsume: '$foodConsume',
        pen: '$pen'
    }
};

export const getPenFoodConsumeQuery = (data: IPenFoodConsumeReq): Array<Record<string, any>> => [
    {
        $match: {
            name: new RegExp(data.name, 'i'),
            date: { $gte: data?.startDate,  $lte: data?.endDate},
            penId: { $eq: mongoose.Types.ObjectId(data.penId) },
            foodConsumeId: { $eq: mongoose.Types.ObjectId(data.foodConsumeId) },
        },
    },
    {
        $sort: { date: -1}
    },
    ...lookupSingleObjectPen(),
    ...lookupSingleObjectFoodConsume(),
    dataDetailResponseProjectPenFoodConsume,
    {
        $facet: {
            count: [{$count: 'total'}],
            items: [
                {$skip: +data.offset},
                {$limit: +data.size},
            ],
        },
    },
    {
        $project: {
            items: 1,
            total: {
                $cond: {
                    if: {$eq: [{$size: '$count'}, 0]},
                    then: 0,
                    else: {$arrayElemAt: ['$count.total', 0]}
                },
            },
        },
    },
]

export const getPenFoodConsumeById = (penFoodConsume: string): Array<Record<string, any>> => [
    {
        $match: {
            _id: { $eq: mongoose.Types.ObjectId(penFoodConsume) }
        },
    },
    ...lookupSingleObjectPen(),
    ...lookupSingleObjectFoodConsume(),
    dataDetailResponseProjectPenFoodConsume
]