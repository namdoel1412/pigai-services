import {IPaginationCommon} from "../../../types/common";
import { IPenFoodConsumeItem, IPenFoodConsumeItemCreateDTO, IPenFoodConsumeItemDetailUpdateDTO } from "../penFoodConsumeItem/penFoodConsumeItem.types";
export interface IPenFoodConsume {
    name: string
    date: Date
    foodConsumeId: string
    penId: string
}

export interface IPenFoodConsumeReq extends IPaginationCommon{
    name?: string,
    startDate?: Date,
    endDate?: Date,
    foodConsumeId?: string,
    penId?: string
}

export interface IPenFoodConsumeCreateDTO{
    name: string,
    date: Date,
    foodConsumeId: string,
    penId: string,
    penFoodConsumeItems: IPenFoodConsumeItemCreateDTO[]
}

export interface IPenFoodConsumeUpdateDTO{
    name?: string
    date?: Date
    foodConsumeId?: string,
    penId?: string,
}

export interface IPenFoodConsumeDetailUpdateDTO{
    name?: string,
    date?: Date,
    foodConsumeId?: string,
    penId?: string,
    penFoodConsumeItemsUpdate?: IPenFoodConsumeItemDetailUpdateDTO[],
    penFoodConsumeItemsCreate?: IPenFoodConsumeItem[]
}