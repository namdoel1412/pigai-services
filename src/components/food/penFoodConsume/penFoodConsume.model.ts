import mongoose, { Model, Document } from 'mongoose';
import { IPenFoodConsume } from '@components/food/penFoodConsume/penFoodConsume.types';

interface IPenFoodConsumeDocument extends IPenFoodConsume, Document { }
interface IPenFoodConsumeModel extends Model<IPenFoodConsumeDocument> { }

const penFoodConsumeSchema = new mongoose.Schema<IPenFoodConsumeDocument, IPenFoodConsumeModel>({
    name: {
        type: String,
        require: true
    },
    date: {
        type: Date,
        require: true
    },
    foodConsumeId:  {
        type: mongoose.Types.ObjectId,
        ref: 'FoodConsume',
        required: true
    },
    penId:  {
        type: mongoose.Types.ObjectId,
        ref: 'Pen',
        required: true
    }
})

penFoodConsumeSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {   delete ret._id  }
});

export default mongoose.model<IPenFoodConsumeDocument, IPenFoodConsumeModel>('PenFoodConsume', penFoodConsumeSchema);