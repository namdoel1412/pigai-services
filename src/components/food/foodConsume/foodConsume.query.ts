import mongoose from "mongoose";
import { IFoodConsumeReq } from "./foodConsume.types";

const dataDetailResponseProjectFoodConsume = {
    $project: {
        id: '$_id',
        _id: 0,
        name: 1,
        date: 1,
        farm: '$farm',
        user: '$user'
    }
};

export const lookupSingleObjectFarm = (): any => (
    [
        {
            $lookup: {
                from: 'farms',
                let: {farmId: '$farmId'},
                as: 'farm',
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$_id', '$$farmId']}
                        }
                    },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            name: 1,
                            address: 1
                        }
                    }
                ]
            }
        },
        {
            $unwind: {
                path: '$farm',
                preserveNullAndEmptyArrays: true
            }
        }
    ]
);

export const lookupSingleObjectUser = (): any => (
    [
        {
            $lookup: {
                from: 'users',
                let: {userId: '$userId'},
                as: 'user',
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$_id', '$$userId']}
                        }
                    },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            username: 1,
                            email: 1,
                            fullName: 1,
                            avatar: 1,
                            phone: 1,
                            createTime: 1,
                            updateTime: 1
                        }
                    }
                ]
            }
        },
        {
            $unwind: {
                path: '$user',
                preserveNullAndEmptyArrays: true
            }
        }
    ]
);

export const getFoodConsumeQuery = (data: IFoodConsumeReq): Array<Record<string, any>> => [
    {
        $match: {
            name: new RegExp(data.name, 'i'),
            date: { $gte: data?.startDate,  $lte: data?.endDate},
            farmId: { $eq: mongoose.Types.ObjectId(data.farmId) },
            
        },
    },
    {
        $sort: { date: -1}
    },
    ...lookupSingleObjectFarm(),
    ...lookupSingleObjectUser(),
    dataDetailResponseProjectFoodConsume,
    {
        $facet: {
            count: [{$count: 'total'}],
            items: [
                {$skip: +data.offset},
                {$limit: +data.size},
            ],
        },
    },
    {
        $project: {
            items: 1,
            total: {
                $cond: {
                    if: {$eq: [{$size: '$count'}, 0]},
                    then: 0,
                    else: {$arrayElemAt: ['$count.total', 0]}
                },
            },
        },
    },
]