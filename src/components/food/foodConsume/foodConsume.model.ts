import mongoose, { Model, Document } from 'mongoose';
import { IFoodConsume } from '@components/food/foodConsume/foodConsume.types';

interface FoodConsumeDocument extends IFoodConsume, Document { }
interface FoodConsumeModel extends Model<FoodConsumeDocument> { }

const foodConsumeSchema = new mongoose.Schema<FoodConsumeDocument, FoodConsumeModel>({
    name: { 
        type: String,
        required: true
    },
    date: Date,
    farmId: {
        type: mongoose.Types.ObjectId,
        ref: 'Farm',
        required: true
    },
    userId: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
        required: true
    }
})

foodConsumeSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {   delete ret._id  }
});

export default mongoose.model<FoodConsumeDocument, FoodConsumeModel>('FoodConsume', foodConsumeSchema);