import {Body, Controller, Delete, Get, Post, Put, Query, Route, Tags} from "tsoa";
import FoodConsume from '../foodConsume/foodConsume.model';
import {IFoodConsume, IFoodConsumeCreateDTO, IFoodConsumeDetailUpdateDTO, IFoodConsumeGetDetailDTO, IFoodConsumeReq, IFoodConsumeUpdateDTO} from "./foodConsume.types";
import {failedResponse, successResponse} from '../../../utils/http'
import { getFoodConsumeQuery } from "./foodConsume.query";
import FarmRepository from "./../../farm/farmRepository";
import UserRepository from "./../../user/userRepository";
import { IFoodConsumeItem } from "../foodConsumeItem/foodConsumeItem.types";
import FoodConsumeItem from '../foodConsumeItem/foodConsumeItem.model';
import FoodConsumeItemRepository from "../foodConsumeItem/foodConsumeItemRepository";
import { IPenFoodConsumeReq } from "../penFoodConsume/penFoodConsume.types";
import { getPenFoodConsumeQuery } from "../penFoodConsume/penFoodConsume.query";
import PenFoodConsumeRepository from "../penFoodConsume/penFoodConsumeRepository";
import PenFoodConsumeItem from '../penFoodConsumeItem/penFoodConsumeItem.model';
import { getPenFoodConsumeItemQueryByPenFoodConsumeId } from "../penFoodConsumeItem/penFoodConsumeItem.query";
import { IPenFoodConsumeAndPaginationReq } from "../penFoodConsumeItem/penFoodConsumeItem.types";

@Route('foodConsume')
@Tags('FoodConsume')
export class FoodConsumeController extends Controller {

    /**
     * get foodConsume Query
     */
    @Get('')
    public async getAllFoodConsume(@Query() name?: string, @Query() farmId?: string, @Query() startDate?: Date, @Query() endDate?: Date, @Query() offset?: number, @Query() size?: number): Promise<any>{
        try {
            let paging: IFoodConsumeReq = {
                name: name ?? "",
                startDate: startDate || new Date('1990-01-26T09:26:31.399+00:00'),
                endDate: endDate || new Date('2050-01-26T09:26:31.399+00:00'),
                offset: offset ?? 0,
                size: size ?? 10,
                farmId: farmId
            }
            const foods = await FoodConsume.aggregate(getFoodConsumeQuery(paging));
            if (foods == null){
                this.setStatus(404);
                return failedResponse('Food is not found', 'FoodNotFound');
            }
            return successResponse(foods[0]);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * get foodConsume by foodConsume
     * @param foodConsumeId
     */
    @Get('/{foodConsumeId}')
    public async getFoodConsumeById(foodConsumeId: string, @Query() offset?: number, @Query() size?: number): Promise<any>{
        try {
            const foodConsumes = await FoodConsume.findOne({})
            .where('_id').equals(foodConsumeId)
            .exec();
            if (foodConsumes == null){
                this.setStatus(404);
                return failedResponse('Food is not found', 'FoodNotFound');
            }
            let queryParams: IPenFoodConsumeReq = {
                startDate: new Date('1990-01-26T09:26:31.399+00:00'),
                endDate: new Date('2050-01-26T09:26:31.399+00:00'),
                offset: 0,
                size: 50,
                foodConsumeId: foodConsumeId
            }
            let filter = getPenFoodConsumeQuery(queryParams);
            if (queryParams.penId == null){
                filter[0].$match.penId = {$ne: ""}
            }
            if (queryParams.foodConsumeId == null){
                filter[0].$match.foodConsumeId = {$ne: ""}
            }
            const foods = await PenFoodConsumeRepository.getPenFoodConsumeByFoodConsumeId(queryParams);
            if (foods == null){
                this.setStatus(404);
                return failedResponse('Food is not found', 'FoodNotFound');
            }
            console.log(foods[0].items)
            let detailPenFoodConsume = JSON.parse(JSON.stringify(foods[0].items))
            console.log(detailPenFoodConsume);
            let len = foods[0].items.length;
            for(let i = 0; i < len; i++){
                let req: IPenFoodConsumeAndPaginationReq = {
                    penFoodConsumeId: foods[0].items[i].id,
                    offset: offset > 0 ? offset : 0,
                    size: size > 0 ? size : 10
                }
                const subPenFoodConsumeItem = await PenFoodConsumeItem.aggregate(getPenFoodConsumeItemQueryByPenFoodConsumeId(req));
                console.log('===========subPenFood=============')
                console.log(subPenFoodConsumeItem[0])
                detailPenFoodConsume[i]['penFoodConsumeItem'] = subPenFoodConsumeItem[0];
            }
            console.log(detailPenFoodConsume);
            let foodConsumeRes: IFoodConsumeGetDetailDTO = {
                id: foodConsumes.id,
                name: foodConsumes.name,
                date: foodConsumes.date,
                farm: await FarmRepository.GetFarmById(foodConsumes.farmId),
                user: await UserRepository.GetUserById(foodConsumes.userId),
                lstPenFoodConsume: detailPenFoodConsume
            }
            return successResponse(foodConsumeRes);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Create food
     * @param data
     */
    @Post()
    public async createFoodConsume(@Body() data: IFoodConsumeCreateDTO): Promise<any>{
        try{
            let foodConsumeData: IFoodConsume = {
                date: data.date,
                farmId: data.farmId,
                name: data.name,
                userId: data.userId
            }
            let result = await new FoodConsume(foodConsumeData).save();
            let lstConsumeItem: IFoodConsumeItem[] = [];
            data.foodConsumeItems.forEach(item => {
                lstConsumeItem.push({
                    foodId: item.foodId,
                    quantity: item.quantity,
                    foodConsumeId: result.id
                })
            })
            FoodConsumeItem.create(lstConsumeItem);
            console.log(result);
            return successResponse(result);
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Update foodConsume
     * @param foodConsumeId
     * @param data
     */
    @Put('/{foodConsumeId}')
    public async updateFoodConsume(foodConsumeId: string, @Body() data: IFoodConsumeDetailUpdateDTO): Promise<any>{
        try{
            if (await isExistFoodConsumes(foodConsumeId)) {
                await FoodConsume.findByIdAndUpdate(foodConsumeId, data);
                const foodConsume = await FoodConsume.findById(foodConsumeId)
                let tmp: IFoodConsumeUpdateDTO = {
                    date: data.date,
                    farmId: data.farmId,
                    userId: data.userId,
                    name: data.name
                }
                await FoodConsume.findByIdAndUpdate(foodConsumeId, tmp);
                if(data.foodConsumeItemsUpdate != null){
                    data.foodConsumeItemsUpdate.forEach(element => {
                        FoodConsumeItemRepository.UpdateFoodConsumeItem(element.id, 
                            {
                                foodId: element.foodId,
                                foodConsumeId: element.foodConsumeId,
                                quantity: element.quantity
                            }) 
                    });
                }

                if(data.foodConsumeItemsCreate != null){
                    FoodConsumeItemRepository.CreateManyFoodConsumeItem(data.foodConsumeItemsCreate);
                }
                return successResponse(foodConsume);
            }
            this.setStatus(404);
            return failedResponse('FoodConsume is not found', 'FoodConsumeNotFound');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Delete foodConsume
     * @param foodConsumeId
     */
    @Delete('/{foodConsumeId}')
    public async deleteFoodConsume(foodConsumeId: string): Promise<any>{
        try{
            if (await isExistFoodConsumes(foodConsumeId)) {
                let res = await FoodConsume.findByIdAndDelete(foodConsumeId);
                return successResponse(res);
            }
            this.setStatus(404);
            return failedResponse('FoodConsume is not found', 'FoodConsumeNotFound');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }
}

const isExistFoodConsumes = async (foodConsumeId: string): Promise<boolean> => {
    const food = await FoodConsume.find({})
            .where('_id').equals(foodConsumeId)
            .exec();
    return food.length > 0;
}