import { IFarmGetDTO } from "@components/farm/farm.types";
import { IUserGetDTO } from "@components/user/user.types";
import {IPaginationCommon} from "../../../types/common";
import { IFoodConsumeItem, IFoodConsumeItemCreateDTO, IFoodConsumeItemDetailUpdateDTO } from "../foodConsumeItem/foodConsumeItem.types";
export interface IFoodConsume {
    name: string
    date: Date
    farmId: string,
    userId: string
}

export interface IFoodConsumeCreateDTO{
    name: string,
    date: Date,
    farmId: string,
    userId: string,
    foodConsumeItems: IFoodConsumeItemCreateDTO[]
}

export interface IFoodConsumeReq extends IPaginationCommon{
    name?: string,
    startDate?: Date,
    endDate?: Date,
    farmId?: string
}

export interface IFoodConsumeGetDetailDTO{
    id: string,
    name: string
    date?: Date
    farm: IFarmGetDTO,
    user: IUserGetDTO,
    lstPenFoodConsume?: any
}

export interface IFoodConsumeGetDTO{
    id: string,
    name: string
    date?: Date
    farmId: string,
    userId: string
}

export interface IFoodConsumeUpdateDTO{
    name?: string
    date?: Date
    farmId?: string,
    userId?: string,
}

export interface IFoodConsumeDetailUpdateDTO{
    name?: string,
    date?: Date,
    farmId?: string,
    userId?: string,
    foodConsumeItemsUpdate: IFoodConsumeItemDetailUpdateDTO[],
    foodConsumeItemsCreate: IFoodConsumeItem[]
}