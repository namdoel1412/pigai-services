import FarmRepository from "./../../farm/farmRepository";
import UserRepository from "./../../user/userRepository";
import FoodConsume from "../foodConsume/foodConsume.model";
import { IFoodConsumeGetDetailDTO, IFoodConsumeGetDTO } from "./FoodConsume.types";

class FoodConsumeRepository{
    static GetFoodConsumeById = async (FoodConsumeId: string): Promise<IFoodConsumeGetDetailDTO> => {
        const foodConsumes = await FoodConsume.findOne({})
            .where('_id').equals(FoodConsumeId)
            .exec();
            if (foodConsumes == null){
                return null;
            }
            let foodConsumeRes: IFoodConsumeGetDetailDTO = {
                id: foodConsumes.id,
                name: foodConsumes.name,
                date: foodConsumes.date,
                farm: await FarmRepository.GetFarmById(foodConsumes.farmId),
                user: await UserRepository.GetUserById(foodConsumes.userId),
                lstPenFoodConsume: null
            }
            return foodConsumeRes;
    }

    static GetFoodConsumesById = async (FoodConsumeId: string): Promise<IFoodConsumeGetDTO> => {
        const foodConsumes = await FoodConsume.findOne({})
            .where('_id').equals(FoodConsumeId)
            .exec();
            if (foodConsumes == null){
                return null;
            }
            let foodConsumeRes: IFoodConsumeGetDTO = {
                id: foodConsumes.id,
                name: foodConsumes.name,
                date: foodConsumes.date,
                farmId: foodConsumes.farmId.toString(),
                userId: foodConsumes.userId
            }
            return foodConsumeRes;
    }
}

export default FoodConsumeRepository;