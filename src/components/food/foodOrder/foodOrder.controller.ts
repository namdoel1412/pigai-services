import FoodOrder from './foodOrder.model';
import { IFoodOrder, IFoodOrderCreateDTO, IFoodOrderDetailUpdateDTO, IFoodOrderUpdateDTO, IFoodSupplierAndPaginationReq } from './foodOrder.types';
import { failedResponse, successResponse } from '../../../utils/http'
import {Body, Controller, Delete, Get, Post, Put, Query, Route, Tags} from 'tsoa';
import { IPaginationCommon } from '@type/common';
import { getFoodOrderQueryByFoodSupplierId } from './foodOrder.query';
import FoodOrderRepository from './foodOrderRepository';
import { IFoodOrderItem } from '../foodOrderItems/foodOrderItems.types';
import FoodOrderItem from '../foodOrderItems/foodOrderItem.model';
import FoodOrderItemRepository from '../foodOrderItems/foodOrderItemRepository';
import FoodInventoryRepository from '../foodInventory/foodInventoryRepository';

@Route('foodOrder')
@Tags('FoodOrder')
export class FoodOrderController extends Controller{

    /**
    Get all FoodOrder of FoodSupplier
     */
    @Get('/foodSupplier')
    public async getAllFoodOrderOfFoodSupplier(@Query() foodSupplierId: string, @Query() offset?: number, @Query() size?: number): Promise<any> {
        try {
            if (offset < 0 || size <= 0) {
                this.setStatus(400);
                return failedResponse('Offset and size are required', 'OffsetRequired');
            }
            let pagination: IPaginationCommon = {
                offset : offset || 0,
                size: size || 10
            }
            const result = await FoodOrder.aggregate(getFoodOrderQueryByFoodSupplierId(foodSupplierId, pagination));
            return successResponse(result[0]);
        } catch (e) {
            return failedResponse(`Caught error ${e}`, '500');
        }
    }


    /**
     * get food by foodId
     * @param foodOrderId
     */
    @Get('/{foodOrderId}')
    public async getFoodOrderById(foodOrderId: string): Promise<any>{
        try {
            const foods = await FoodOrderRepository.GetFoodOrderById(foodOrderId);
            if (foods == null){
                this.setStatus(404);
                return failedResponse('Food is not found', 'FoodNotFound');
            }
            return successResponse(foods);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    // /**
    //  * Create foodOrder
    //  * @param data
    //  */
    // @Post()
    // public async createFoodOrder(@Body() data: IFoodOrder): Promise<any>{
    //     try{
    //         let result = await new FoodOrder(data).save();
    //         return successResponse(result);
    //     } catch (err) {
    //         return failedResponse(`Error: ${err}`, 'ServiceException');
    //     }
    // }

    /**
     * Create foodOrder
     * @param data
     */
    @Post()
    public async createAnOrder(@Body() data: IFoodOrderCreateDTO): Promise<any>{
        const session = await FoodOrder.startSession();
        try{
            session.startTransaction();
            let foodOrder: IFoodOrder = {
                date: data.date,
                foodSupplierId: data.foodSupplierId,
                name: data.name,
                farmId: data.farmId,
                totalPrice: data.totalPrice
            }
            let result = await new FoodOrder(foodOrder).save();
            let lstOrderItem: IFoodOrderItem[] = [];
            for(let item of data.foodOrderItems){
                lstOrderItem.push({
                    foodId: item.foodId,
                    quantity: item.quantity,
                    foodOrderId: result.id,
                    price: item.price,
                    subTotal: item.subTotal,
                    expirationDate: item.expirationDate
                })
                let subRes = await FoodInventoryRepository.ImportFoodInventory({
                    farmId: data.farmId,
                    foodId: item.foodId,
                    foodSupplierId: data.foodSupplierId,
                    quantity: item.quantity
                })
                if (subRes == false){
                    await session.abortTransaction();
                    return failedResponse('One of orders is invalid', 'OutOfStock');
                }
            }
            await FoodOrderItem.create(lstOrderItem);
            await session.commitTransaction();
            return successResponse(result);
        } catch (err) {
            await session.abortTransaction();
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }
 

    /**
     * Update foodOrder
     * @param foodId
     * @param data
     */
    @Put('/{foodOrderId}')
    public async updateFood(foodOrderId: string, @Body() data: IFoodOrderDetailUpdateDTO): Promise<any>{
        try{
            if (await isExistFoodOrders(foodOrderId)) {
                let tmp: IFoodOrderUpdateDTO = {
                    date: data.date,
                    foodSupplierId: data.foodSupplierId,
                    name: data.name,
                    totalPrice: data.totalPrice
                }
                await FoodOrder.findByIdAndUpdate(foodOrderId, tmp);
                if(data.foodOrderItemUpdate != null){
                    data.foodOrderItemUpdate.forEach(element => {
                        FoodOrderItemRepository.UpdateFoodOrderItem(element.id, 
                            {
                                foodId: element.foodId,
                                foodOrderId: element.foodOrderId,
                                quantity: element.quantity,
                                expirationDate: element.expirationDate,
                                price: element.price,
                                subTotal: element.subTotal
                            }) 
                    });
                }

                if(data.foodOrderItemCreate != null){
                    data.foodOrderItemCreate.forEach(element => {
                        FoodOrderItemRepository.CreateFoodOrderItem(element);
                    })
                }
                const foodOrder = await FoodOrder.findById(foodOrderId)
                return successResponse(foodOrder);
            }
            this.setStatus(404);
            return failedResponse('Food is not found', 'FoodNotFound');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Delete foodOrder
     * @param foodId
     */
    @Delete('/{foodOrderId}')
    public async deleteFoodOrder(foodOrderId: string): Promise<any>{
        try{
            if (await isExistFoodOrders(foodOrderId)) {
                let res = await FoodOrder.findByIdAndDelete(foodOrderId);
                return successResponse(res);
            }
            this.setStatus(404);
            return failedResponse('Food is not found', 'FoodNotFound');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }
}

const isExistFoodOrders = async (foodOrderId: string): Promise<boolean> => {
    const foodOrder = await FoodOrder.find({})
            .where('_id').equals(foodOrderId)
            .exec();
    return foodOrder.length > 0;
}