import FoodOrder from "../foodOrder/foodOrder.model";
import FoodSupplierRepository from "../foodSupplier/foodSupplierRepository";
import { IFoodOrderGetDTO } from "./foodOrder.types";

class FoodOrderRepository{
    static GetFoodOrderById = async (foodOrderId: string): Promise<IFoodOrderGetDTO> => {
        let foodOrder = await FoodOrder.findOne({}).where('_id').equals(foodOrderId).exec();
        let foodRes: IFoodOrderGetDTO = {
            id: foodOrder.id,
            name: foodOrder.name,
            date: foodOrder.date,
            foodSupplier: await FoodSupplierRepository.GetFoodSupplierById(foodOrder.foodSupplierId),
            farmId: foodOrder.farmId,
            totalPrice: foodOrder.totalPrice
        }
        return foodRes;
    }
}

export default FoodOrderRepository;