import {IPaginationCommon} from "../../../types/common";
import { IFoodOrderItem, IFoodOrderItemCreateDTO, IFoodOrderItemDetailUpdateDTO } from "../foodOrderItems/foodOrderItems.types";
import { IFoodSupplierGetDTO } from "../foodSupplier/foodSupplier.types";

export interface IFoodOrder {
    name: string
    date: Date
    foodSupplierId: string
    farmId: string,
    totalPrice?: number
}

export interface IFoodOrderGetDTO{
    id: string
    name: string
    date: Date
    foodSupplier: IFoodSupplierGetDTO
    farmId?: string,
    totalPrice?: number
}

export interface IFoodOrderPagination extends IPaginationCommon{}

export interface IFoodSupplierAndPaginationReq extends IFoodOrderPagination{
    foodSupplierId: string
}

export interface IFoodOrderUpdateDTO{
    name?: string
    date?: Date
    foodSupplierId?: string,
    farmId?: string,
    totalPrice?: number
}

export interface IFoodOrderDetailUpdateDTO{
    name?: string,
    date?: Date,
    foodSupplierId?: string,
    farmId?: string,
    foodOrderItemUpdate?: IFoodOrderItemDetailUpdateDTO[],
    foodOrderItemCreate?: IFoodOrderItem[],
    totalPrice?: number
}

export interface IFoodOrderCreateDTO{
    name: string,
    date: Date,
    foodSupplierId: string,
    foodOrderItems: IFoodOrderItemCreateDTO[],
    totalPrice?: number,
    farmId: string
}