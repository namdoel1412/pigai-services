import mongoose, { Model, Document } from 'mongoose';
import { IFoodOrder } from '@components/food/foodOrder/foodOrder.types';

interface FoodOrderDocument extends IFoodOrder, Document { }
interface FoodOrderModel extends Model<FoodOrderDocument> { }

const foodOrderSchema = new mongoose.Schema<FoodOrderDocument, FoodOrderModel>({
    name: {
        type: String,
        required: true
    },
    date: Date,
    foodSupplierId: {
        type: mongoose.Types.ObjectId,
        ref: 'FoodSupplier',
        required: true
    },
    farmId: {
        type: mongoose.Types.ObjectId,
        ref: 'Farm'
    },
    totalPrice: {
        type: Number,
        require: false,
        default: 0
    }
})

foodOrderSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {   delete ret._id  }
});

export default mongoose.model<FoodOrderDocument, FoodOrderModel>('FoodOrder', foodOrderSchema);