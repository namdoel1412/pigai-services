import mongoose from "mongoose";
import { IFoodOrderPagination } from "./foodOrder.types";

const dataDetailResponseProjectFoodOrder = {
    $project: {
        id: '$_id',
        _id: 0,
        name: 1,
        date: 1,
        foodSupplier: '$foodsupplier'
    }
};

export const lookupSingleObjectFoodSupplier = (): any => (
    [
        {
            $lookup: {
                from: 'foodsuppliers',
                let: {foodSupplierId: '$foodSupplierId'},
                as: 'foodsupplier',
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$_id', '$$foodSupplierId']}
                        }
                    },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            name: 1,
                            address: 1,
                            farmId: 1
                        }
                    }
                ]
            }
        },
        {
            $unwind: {
                path: '$foodsupplier',
                preserveNullAndEmptyArrays: true
            }
        }
    ]
);

export const getFoodOrderQueryByFoodSupplierId = (foodSupplierId: string, data: IFoodOrderPagination, name?: string ): Array<Record<string, any>> => [
    {
        $match: {
            foodSupplierId: {$eq: mongoose.Types.ObjectId(foodSupplierId)},
            // name: new RegExp(name, 'g'),
        },
    },
    ...lookupSingleObjectFoodSupplier(),
    dataDetailResponseProjectFoodOrder,
    {
        $facet: {
            count: [{$count: 'total'}],
            items: [
                {$skip: +data.offset},
                {$limit: +data.size},
            ],
        },
    },
    {
        $project: {
            items: 1,
            total: {
                $cond: {
                    if: {$eq: [{$size: '$count'}, 0]},
                    then: 0,
                    else: {$arrayElemAt: ['$count.total', 0]}
                },
            },
        },
    },
]