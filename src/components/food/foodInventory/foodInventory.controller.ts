import {Body, Controller, Delete, Get, Post, Put, Query, Route, Tags} from "tsoa";
import FoodInventory from './foodInventory.model';
import {failedResponse, successResponse} from '../../../utils/http';
import { IFoodInventoryGetDetailDTO } from "./foodInventory.types";
import FarmRepository from "@components/farm/farmRepository";
import { getFoodInventoryQuery } from "./foodInventory.query";

@Route('foodInventory')
@Tags('FoodInventory')
export class FoodInventoryController extends Controller {

    /**
     * get foodInventory Query
     */
    @Get('')
    public async getFoodInventory(@Query() farmId: string, @Query() foodSupplierId: string, @Query() size?: number, @Query() offset?: number): Promise<any>{
        try {
            let data = await FoodInventory.aggregate(getFoodInventoryQuery({
                farmId: farmId,
                foodSupplierId: foodSupplierId,
                size: size ?? 10,
                offset: offset ?? 0
            }))
            return successResponse(data[0]);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }
}