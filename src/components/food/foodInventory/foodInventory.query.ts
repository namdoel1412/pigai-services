import { lookupSingleObjectFarm, lookupSingleObjectFoodAggregation, lookupSingleObjectFoodSupplier } from "./../../../utils/aggregation";
import mongoose from "mongoose";
import { IFoodInventoryReq } from "./foodInventory.types";

const dataDetailResponseProjectFoodInventory = {
    $project: {
        id: '$_id',
        _id: 0,
        farm: '$farm',
        foodSupplier: '$foodsupplier',
        food: '$food',
        quantity: 1
    }
};

export const getFoodInventoryQuery = (data: IFoodInventoryReq): Array<Record<string, any>> => [
    {
        $match: {
            farmId: { $eq: mongoose.Types.ObjectId(data.farmId) },
            foodSupplierId: { $eq: mongoose.Types.ObjectId(data.foodSupplierId) }
        },
    },
    // {
    //     $sort: { date: -1}
    // },
    ...lookupSingleObjectFarm(),
    ...lookupSingleObjectFoodAggregation(),
    ...lookupSingleObjectFoodSupplier(),
    dataDetailResponseProjectFoodInventory,
    {
        $facet: {
            count: [{$count: 'total'}],
            items: [
                {$skip: +data.offset},
                {$limit: +data.size},
            ],
        },
    },
    {
        $project: {
            items: 1,
            total: {
                $cond: {
                    if: {$eq: [{$size: '$count'}, 0]},
                    then: 0,
                    else: {$arrayElemAt: ['$count.total', 0]}
                },
            },
        },
    },
]