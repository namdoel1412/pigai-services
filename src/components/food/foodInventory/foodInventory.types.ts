import { IFarmGetDTO } from "@components/farm/farm.types";
import { IPaginationCommon } from "@type/common";
import { IFoodGetDTO } from "../food/food.types";
import { IFoodSupplierGetDTO } from "../foodSupplier/foodSupplier.types";

export interface IFoodInventory{
    foodSupplierId: string,
    foodId: string,
    quantity: number,
    farmId: string
}

export interface IFoodInventoryGetDTO{
    id: string,
    foodSupplierId: string,
    foodId: string,
    farmId: string,
    quantity: number
}

export interface IFoodInventoryGetDetailDTO{
    id: string,
    farm: IFarmGetDTO,
    foodSupplier: IFoodSupplierGetDTO,
    food: IFoodGetDTO,
    quantity: number
}

export interface IFoodInventoryUpdateDTO{
    farmId: string,
    foodSupplierId: string,
    foodId: string,
    quantity: number
}

export interface IFoodInventoryReq extends IPaginationCommon{
    farmId: string,
    foodSupplierId: string
}