import FoodInventory from './foodInventory.model';
import { IFoodInventory, IFoodInventoryUpdateDTO } from './foodInventory.types';

class FoodInventoryRepository{
    // static GetfoodInventoryById = async (farmId: string): Promise<IfoodInventoryGetDTO> => {
    //     let foodInventory = await foodInventory.findOne({}).where('_id').equals(foodInventoryId).exec();
    //     let foodRes = {
    //         id: foodInventory.id,
    //         foodConsumeId: foodInventory.foodConsumeId,
    //         quantity: foodInventory.quantity,
    //         foodId: foodInventory.foodId
    //     }
    //     return foodRes;
    // }

    static ExportFoodInventory = async (data: IFoodInventoryUpdateDTO): Promise<any> => {
        try{
            console.log(data)
            let entity = await FoodInventoryRepository.isExistFoodInventory(data.farmId, data.foodSupplierId, data.foodId);
            console.log(entity)
            if (entity != null){
                if(data.quantity > entity.quantity){
                    return false
                }
                entity.quantity-=data.quantity ?? 0;
                console.log(entity)
                let res = await FoodInventory.findByIdAndUpdate(entity.id, entity);
                return entity;
            }
            else{
                let res = await FoodInventoryRepository.CreateFoodInventory({
                    foodId: data.foodId,
                    foodSupplierId: data.foodSupplierId,
                    farmId: data.farmId,
                    quantity: data.quantity
                })
                console.log('Not found')
                console.log(res)
            }
        }
        catch (err) {
            console.log(`Error: ${err}`);
            return false;
        }
    }

    static ImportFoodInventory = async (data: IFoodInventoryUpdateDTO): Promise<any> => {
        try{
            let entity = await FoodInventoryRepository.isExistFoodInventory(data.farmId, data.foodSupplierId, data.foodId);
            if (entity != null){
                entity.quantity+=data.quantity ?? 0;
                console.log(entity)
                let res = await FoodInventory.findByIdAndUpdate(entity.id, entity);
                return entity;
            }
            else{
                let res = await FoodInventoryRepository.CreateFoodInventory({
                    foodId: data.foodId,
                    foodSupplierId: data.foodSupplierId,
                    farmId: data.farmId,
                    quantity: data.quantity
                })
                console.log('Not found__')
                console.log(res)
                return res;
            }
        }
        catch (err) {
            console.log(`Error: ${err}`);
            return false;
        }
    }

    static CreateFoodInventory = async (data: IFoodInventory): Promise<any> => {
        try{
            console.log(data)
            let result = await new FoodInventory(data).save();
            return true;
        } catch (err) {
            console.log(err)
            return false;
        }
    }

    static CreateManyFoodInventory = async (data: IFoodInventory[]): Promise<any> => {
        try{
            FoodInventory.create(data);
            return true;
        } catch (err) {
            return false;
        }
    }

    static isExistFoodInventory = async (farmId: string, foodSupplierId: string, foodId: string): Promise<any> => {
        const foodInventory = await FoodInventory.findOne({})
                .where('farmId').equals(farmId)
                .where('foodSupplierId').equals(foodSupplierId)
                .where('foodId').equals(foodId)
                .exec();
        return foodInventory ?? null;
    }
}

export default FoodInventoryRepository;