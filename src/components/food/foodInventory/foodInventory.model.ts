import mongoose, { Model, Document } from 'mongoose';
import { IFoodInventory } from '@components/food/foodInventory/foodInventory.types';

interface FoodInventoryDocument extends IFoodInventory, Document { }
interface FoodInventoryModel extends Model<FoodInventoryDocument> { }

const foodInventorySchema = new mongoose.Schema<FoodInventoryDocument, FoodInventoryModel>({
    farmId: {
        type: mongoose.Types.ObjectId,
        ref: 'Farm',
        required: true
    },
    foodSupplierId: {
        type: mongoose.Types.ObjectId,
        ref: 'FoodSupplier',
        required: true,
    },
    foodId: {
        type: mongoose.Types.ObjectId,
        ref: 'Food',
        required: true,
    },
    quantity: {
        type: Number,
        required: true
    }
})

foodInventorySchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {   delete ret._id  }
});

export default mongoose.model<FoodInventoryDocument, FoodInventoryModel>('FoodInventory', foodInventorySchema);