import mongoose, { Model, Document } from 'mongoose';
import { IFoodSupplier } from '../foodSupplier/foodSupplier.types';

interface FoodSupplierDocument extends IFoodSupplier, Document { }
interface FoodSupplierModel extends Model<FoodSupplierDocument> { }

const foodSupplierSchema = new mongoose.Schema<FoodSupplierDocument, FoodSupplierModel>({
    name: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    farmId: {
        type: mongoose.Types.ObjectId,
        ref: 'Farm',
        required: true
    }
})

export default mongoose.model<FoodSupplierDocument, FoodSupplierModel>('FoodSupplier', foodSupplierSchema);