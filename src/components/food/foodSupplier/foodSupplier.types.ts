import {IPaginationCommon} from "../../../types/common";

export interface IFoodSupplier {
    name: string
    address: string
    farmId: string
}

export interface IFoodSupplierPagination extends IPaginationCommon{}

export interface IReqGetFoodSupplierByFarmId extends IFoodSupplierPagination{
    farmId: string
    name?: string
}

export interface IFoodSupplierGetDTO{
    id: string
    name: string
    address?: string
    farmId: string
}
