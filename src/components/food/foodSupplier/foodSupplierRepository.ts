import FoodSupplier from "../foodSupplier/foodSupplier.model";
import { IFoodSupplierGetDTO } from "./foodSupplier.types";

class FoodSupplierRepository{
    static GetFoodSupplierById = async (foodSupplierId: string): Promise<IFoodSupplierGetDTO> => {
        let foodSupplier = await FoodSupplier.findOne({}).where('_id').equals(foodSupplierId).exec();
        let foodRes: IFoodSupplierGetDTO = {
            id: foodSupplier.id,
            name: foodSupplier.name,
            address: foodSupplier.address,
            farmId: foodSupplier.farmId
        }
        return foodRes;
    }
}

export default FoodSupplierRepository;