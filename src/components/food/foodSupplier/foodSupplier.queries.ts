import {IFarmPagination} from "../../farm/farm.types";
import mongoose from "mongoose";

const dataDetailResponseProjectFarm = {
    $project: {
        id: '$_id',
        _id: 0,
        name: 1,
        address: 1,
        owner: '$user'
    }
};

export const lookupSingleObjectFarm = (): any => (
    [
        {
            $lookup: {
                from: 'farms',
                let: {farmId: '$farmId'},
                as: 'farm',
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$_id', '$$farmId']}
                        }
                    },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            name: 1,
                            address: 1,
                        }
                    }
                ]
            }
        },
        {
            $unwind: {
                path: '$farm',
                preserveNullAndEmptyArrays: true
            }
        }
    ]
);

export const getFoodSupplierQueryByFarmId = (farmId: string, data: IFarmPagination, name?: string ): Array<Record<string, any>> => [
    {
        $match: {
            farmId: {$eq: mongoose.Types.ObjectId(farmId)},
            name: new RegExp(name, 'g'),
        },
    },
    ...lookupSingleObjectFarm(),
    dataDetailResponseProjectFarm,
    {
        $facet: {
            count: [{$count: 'total'}],
            items: [
                {$skip: +data.offset},
                {$limit: +data.size},
            ],
        },
    },
    {
        $project: {
            items: 1,
            total: {
                $cond: {
                    if: {$eq: [{$size: '$count'}, 0]},
                    then: 0,
                    else: {$arrayElemAt: ['$count.total', 0]}
                },
            },
        },
    },
]