import {Body, Controller, Delete, Get, Post, Put, Route, Tags} from "tsoa";
import {failedResponse, successResponse} from "../../../utils/http";
import {IFoodSupplier, IReqGetFoodSupplierByFarmId} from "../foodSupplier/foodSupplier.types";
import Farm from "../../farm/farm.model";
import FoodSupplier from "./foodSupplier.model";
import {getFoodSupplierQueryByFarmId} from "../foodSupplier/foodSupplier.queries";
import {IPaginationCommon} from "../../../types/common";

@Route('/foodSuppliers')
@Tags('FoodSupplier')
export class FoodSupplierController extends Controller{

    /**
    Get all suppliers of farm
     */
    @Post('/farm')
    public async getAllFoodSupplierOfFarm(@Body() data: IReqGetFoodSupplierByFarmId): Promise<any> {
        try {
            if (data.offset < 0 || data.size <= 0) {
                this.setStatus(400);
                return failedResponse('Offset and size are required', 'OffsetRequired');
            }
            let pagination: IPaginationCommon = {
                offset : data.offset || 0,
                size: data.size || 10
            }
            const result = await FoodSupplier.aggregate(getFoodSupplierQueryByFarmId(data.farmId, pagination));
            return successResponse(result[0]);
        } catch (e) {
            return failedResponse(`Caught error ${e}`, '500');
        }
    }

    /**
    Create food supplier
     */
    @Post('/')
    public async createFoodSupplier(@Body() data: IFoodSupplier): Promise<any>{
        try {
            const farm = await Farm.findById(data.farmId);
            if (!farm) {
                return failedResponse('Không tìm thấy trang trại', 'FarmNotFound');
            }
            const foodSupplier = await FoodSupplier.find({ farmId: data.farmId, name: data.name, address: data.address });
            if (foodSupplier.length !== 0) {
                return failedResponse('Nhà cung cấp thức ăn đã tồn tại', 'UniqueName');
            }
            await new FoodSupplier(data).save();
            return successResponse('insertSuccess');
        } catch (e) {
            return failedResponse(`Caught error ${e}`, '500');
        }
    }

    /**
     * Update food supplier
     */
    @Put('/{foodSupplierId}')
    public async updateFarm(foodSupplierId: string, @Body() data: IFoodSupplier): Promise<any>{
        try {
            if (!data.name || !data.farmId) {
                this.setStatus(400);
                return failedResponse('BodyRequired', 'BodyRequired');
            }
            const foodSupplier = await FoodSupplier.find({ farmId: data.farmId, name: data.name, address: data.address });
            if (foodSupplier.length !== 0) {
                return failedResponse('Nhà cung cấp thức ăn đã tồn tại', 'UniqueName');
            }
            await FoodSupplier.updateOne({ _id: { $eq: foodSupplierId } }, data);
            return successResponse("updateSuccess");
        }
        catch (e) {
            return failedResponse(`Caught error ${e}`, '500');
        }
    }

    /**
     * Delete farm
     */
    @Delete('/{foodSupplierId}')
    public async removeFoodSupplier(foodSupplierId: string): Promise<any>{
        try {
            await FoodSupplier.deleteOne({ _id: { $eq: foodSupplierId } });
            return successResponse(true);
        } catch (e) {
            return failedResponse(`Caught error ${e}`, '500');
        }
    }

}