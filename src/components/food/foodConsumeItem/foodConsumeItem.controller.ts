import {Body, Controller, Delete, Get, Post, Put, Query, Route, Tags} from "tsoa";
import FoodConsumeItem from './foodConsumeItem.model';
import {IFoodConsumeItem, IFoodConsumeItemReq, IFoodConsumeItemUpdateDTO} from "./foodConsumeItem.types";
import {failedResponse, successResponse} from '../../../utils/http'
import { getFoodConsumeItemByFoodConsumeId, getFoodConsumeItemQuery } from "./foodConsumeItem.query";

@Route('foodConsumeItem')
@Tags('FoodConsumeItem')
export class FoodConsumeItemController extends Controller {

    /**
     * get FoodConsumeItem Query
     */
    @Get('')
    public async getAllFoodConsumeItem(@Query() foodConsumeId: string, @Query() offset?: number, @Query() size?: number): Promise<any>{
        try {
            let paging: IFoodConsumeItemReq = {
                foodConsumeId: foodConsumeId,
                offset: offset ?? 0,
                size: size ?? 10
            }
            const foods = await FoodConsumeItem.aggregate(getFoodConsumeItemQuery(paging));
            if (foods == null){
                this.setStatus(404);
                return failedResponse('Food is not found', 'FoodNotFound');
            }
            return successResponse(foods[0]);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * get FoodConsumeItem by FoodConsumeItem
     * @param FoodConsumeItemId
     */
    @Get('/{FoodConsumeItemId}')
    public async getFoodConsumeItemById(FoodConsumeItemId: string): Promise<any>{
        try {
            const foodConsumeItems = await FoodConsumeItem.aggregate(getFoodConsumeItemByFoodConsumeId(FoodConsumeItemId))
            if (foodConsumeItems == null){
                this.setStatus(404);
                return failedResponse('Food is not found', 'FoodNotFound');
            }
            return successResponse(foodConsumeItems[0]);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Create food
     * @param data
     */
    @Post()
    public async createFoodConsumeItem(@Body() data: IFoodConsumeItem): Promise<any>{
        try{
            let result = await new FoodConsumeItem(data).save();
            return successResponse(result);
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Update FoodConsumeItem
     * @param FoodConsumeItemId
     * @param data
     */
    @Put('/{FoodConsumeItemId}')
    public async updateFoodConsumeItem(FoodConsumeItemId: string, @Body() data: IFoodConsumeItemUpdateDTO): Promise<any>{
        try{
            if (await isExistFoodConsumeItems(FoodConsumeItemId)) {
                await FoodConsumeItem.findByIdAndUpdate(FoodConsumeItemId, data);
                const res = await FoodConsumeItem.findById(FoodConsumeItemId)
                return successResponse(res);
            }
            this.setStatus(404);
            return failedResponse('FoodConsumeItem is not found', 'FoodConsumeItemNotFound');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Delete FoodConsumeItem
     * @param FoodConsumeItemId
     */
    @Delete('/{FoodConsumeItemId}')
    public async deleteFoodConsumeItem(FoodConsumeItemId: string): Promise<any>{
        try{
            if (await isExistFoodConsumeItems(FoodConsumeItemId)) {
                let res = await FoodConsumeItem.findByIdAndDelete(FoodConsumeItemId);
                return successResponse(res);
            }
            this.setStatus(404);
            return failedResponse('FoodConsumeItem is not found', 'FoodConsumeItemNotFound');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }
}

const isExistFoodConsumeItems = async (FoodConsumeItemId: string): Promise<boolean> => {
    const food = await FoodConsumeItem.find({})
            .where('_id').equals(FoodConsumeItemId)
            .exec();
    return food.length > 0;
}