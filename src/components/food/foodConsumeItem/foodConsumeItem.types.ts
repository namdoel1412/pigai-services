import {IPaginationCommon} from "../../../types/common";
import { IFoodGetDTO } from "../food/food.types";
import { IFoodConsumeGetDTO } from "../foodConsume/foodConsume.types";

export interface IFoodConsumeItem {
    foodConsumeId: string
    foodId: string
    quantity: number
}

export interface IFoodConsumeItemCreateDTO {
    foodId: string
    quantity: number
}

export interface IFoodConsumeItemGetDTO {
    id: string,
    foodConsume: IFoodConsumeGetDTO,
    food: IFoodGetDTO,
    quantity: number,
}

export interface IFoodConsumeItemReq extends IPaginationCommon{
    foodConsumeId?: string
}

export interface IFoodConsumeItemUpdateDTO {
    foodConsumeId?: string,
    foodId?: string,
    quantity?: number,
}

export interface IFoodConsumeItemDetailUpdateDTO {
    id: string,
    foodConsumeId?: string,
    foodId?: string,
    quantity?: number,
}