import FoodConsumeItem from './foodConsumeItem.model';
import { IFoodConsumeItem, IFoodConsumeItemGetDTO, IFoodConsumeItemUpdateDTO } from './foodConsumeItem.types';

class FoodConsumeItemRepository{
    // static GetFoodConsumeItemById = async (foodConsumeItemId: string): Promise<IFoodConsumeItemGetDTO> => {
    //     let foodConsumeItem = await FoodConsumeItem.findOne({}).where('_id').equals(foodConsumeItemId).exec();
    //     let foodRes = {
    //         id: foodConsumeItem.id,
    //         foodConsumeId: foodConsumeItem.foodConsumeId,
    //         quantity: foodConsumeItem.quantity,
    //         foodId: foodConsumeItem.foodId
    //     }
    //     return foodRes;
    // }

    static UpdateFoodConsumeItem = async (foodConsumeItemId: string, data: IFoodConsumeItemUpdateDTO): Promise<any> => {
        if (await FoodConsumeItemRepository.isExistFoodConsumeItems(foodConsumeItemId)) {
            await FoodConsumeItem.findByIdAndUpdate(foodConsumeItemId, data);
            return true;
        }
    }

    static CreateFoodConsumeItem = async (data: IFoodConsumeItem): Promise<any> => {
        try{
            let result = await new FoodConsumeItem(data).save();
            return true;
        } catch (err) {
            return false;
        }
    }

    static CreateManyFoodConsumeItem = async (data: IFoodConsumeItem[]): Promise<any> => {
        try{
            FoodConsumeItem.create(data);
            return true;
        } catch (err) {
            return false;
        }
    }

    static isExistFoodConsumeItems = async (foodConsumeItemId: string): Promise<boolean> => {
        const foodConsumeItem = await FoodConsumeItem.find({})
                .where('_id').equals(foodConsumeItemId)
                .exec();
        return foodConsumeItem.length > 0;
    }
}

export default FoodConsumeItemRepository;