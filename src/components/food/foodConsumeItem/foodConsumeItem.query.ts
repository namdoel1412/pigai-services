import mongoose from "mongoose";
import { lookupSingleObjectFoodType, lookupSingleObjectFoodUnit } from "../food/food.query";
import { IFoodConsumeItemReq } from "./foodConsumeItem.types";

const dataDetailResponseProjectFoodConsumeItem = {
    $project: {
        id: '$_id',
        _id: 0,
        quantity: 1,
        foodConsume: '$foodconsume',
        food: '$food'
    }
};

export const lookupSingleObjectFoodConsume = (): any => (
    [
        {
            $lookup: {
                from: 'foodconsumes',
                let: {foodConsumeId: '$foodConsumeId'},
                as: 'foodconsume',
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$_id', '$$foodConsumeId']}
                        }
                    },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            name: 1
                        }
                    }
                ]
            }
        },
        {
            $unwind: {
                path: '$foodconsume',
                preserveNullAndEmptyArrays: true
            }
        }
    ]
);

export const lookupSingleObjectFood = (): any => (
    [
        {
            $lookup: {
                from: 'foods',
                let: {foodId: '$foodId'},
                as: 'food',
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$_id', '$$foodId']}
                        }
                    },
                    ...lookupSingleObjectFoodType(),
                    ...lookupSingleObjectFoodUnit(),
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            name: 1,
                            foodUnit: '$foodunit',
                            foodType: '$foodtype'
                        },
                    }
                ]
            }
        },
        {
            $unwind: {
                path: '$food',
                preserveNullAndEmptyArrays: true
            }
        }
    ]
);

export const getFoodConsumeItemQuery = (data: IFoodConsumeItemReq): Array<Record<string, any>> => [
    {
        $match: {
            foodConsumeId: { $eq: mongoose.Types.ObjectId(data.foodConsumeId) },
        }
    },
    // {
    //     $sort: { name: 1 }
    // },
    ...lookupSingleObjectFoodConsume(),
    ...lookupSingleObjectFood(),
    dataDetailResponseProjectFoodConsumeItem,
    {
        $facet: {
            count: [{$count: 'total'}],
            items: [
                {$skip: +data.offset},
                {$limit: +data.size},
            ],
        },
    },
    {
        $project: {
            items: 1,
            total: {
                $cond: {
                    if: {$eq: [{$size: '$count'}, 0]},
                    then: 0,
                    else: {$arrayElemAt: ['$count.total', 0]}
                },
            },
        },
    },
]

export const getFoodConsumeItemByFoodConsumeId = (foodConsumeItemId: string): Array<Record<string, any>> => [
    {
        $match: {
            _id: { $eq: mongoose.Types.ObjectId(foodConsumeItemId) }
        }
    },
    ...lookupSingleObjectFoodConsume(),
    ...lookupSingleObjectFood(),
    dataDetailResponseProjectFoodConsumeItem,
]