import mongoose, { Model, Document } from 'mongoose';
import { IFoodConsumeItem } from '@components/food/foodConsumeItem/foodConsumeItem.types';

interface FoodConsumeItemDocument extends IFoodConsumeItem, Document { }
interface FoodConsumeItemModel extends Model<FoodConsumeItemDocument> { }

const foodConsumeItemSchema = new mongoose.Schema<FoodConsumeItemDocument, FoodConsumeItemModel>({
    foodConsumeId: {
        type: mongoose.Types.ObjectId,
        ref: 'FoodConsume',
        required: true,
    },
    foodId: {
        type: mongoose.Types.ObjectId,
        ref: 'Food',
        required: true,
    },
    quantity: Number
})

foodConsumeItemSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {   delete ret._id  }
});

export default mongoose.model<FoodConsumeItemDocument, FoodConsumeItemModel>('FoodConsumeItem', foodConsumeItemSchema);