import FoodOrderItem from "../foodOrderItems/foodOrderItem.model";
import { IFoodOrderItem, IFoodOrderItemGetDTO, IFoodOrderItemUpdateDTO } from "./foodOrderItems.types";

class FoodOrderItemRepository{
    static GetFoodOrderItemById = async (foodOrderItemId: string): Promise<IFoodOrderItemGetDTO> => {
        let foodOrderItem = await FoodOrderItem.findOne({}).where('_id').equals(foodOrderItemId).exec();
        let foodRes = {
            id: foodOrderItem.id,
            foodOrderId: foodOrderItem.foodOrderId,
            quantity: foodOrderItem.quantity,
            foodId: foodOrderItem.foodId
        }
        return foodRes;
    }

    static UpdateFoodOrderItem = async (foodOrderItemId: string, data: IFoodOrderItemUpdateDTO): Promise<any> => {
        if (await FoodOrderItemRepository.isExistFoodOrderItems(foodOrderItemId)) {
            await FoodOrderItem.findByIdAndUpdate(foodOrderItemId, data);
            return true;
        }
    }

    static CreateFoodOrderItem = async (data: IFoodOrderItem): Promise<any> => {
        try{
            let result = await new FoodOrderItem(data).save();
            return true;
        } catch (err) {
            return false;
        }
    }

    static isExistFoodOrderItems = async (foodOrderItemId: string): Promise<boolean> => {
        const foodOrderItem = await FoodOrderItem.find({})
                .where('_id').equals(foodOrderItemId)
                .exec();
        return foodOrderItem.length > 0;
    }
}

export default FoodOrderItemRepository;