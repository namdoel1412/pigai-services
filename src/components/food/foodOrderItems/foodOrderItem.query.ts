import { lookupSingleObjectFoodAggregation } from "../../../utils/aggregation";
import mongoose from "mongoose";
import { IFoodOrderItemPagination } from "./foodOrderItems.types";


const dataDetailResponseProjectFoodOrderItem = {
    $project: {
        id: '$_id',
        _id: 0,
        name: 1,
        foodOrderId: 1,
        food: '$food',
        expirationDate: 1,
        price: 1,
        subTotal: 1,
        quantity: 1
    }
};

export const lookupSingleObjectFood = (): any => (
    [
        {
            $lookup: {
                from: 'foods',
                let: {foodId: '$foodId'},
                as: 'food',
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$_id', '$$foodId']}
                        }
                    },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            name: 1,
                            foodTypeId: 1,
                            foodUnitId: 1
                        }
                    }
                ]
            }
        },
        {
            $unwind: {
                path: '$food',
                preserveNullAndEmptyArrays: true
            }
        }
    ]
);

export const getFoodOrderItemQueryByFoodOrderId = (foodOrderId: string, data: IFoodOrderItemPagination, name?: string ): Array<Record<string, any>> => [
    {
        $match: {
            foodOrderId: {$eq: mongoose.Types.ObjectId(foodOrderId)},
            // name: new RegExp(name, 'g'),
        },
    },
    ...lookupSingleObjectFoodAggregation(),
    dataDetailResponseProjectFoodOrderItem,
    {
        $facet: {
            count: [{$count: 'total'}],
            items: [
                {$skip: +data.offset},
                {$limit: +data.size},
            ],
        },
    },
    {
        $project: {
            items: 1,
            total: {
                $cond: {
                    if: {$eq: [{$size: '$count'}, 0]},
                    then: 0,
                    else: {$arrayElemAt: ['$count.total', 0]}
                },
            },
        },
    },
]