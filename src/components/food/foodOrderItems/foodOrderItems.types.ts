import {IPaginationCommon} from "../../../types/common";
import { IFoodGetDTO } from "../food/food.types";

export interface IFoodOrderItem {
    foodOrderId: string
    foodId: string
    quantity: number,
    price?: number,
    subTotal?: number,
    expirationDate?: Date
}

export interface IFoodOrderItemUpdateDTO {
    foodOrderId?: string,
    foodId?: string,
    quantity?: number,
    price?: number,
    subTotal?: number,
    expirationDate?: Date
}

export interface IFoodOrderItemDetailUpdateDTO {
    id: string,
    foodOrderId?: string,
    foodId?: string,
    quantity?: number,
    price?: number,
    subTotal?: number,
    expirationDate?: Date
}

export interface IFoodOrderItemPagination extends IPaginationCommon{}

export interface IFoodOrderAndPaginationReq extends IFoodOrderItemPagination{
    foodOrderId: string
}

export interface IFoodOrderItemGetDTO {
    id: string
    foodOrderId: string
    foodId: string
    quantity: number,
    price?: number,
    subTotal?: number,
    expirationDate?: Date
}

export interface IFoodOrderItemCreateDTO{
    foodId: string,
    quantity: number,
    price?: number,
    subTotal?: number,
    expirationDate?: Date
}