import FoodOrderItem from './foodOrderItem.model';
import { IFoodOrderAndPaginationReq, IFoodOrderItem, IFoodOrderItemUpdateDTO } from './foodOrderItems.types';
import { failedResponse, successResponse } from '../../../utils/http'
import {Body, Controller, Delete, Get, Post, Put, Query, Route, Tags} from 'tsoa';
import { IPaginationCommon } from '@type/common';
import { getFoodOrderItemQueryByFoodOrderId } from './foodOrderItem.query';
import FoodOrderItemRepository from './foodOrderItemRepository';
import FoodRepository from '../food/foodRepository';

@Route('foodOrderItem')
@Tags('FoodOrderItem')
export class FoodOrderItemController extends Controller{

    /**
    Get all FoodOrderItem of Food
     */
    @Post('/foodOrder')
    public async getAllFoodOrderItemOfAFoodOrder(@Body() data: IFoodOrderAndPaginationReq): Promise<any> {
        try {
            if (data.offset < 0 || data.size <= 0) {
                this.setStatus(400);
                return failedResponse('Offset and size are required', 'OffsetRequired');
            }
            let pagination: IPaginationCommon = {
                offset : data.offset || 0,
                size: data.size || 10
            }
            const result = await FoodOrderItem.aggregate(getFoodOrderItemQueryByFoodOrderId(data.foodOrderId, pagination));
            return successResponse(result[0]);
        } catch (e) {
            return failedResponse(`Caught error ${e}`, '500');
        }
    }


    /**
     * get foodOrderItem by foodOrderItemId
     * @param foodOrderItemId
     */
    @Get('/{foodOrderItemId}')
    public async getFoodOrderItemById(foodOrderItemId: string): Promise<any>{
        try {
            let foods = await FoodOrderItemRepository.GetFoodOrderItemById(foodOrderItemId);
            if (foods == null){
                this.setStatus(404);
                return failedResponse('Food Order Item is not found', 'FoodOrderItemNotFound');
            }
            return successResponse(foods);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Create foodOrderItem
     * @param data
     */
    @Post()
    public async createFoodOrderItem(@Body() data: IFoodOrderItem): Promise<any>{
        try{
            let result = await new FoodOrderItem(data).save();
            return successResponse(result);
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Update foodOrderItem
     * @param foodId
     * @param data
     */
    @Put('/{foodOrderItemId}')
    public async updateFood(foodOrderItemId: string, @Body() data: IFoodOrderItemUpdateDTO): Promise<any>{
        try{
            if (await isExistFoodOrderItems(foodOrderItemId)) {
                await FoodOrderItem.findByIdAndUpdate(foodOrderItemId, data);
                const foodOrderItem = await FoodOrderItem.findById(foodOrderItemId)
                return successResponse(foodOrderItem);
            }
            this.setStatus(404);
            return failedResponse('Food Order Item is not found', 'FoodOrderItemNotFound');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Delete foodOrderItem
     * @param foodId
     */
    @Delete('/{foodOrderItemId}')
    public async deleteFoodOrderItem(foodOrderItemId: string): Promise<any>{
        try{
            if (await isExistFoodOrderItems(foodOrderItemId)) {
                let res = await FoodOrderItem.findByIdAndDelete(foodOrderItemId);
                return successResponse(res);
            }
            this.setStatus(404);
            return failedResponse('Food Order Item is not found', 'FoodOrderItemNotFound');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }
}

const isExistFoodOrderItems = async (foodOrderItemId: string): Promise<boolean> => {
    const foodOrderItem = await FoodOrderItem.find({})
            .where('_id').equals(foodOrderItemId)
            .exec();
    return foodOrderItem.length > 0;
}