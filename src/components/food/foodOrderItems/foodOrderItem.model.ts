import mongoose, { Model, Document } from 'mongoose';
import { IFoodOrderItem } from '@components/food/foodOrderItems/foodOrderItems.types';

interface FoodOrderItemDocument extends IFoodOrderItem, Document { }
interface FoodOrderItemModel extends Model<FoodOrderItemDocument> { }

const foodOrderItemSchema = new mongoose.Schema<FoodOrderItemDocument, FoodOrderItemModel>({
    quantity: Number,
    foodOrderId: {
        type: mongoose.Types.ObjectId,
        ref: 'FoodOrder',
        required: true
    },
    foodId: {
        type: mongoose.Types.ObjectId,
        ref: 'Food',
        required: true
    },
    expirationDate: {
        type: Date,
        require: false
    },
    price: {
        type: Number,
        required: false,
        default: 0
    },
    subTotal: {
        type: Number,
        required: false,
        default: 0
    }
})

foodOrderItemSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {   delete ret._id  }
});

export default mongoose.model<FoodOrderItemDocument, FoodOrderItemModel>('FoodOrderItem', foodOrderItemSchema);