export interface IPigFoodConsume {
    name: string
    date: string
    penFoodConsumeId: string
    pigId: string
}  