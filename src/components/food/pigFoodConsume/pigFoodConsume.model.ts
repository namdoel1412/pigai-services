import mongoose, { Model, Document } from 'mongoose';
import { IPigFoodConsume } from '@components/food/pigFoodConsume/pigFoodConsume.types';

interface IPigFoodConsumeDocument extends IPigFoodConsume, Document { }
interface IPigFoodConsumeModel extends Model<IPigFoodConsumeDocument> { }

const pigFoodConsumeSchema = new mongoose.Schema<IPigFoodConsumeDocument, IPigFoodConsumeModel>({
    name: String,
    date: String,
    penFoodConsumeId: {
        type: mongoose.Types.ObjectId,
        ref: 'PenFoodConsume',
        required: true
    },
    pigId: {
        type: mongoose.Types.ObjectId,
        ref: 'Pig',
        required: true
    },
    userId: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
        required: true
    }
})

export default mongoose.model<IPigFoodConsumeDocument, IPigFoodConsumeModel>('PigFoodConsume', pigFoodConsumeSchema);