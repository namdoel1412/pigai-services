import {Controller, Body, Get, Post, Put, Query, Route, Tags} from "tsoa";
import FoodType from "../foodType/foodType.model";
import {failedResponse, successResponse} from "../../../utils/http";
import {IFoodType} from "../foodType/foodType.types";
import {getAllFoodTypeQuery} from "../foodType/foodType.queries";

@Route('foodTypes')
@Tags('FoodType')
export class FoodTypeController extends Controller {

    /*
    Get list food types
     */
    @Get('?')
    public async getListFoodType(@Query() name?: string): Promise<any>{
        try {
            const result = await FoodType.aggregate(getAllFoodTypeQuery(name));
            return successResponse(result);
        }
        catch (e) {
            return failedResponse(`Caught error ${e}`, '500');
        }
    }

    /**
     * Create food type
     * @param data
     */
    @Post('/')
    public async create(@Body() data: IFoodType): Promise<any>{
        try{
            if (await isExistFoodType(data)) {
                this.setStatus(400);
                return failedResponse('Tên loại thức ăn đã tồn tại', 'UniqueName');
            }
            await new FoodType(data).save();
            return successResponse('insertSuccess');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Update food type
     * @param foodTypeId
     * @param data
     */
    @Put('/{foodTypeId}')
    public async update(foodTypeId: string, @Body() data: IFoodType): Promise<any>{
        try{
            if (await isExistFoodType(data)) {
                this.setStatus(400);
                return failedResponse('Tên loại thức ăn đã tồn tại', 'UniqueName');
            }
            const foodType = await FoodType.findByIdAndUpdate(foodTypeId, data);
            if (!foodType) {
                this.setStatus(404);
                return failedResponse('Không tìm thấy loại thức ăn cần tìm', 'FoodTypeNotFound');
            }
            return successResponse('updateSuccess');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }
}

const isExistFoodType = async (input: IFoodType): Promise<boolean> => {
    const { name } = input;
    return (await FoodType.find({ name })).length > 0;
}