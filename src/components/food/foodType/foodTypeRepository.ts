import { IFoodType, IFoodTypeGetDTO } from "./foodType.types";
import FoodType from "../foodType/foodType.model";

class FoodTypeRepository{
    static GetFoodTypeById = async (foodTypeId: string): Promise<IFoodTypeGetDTO> => {
        const foodType = await FoodType.findOne({}).where('_id').equals(foodTypeId).exec();
        let res: IFoodTypeGetDTO = {
            id: foodType.id,
            name: foodType.name
        };
        return res;
    }
}

export default FoodTypeRepository;