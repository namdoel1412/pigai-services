export interface IFoodType {
    name: string
}

export interface IFoodTypeGetDTO{
    id: string,
    name: string
}