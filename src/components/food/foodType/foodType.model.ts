import mongoose, { Model, Document } from 'mongoose';
import { IFoodType } from '../foodType/foodType.types';

interface FoodTypeDocument extends IFoodType, Document { }
interface FoodTypeModel extends Model<FoodTypeDocument> { }

const foodTypeSchema = new mongoose.Schema<FoodTypeDocument, FoodTypeModel>({
    name: {
        type: String,
        required: true
    },
})

foodTypeSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) { delete ret._id }
});

export default mongoose.model<FoodTypeDocument, FoodTypeModel>('FoodType', foodTypeSchema);