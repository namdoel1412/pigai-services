export const getAllFoodUnitQuery = (name: string): any => [
    {
        $match: {
            name: new RegExp(name, 'g'),
        }
    },
    {
        $project: {
            _id: 0,
            id: '$_id',
            name: 1,
        }
    }
];