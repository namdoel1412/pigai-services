import {Controller, Body, Get, Post, Put, Query, Route, Tags} from "tsoa";
import FoodUnit from "../foodUnit/foodUnit.model";
import {failedResponse, successResponse} from "../../../utils/http";
import {getAllFoodUnitQuery} from "../foodUnit/foodUnit.queries";
import {IFoodUnit} from "../foodUnit/foodUnit.types";

@Route('foodUnits')
@Tags('FoodUnit')
export class FoodUnitController extends Controller {

    /*
    Get list food units
     */
    @Get('?')
    public async getListFoodUnit(@Query() name?: string): Promise<any>{
        try {
            const result = await FoodUnit.aggregate(getAllFoodUnitQuery(name));
            return successResponse(result);
        }
        catch (e) {
            return failedResponse(`Caught error ${e}`, '500');
        }
    }

    /**
     * Create food unit
     * @param data
     */
    @Post('/')
    public async create(@Body() data: IFoodUnit): Promise<any>{
        try{
            if (await isExistFoodUnit(data)) {
                this.setStatus(400);
                return failedResponse('Tên đơn vị thực phẩm đã tồn tại', 'UniqueName');
            }
            await new FoodUnit(data).save();
            return successResponse('insertSuccess');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Update food unit
     * @param foodUnitId
     * @param data
     */
    @Put('/{foodUnitId}')
    public async update(foodUnitId: string, @Body() data: IFoodUnit): Promise<any>{
        try{
            if (await isExistFoodUnit(data)) {
                this.setStatus(400);
                return failedResponse('Tên đơn vị thực phẩm đã tồn tại', 'UniqueName');
            }
            const foodUnit = await FoodUnit.findByIdAndUpdate(foodUnitId, data);
            if (!foodUnit) {
                this.setStatus(404);
                return failedResponse('Không tìm thấy đơn vị thực phẩm cần tìm', 'FoodUnitNotFound');
            }
            return successResponse('updateSuccess');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }
}

const isExistFoodUnit = async (input: IFoodUnit): Promise<boolean> => {
    const { name } = input;
    return (await FoodUnit.find({ name })).length > 0;
}