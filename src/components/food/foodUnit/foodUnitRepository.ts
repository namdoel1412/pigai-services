import FoodUnit from "../foodUnit/foodUnit.model";
import { IFoodUnit, IFoodUnitGetDTO } from "./foodUnit.types";

class FoodUnitRepository{
    static FindGoodUnitById = async (foodUnitId: string): Promise<IFoodUnitGetDTO> => {
        let foodUnit = await FoodUnit.findOne({}).where('_id').equals(foodUnitId).exec();
        let foodRes: IFoodUnitGetDTO = {
            id: foodUnit.id,
            name: foodUnit.name
        }
        return foodRes;
    }
}

export default FoodUnitRepository;