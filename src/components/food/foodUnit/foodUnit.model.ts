import mongoose, { Model, Document } from 'mongoose';
import { IFoodUnit } from '../foodUnit/foodUnit.types';

interface FoodUnitDocument extends IFoodUnit, Document { }
interface FoodUnitModel extends Model<FoodUnitDocument> { }

const foodUnitSchema = new mongoose.Schema<FoodUnitDocument, FoodUnitModel>({
    name: {
        type: String,
        required: true
    },
})

foodUnitSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) { delete ret._id }
});

export default mongoose.model<FoodUnitDocument, FoodUnitModel>('FoodUnit', foodUnitSchema);