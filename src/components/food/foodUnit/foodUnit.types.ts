export enum EInitialFoodUnit {
    Kilogram = 'Kg',
    Mililit = 'ml',
    Lit = 'l',
    Bag = 'Bao',
}

export interface IFoodUnit {
    name: string
}

export interface IFoodUnitGetDTO{
    id: string,
    name: string
}
