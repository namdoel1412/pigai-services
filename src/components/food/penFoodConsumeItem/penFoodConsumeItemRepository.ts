import PenFoodConsumeItem from './penFoodConsumeItem.model';
import { IPenFoodConsumeItem, IPenFoodConsumeItemGetDTO, IPenFoodConsumeItemUpdateDTO } from './penFoodConsumeItem.types';

class PenFoodConsumeItemRepository{
    static GetPenFoodConsumeItemById = async (penFoodConsumeItemId: string): Promise<IPenFoodConsumeItemGetDTO> => {
        let penFoodConsumeItem = await PenFoodConsumeItem.findOne({}).where('_id').equals(penFoodConsumeItemId).exec();
        let foodRes = {
            id: penFoodConsumeItem.id,
            penFoodConsumeId: penFoodConsumeItem.penFoodConsumeId,
            quantity: penFoodConsumeItem.quantity,
            foodId: penFoodConsumeItem.foodId
        }
        return foodRes;
    }

    static UpdatePenFoodConsumeItem = async (penFoodConsumeItemId: string, data: IPenFoodConsumeItemUpdateDTO): Promise<any> => {
        if (await PenFoodConsumeItemRepository.isExistPenFoodConsumeItems(penFoodConsumeItemId)) {
            await PenFoodConsumeItem.findByIdAndUpdate(penFoodConsumeItemId, data);
            return true;
        }
    }

    static CreatePenFoodConsumeItem = async (data: IPenFoodConsumeItem): Promise<any> => {
        try{
            let result = await new PenFoodConsumeItem(data).save();
            return true;
        } catch (err) {
            return false;
        }
    }

    static CreateManyPenFoodConsumeItem = async (data: IPenFoodConsumeItem[]): Promise<any> => {
        try{
            await PenFoodConsumeItem.create(data);
            return true;
        } catch (err) {
            return false;
        }
    }

    static isExistPenFoodConsumeItems = async (PenFoodConsumeItemId: string): Promise<boolean> => {
        const penFoodConsumeItem = await PenFoodConsumeItem.find({})
                .where('_id').equals(PenFoodConsumeItemId)
                .exec();
        return penFoodConsumeItem.length > 0;
    }
}

export default PenFoodConsumeItemRepository;