import PenFoodConsumeItem from './penFoodConsumeItem.model';
import { failedResponse, successResponse } from '../../../utils/http'
import {Body, Controller, Delete, Get, Post, Put, Query, Route, Tags} from 'tsoa';
import { IPenFoodConsumeAndPaginationReq, IPenFoodConsumeItem, IPenFoodConsumeItemUpdateDTO } from './penFoodConsumeItem.types';
import { getPenFoodConsumeItemQueryByPenFoodConsumeId } from './penFoodConsumeItem.query';
import PenFoodConsumeItemRepository from './penFoodConsumeItemRepository';

@Route('penFoodConsumeItem')
@Tags('PenFoodConsumeItem')
export class PenFoodConsumeItemController extends Controller{

    /**
    Get all PenFoodConsumeItem of Food
     */
    @Post('/penFoodConsume')
    public async getAllPenFoodConsumeItemOfAFoodOrder(@Body() data: IPenFoodConsumeAndPaginationReq): Promise<any> {
        try {
            if (data.offset < 0 || data.size <= 0) {
                this.setStatus(400);
                return failedResponse('Offset and size are required', 'OffsetRequired');
            }
            if (data.size <= 0){
                data.size = 10;
            }
            const result = await PenFoodConsumeItem.aggregate(getPenFoodConsumeItemQueryByPenFoodConsumeId(data));
            return successResponse(result[0]);
        } catch (e) {
            return failedResponse(`Caught error ${e}`, '500');
        }
    }


    /**
     * get PenFoodConsumeItem by PenFoodConsumeItemId
     * @param PenFoodConsumeItemId
     */
    @Get('/{penFoodConsumeItemId}')
    public async getPenFoodConsumeItemById(penFoodConsumeItemId: string): Promise<any>{
        try {
            let foods = await PenFoodConsumeItemRepository.GetPenFoodConsumeItemById(penFoodConsumeItemId);
            if (foods == null){
                this.setStatus(404);
                return failedResponse('Food Order Item is not found', 'PenFoodConsumeItemNotFound');
            }
            return successResponse(foods);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Create PenFoodConsumeItem
     * @param data
     */
    @Post()
    public async createPenFoodConsumeItem(@Body() data: IPenFoodConsumeItem): Promise<any>{
        try{
            let result = await new PenFoodConsumeItem(data).save();
            return successResponse(result);
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Update PenFoodConsumeItem
     * @param foodId
     * @param data
     */
    @Put('/{penFoodConsumeItemId}')
    public async updateFood(penFoodConsumeItemId: string, @Body() data: IPenFoodConsumeItemUpdateDTO): Promise<any>{
        try{
            if (await PenFoodConsumeItemRepository.isExistPenFoodConsumeItems(penFoodConsumeItemId)) {
                await PenFoodConsumeItem.findByIdAndUpdate(penFoodConsumeItemId, data);
                const penFoodConsumeItem = await PenFoodConsumeItem.findById(penFoodConsumeItemId)
                return successResponse(penFoodConsumeItem);
            }
            this.setStatus(404);
            return failedResponse('Food Order Item is not found', 'PenFoodConsumeItemNotFound');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Delete PenFoodConsumeItem
     * @param foodId
     */
    @Delete('/{penFoodConsumeItemId}')
    public async deletePenFoodConsumeItem(penFoodConsumeItemId: string): Promise<any>{
        try{
            if (await PenFoodConsumeItemRepository.isExistPenFoodConsumeItems(penFoodConsumeItemId)) {
                let res = await PenFoodConsumeItem.findByIdAndDelete(penFoodConsumeItemId);
                return successResponse(res);
            }
            this.setStatus(404);
            return failedResponse('Food Order Item is not found', 'PenFoodConsumeItemNotFound');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }
}