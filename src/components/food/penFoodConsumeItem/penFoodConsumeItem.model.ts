import mongoose, { Model, Document } from 'mongoose';
import { IPenFoodConsumeItem } from '@components/food/penFoodConsumeItem/penFoodConsumeItem.types';

interface PenFoodConsumeItemDocument extends IPenFoodConsumeItem, Document { }
interface PenFoodConsumeItemModel extends Model<PenFoodConsumeItemDocument> { }

const penFoodConsumeItemSchema = new mongoose.Schema<PenFoodConsumeItemDocument, PenFoodConsumeItemModel>({
    penFoodConsumeId: {
        type: mongoose.Types.ObjectId,
        ref: 'PenFoodConsume',
        required: true,
    },
    foodId: {
        type: mongoose.Types.ObjectId,
        ref: 'Food',
        required: true,
    },
    foodSupplierId: {
        type: mongoose.Types.ObjectId,
        ref: 'FoodSupplier',
        require: true
    },
    quantity: {
        type: Number,
        require: true
    }
})

penFoodConsumeItemSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {   delete ret._id  }
});

export default mongoose.model<PenFoodConsumeItemDocument, PenFoodConsumeItemModel>('PenFoodConsumeItem', penFoodConsumeItemSchema);