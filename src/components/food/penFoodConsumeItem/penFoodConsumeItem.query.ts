import { lookupSingleObjectFoodAggregation } from "../../../utils/aggregation";
import mongoose from "mongoose";
import { IPenFoodConsumeAndPaginationReq } from "./penFoodConsumeItem.types";


const dataDetailResponseProjectPenFoodConsumeItem = {
    $project: {
        id: '$_id',
        _id: 0,
        penFoodConsumeId: 1,
        food: '$food',
        quantity: 1
    }
};

export const getPenFoodConsumeItemQueryByPenFoodConsumeId = (data: IPenFoodConsumeAndPaginationReq): Array<Record<string, any>> => [
    {
        $match: {
            penFoodConsumeId: {$eq: mongoose.Types.ObjectId(data.penFoodConsumeId)},
            // name: new RegExp(name, 'g'),
        },
    },
    ...lookupSingleObjectFoodAggregation(),
    dataDetailResponseProjectPenFoodConsumeItem,
    {
        $facet: {
            count: [{$count: 'total'}],
            items: [
                {$skip: +data.offset},
                {$limit: +data.size},
            ],
        },
    },
    {
        $project: {
            items: 1,
            total: {
                $cond: {
                    if: {$eq: [{$size: '$count'}, 0]},
                    then: 0,
                    else: {$arrayElemAt: ['$count.total', 0]}
                },
            },
        },
    },
]