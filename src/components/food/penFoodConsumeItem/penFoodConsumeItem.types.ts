import {IPaginationCommon} from "../../../types/common";

export interface IPenFoodConsumeItem {
    penFoodConsumeId: string
    foodId: string
    foodSupplierId: string
    quantity: number
}

export interface IPenFoodConsumeAndPaginationReq extends IPaginationCommon{
    penFoodConsumeId: string
}

export interface IListPenFoodConsumeAndPaginationReq extends IPaginationCommon{
    penFoodConsumeId: string
}

export interface IPenFoodConsumeItemGetDTO {
    id: string
    penFoodConsumeId: string
    foodId: string
    foodSupplierId?: string
    quantity: number
}

export interface IPenFoodConsumeItemUpdateDTO{
    penFoodConsumeId?: string,
    foodId?: string,
    foodSupplierId?: string,
    quantity?: number
}

export interface IPenFoodConsumeItemCreateDTO {
    foodId: string,
    foodSupplierId: string,
    quantity: number
}

export interface IPenFoodConsumeItemDetailUpdateDTO {
    id: string,
    penFoodConsumeId?: string,
    foodId?: string,
    foodSupplierId?: string,
    quantity?: number,
}