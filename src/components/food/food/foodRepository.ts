import Food from "../food/Food.model";
import FoodTypeRepository from "../foodType/foodTypeRepository";
import FoodUnitRepository from "../foodUnit/foodUnitRepository";
import { IFoodGetDTO } from "./food.types";

class FoodRepository{
    static GetFoodById = async (foodId: string): Promise<IFoodGetDTO> => {
        const foods = await Food.findOne({})
            .where('_id').equals(foodId)
            .exec();
            if (foods == null){
                return null;
            }
            let foodRes: IFoodGetDTO = {
                id: foods?.id,
                name: foods?.name,
                foodUnit: await FoodUnitRepository.FindGoodUnitById(foods.foodUnitId),
                foodType: await FoodTypeRepository.GetFoodTypeById(foods.foodTypeId)
            } 
            return foodRes;
    }
}

export default FoodRepository;