import mongoose, { Model, Document } from 'mongoose';
import { IFood } from '@components/food/food/food.types';

interface FoodDocument extends IFood, Document { }
interface FoodModel extends Model<FoodDocument> { }

const foodSchema = new mongoose.Schema<FoodDocument, FoodModel>({
    name: {
        type: String,
        required: true
    },
    foodTypeId: { type: mongoose.Types.ObjectId, ref: 'FoodType', require: true},
    foodUnitId: { type: mongoose.Types.ObjectId, ref: 'FoodUnit', require: true}
})

foodSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {   delete ret._id  }
});

export default mongoose.model<FoodDocument, FoodModel>('Food', foodSchema);