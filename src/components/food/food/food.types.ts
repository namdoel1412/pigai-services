import { IFoodTypeGetDTO } from "../foodType/foodType.types";
import { IFoodUnitGetDTO } from "../foodUnit/foodUnit.types";
import {IPaginationCommon} from "../../../types/common";

export interface IFood {
    name: string
    foodTypeId: string
    foodUnitId: string
}

export interface IFoodUpdateDTO {
    name?: string
    foodTypeId?: string
    foodUnitId?: string
}

export interface IFoodGetDTO {
    id: string,
    name: string,
    foodType: IFoodTypeGetDTO,
    foodUnit: IFoodUnitGetDTO
}

export interface IFoodReq extends IPaginationCommon{
    name: string
}