import Food from './food.model';
import { IFood, IFoodGetDTO, IFoodReq, IFoodUpdateDTO } from './food.types';
import { failedResponse, successResponse } from '../../../utils/http'
import {Body, Controller, Delete, Get, Post, Put, Query, Route, Tags} from 'tsoa';
import FoodUnitRepository from '../foodUnit/foodUnitRepository';
import FoodTypeRepository from '../foodType/foodTypeRepository';
import { getFoodQuery } from './food.query';

@Route('food')
@Tags('Food')
export class FoodController extends Controller{

    @Get('')
    public async getAllFood(@Query() name?: string, @Query() offset?: number, @Query() size?: number): Promise<any>{
        try {
            let paging: IFoodReq = {
                name: name ?? "",
                offset: offset ?? 0,
                size: size ?? 10
            }
            const foods = await Food.aggregate(getFoodQuery(paging));
            if (foods == null){
                this.setStatus(404);
                return failedResponse('Food is not found', 'FoodNotFound');
            }
            return successResponse(foods[0]);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * get food by foodId
     * @param foodId
     */
    @Get('/{foodId}')
    public async getFoodById(foodId: string): Promise<any>{
        try {
            const foods = await Food.findOne({})
            .where('_id').equals(foodId)
            .exec();
            if (foods == null){
                this.setStatus(404);
                return failedResponse('Food is not found', 'FoodNotFound');
            }
            let foodRes: IFoodGetDTO = {
                id: foods.id,
                name: foods.name,
                foodUnit: await FoodUnitRepository.FindGoodUnitById(foods.foodUnitId),
                foodType: await FoodTypeRepository.GetFoodTypeById(foods.foodTypeId)
            } 
            return successResponse(foodRes);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Create food
     * @param data
     */
    @Post()
    public async createFood(@Body() data: IFood): Promise<any>{
        try{
            let result = await new Food(data).save();
            return successResponse(result);
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Update food
     * @param foodId
     * @param data
     */
    @Put('/{foodId}')
    public async updateFood(foodId: string, @Body() data: IFoodUpdateDTO): Promise<any>{
        try{
            if (await isExistFoods(foodId)) {
                await Food.findByIdAndUpdate(foodId, data);
                const food = await Food.findById(foodId)
                return successResponse(food);
            }
            this.setStatus(404);
            return failedResponse('Food is not found', 'FoodNotFound');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Delete food
     * @param foodId
     */
    @Delete('/{foodId}')
    public async deleteFood(foodId: string): Promise<any>{
        try{
            if (await isExistFoods(foodId)) {
                let res = await Food.findByIdAndDelete(foodId);
                return successResponse(res);
            }
            this.setStatus(404);
            return failedResponse('Food is not found', 'FoodNotFound');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }
}

const isExistFoods = async (foodId: string): Promise<boolean> => {
    const food = await Food.find({})
            .where('_id').equals(foodId)
            .exec();
    return food.length > 0;
}