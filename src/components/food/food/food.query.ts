import mongoose from "mongoose";
import { IFoodReq } from "./food.types";

const dataDetailResponseProjectFood = {
    $project: {
        id: '$_id',
        _id: 0,
        name: 1,
        foodUnit: '$foodunit',
        foodType: '$foodtype'
    }
};

export const lookupSingleObjectFoodUnit = (): any => (
    [
        {
            $lookup: {
                from: 'foodunits',
                let: {foodUnitId: '$foodUnitId'},
                as: 'foodunit',
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$_id', '$$foodUnitId']}
                        }
                    },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            name: 1
                        }
                    }
                ]
            }
        },
        {
            $unwind: {
                path: '$foodunit',
                preserveNullAndEmptyArrays: true
            }
        }
    ]
);

export const lookupSingleObjectFoodType = (): any => (
    [
        {
            $lookup: {
                from: 'foodtypes',
                let: {foodTypeId: '$foodTypeId'},
                as: 'foodtype',
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$_id', '$$foodTypeId']}
                        }
                    },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            name: 1
                        }
                    }
                ]
            }
        },
        {
            $unwind: {
                path: '$foodtype',
                preserveNullAndEmptyArrays: true
            }
        }
    ]
);

export const getFoodQuery = (data: IFoodReq): Array<Record<string, any>> => [
    {
        $match: {
            _id: {$ne: ""},
            name: new RegExp(data.name, 'i'),
        },
    },
    ...lookupSingleObjectFoodUnit(),
    ...lookupSingleObjectFoodType(),
    dataDetailResponseProjectFood,
    {
        $facet: {
            count: [{$count: 'total'}],
            items: [
                {$skip: +data.offset},
                {$limit: +data.size},
            ],
        },
    },
    {
        $project: {
            items: 1,
            total: {
                $cond: {
                    if: {$eq: [{$size: '$count'}, 0]},
                    then: 0,
                    else: {$arrayElemAt: ['$count.total', 0]}
                },
            },
        },
    },
]