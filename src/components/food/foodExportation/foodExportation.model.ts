import mongoose, { Model, Document } from 'mongoose';
import { IFoodExportation } from '@components/food/foodExportation/foodExportation.types';

interface FoodExportationDocument extends IFoodExportation, Document { }
interface FoodExportationModel extends Model<FoodExportationDocument> { }

const foodExportationSchema = new mongoose.Schema<FoodExportationDocument, FoodExportationModel>({
    farmId: {
        type: mongoose.Types.ObjectId,
        ref: 'Farm',
        required: true
    },
    foodSupplierId: {
        type: mongoose.Types.ObjectId,
        ref: 'FoodSupplier',
        required: true,
    },
    foodId: {
        type: mongoose.Types.ObjectId,
        ref: 'Food',
        required: true,
    },
    penId: {
        type: mongoose.Types.ObjectId,
        ref: 'Pen',
        required: true,
    },
    quantity: {
        type: Number,
        required: true,
        default: 0
    }
})

foodExportationSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {   delete ret._id  }
});

export default mongoose.model<FoodExportationDocument, FoodExportationModel>('FoodExportation', foodExportationSchema);