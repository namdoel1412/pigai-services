export interface IFoodExportation{
    foodSupplierId: string,
    foodId: string,
    penId: string,
    quantity: number
}

export interface IFoodExportationGetDTO{
    id: string,
    foodSupplierId: string,
    foodId: string,
    penId: string,
    quantity: number
}

export interface IFoodExportationReq{
    foodSupplierId: string,
    foodId: string,
    penId: string,
    quantity: number
}

export interface IFoodExportationUpdateDTO{
    farmId: string,
    foodSupplierId: string,
    foodId: string,
    penId: string,
    quantity: number
}