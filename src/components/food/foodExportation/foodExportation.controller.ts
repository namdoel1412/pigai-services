import {Body, Controller, Delete, Get, Post, Put, Query, Route, Tags} from "tsoa";
import foodExportation from './foodExportation.model';
import {failedResponse, successResponse} from '../../../utils/http';
import FoodExportationRepository from "./foodExportationRepository";
import { IFoodExportationUpdateDTO } from "./foodExportation.types";

@Route('foodExportation')
@Tags('FoodExportation')
export class foodExportationController extends Controller {

    /**
     * get foodExportation Query
     */
    @Put('')
    public async updateFoodExportation(@Body() data: IFoodExportationUpdateDTO): Promise<any>{
        try {
            let res = await FoodExportationRepository.UpdateFoodExportation(data)
            return successResponse(res);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }
}