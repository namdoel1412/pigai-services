import FoodExportation from './foodExportation.model';
import { IFoodExportation, IFoodExportationUpdateDTO } from './foodExportation.types';

class FoodExportationRepository{
    // static GetfoodExportationById = async (foodExportationId: string): Promise<IfoodExportationGetDTO> => {
    //     let foodExportation = await foodExportation.findOne({}).where('_id').equals(foodExportationId).exec();
    //     let foodRes = {
    //         id: foodExportation.id,
    //         foodConsumeId: foodExportation.foodConsumeId,
    //         quantity: foodExportation.quantity,
    //         foodId: foodExportation.foodId
    //     }
    //     return foodRes;
    // }

    static UpdateFoodExportation = async (data: IFoodExportationUpdateDTO): Promise<any> => {
        try{
            let entity = await FoodExportationRepository.isExistFoodExportations(data.farmId, data.foodSupplierId, data.foodId, data.penId);
            if (entity != null){
                entity.quantity+=data.quantity ?? 0;
                console.log(entity)
                let res = await FoodExportation.findByIdAndUpdate(entity.id, entity);
                return entity;
            }
            else{
                let res = await this.CreateFoodExportation({
                    foodId: data.foodId,
                    foodSupplierId: data.foodSupplierId,
                    penId: data.penId,
                    quantity: data.quantity
                })
                console.log('Not found')
                console.log(res)
            }
            return true;
        }
        catch (err) {
            console.log(`Error: ${err}`);
            return false;
        }
    }

    static CreateFoodExportation = async (data: IFoodExportation): Promise<any> => {
        try{
            let result = await new FoodExportation(data).save();
            return true;
        } catch (err) {
            return false;
        }
    }

    static CreateManyFoodExportation = async (data: IFoodExportation[]): Promise<any> => {
        try{
            FoodExportation.create(data);
            return true;
        } catch (err) {
            console.log(err)
            return false;
        }
    }

    static isExistFoodExportations = async (farmId: string, foodSupplierId: string, foodId: string, penId: string): Promise<any> => {
        const foodExportation = await FoodExportation.findOne({})
                .where('farmId').equals(farmId)
                .where('foodSupplierId').equals(foodSupplierId)
                .where('foodId').equals(foodId)
                .where('penId').equals(penId)
                .exec();
        return foodExportation ?? null;
    }
}

export default FoodExportationRepository;