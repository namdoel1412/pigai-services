import mongoose, { Model, Document } from 'mongoose';
import { IPigFoodConsumeItem } from '@components/food/pigFoodConsumeItem/pigFoodConsumeItem.types';

interface PigFoodConsumeItemDocument extends IPigFoodConsumeItem, Document { }
interface PigFoodConsumeItemModel extends Model<PigFoodConsumeItemDocument> { }

const pigFoodConsumeItemSchema = new mongoose.Schema<PigFoodConsumeItemDocument, PigFoodConsumeItemModel>({
    pigFoodConsumeId: {
        type: mongoose.Types.ObjectId,
        ref: 'PigFoodConsume',
        required: true,
    },
    foodId: {
        type: mongoose.Types.ObjectId,
        ref: 'Food',
        required: true,
    },
    quantity: Number
})

export default mongoose.model<PigFoodConsumeItemDocument, PigFoodConsumeItemModel>('PigFoodConsumeItem', pigFoodConsumeItemSchema);