export interface IPigFoodConsumeItem {
    pigFoodConsumeId: string
    foodId: string
    quantity: number
}