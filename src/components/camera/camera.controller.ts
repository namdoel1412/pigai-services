import Camera from './camera.model';
import { ICamera, ICameraUpdateDTO } from './camera.types';
import { failedResponse, successResponse } from '../../utils/http'
import {Body, Controller, Delete, Get, Post, Put, Query, Route, Tags} from 'tsoa';

@Route('camera')
@Tags('Camera')
export class CameraController extends Controller{

    @Get('/pen')
    public async getAllCamInPen(@Query() penId?: string): Promise<any>{
        try {
            const cameras = await Camera.find({})
            .where('penId').equals(penId)
            .exec();
            return successResponse(cameras);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    @Get('/{cameraId}')
    public async getCamByCamId(cameraId: string): Promise<any>{
        try {
            const cameras = await Camera.findOne({})
            .where('_id').equals(cameraId)
            .exec();
            if (cameras == null){
                this.setStatus(404);
                return failedResponse('Không tìm thấy camera', 'CameraNotFound');
            }
            return successResponse(cameras);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Create event type
     * @param data
     */
    @Post()
    public async create(@Body() data: ICamera): Promise<any>{
        try{
            await new Camera(data).save();
            return successResponse('insertSuccess');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Update event type
     * @param eventTypeId
     * @param data
     */
    @Put('/{cameraId}')
    public async update(cameraId: string, @Body() data: ICameraUpdateDTO): Promise<any>{
        try{
            if (await isExistCameras(cameraId)) {
                await Camera.findByIdAndUpdate(cameraId, data);
                const camera = await Camera.findById(cameraId)
                return successResponse(camera);
            }
            this.setStatus(404);
            return failedResponse('Không tìm thấy camera', 'CameraNotFound');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Delete food
     * @param foodId
     */
     @Delete('/{camId}')
     public async deleteCamera(camId: string): Promise<any>{
         try{
             if (await isExistCameras(camId)) {
                 let res = await Camera.findByIdAndDelete(camId);
                 return successResponse(res);
             }
             this.setStatus(404);
             return failedResponse('Camera is not found', 'CameraNotFound');
         } catch (err) {
             return failedResponse(`Error: ${err}`, 'ServiceException');
         }
     }
}

const isExistCameras = async (cameraId: string): Promise<boolean> => {
    const cameras = await Camera.find({})
            .where('_id').equals(cameraId)
            .exec();
    return cameras.length > 0;
}