export interface ICamera {
    penId: string
    camName?: string
    account?: string
    passwordCam: string
    manufacturer?: string
    linkStreaming: string
    metaData?: string
}

export interface ICameraGetDTO {
    penId: string
    camName?: string
    account?: string
    passwordCam: string
    manufacturer?: string
    linkStreaming: string
    metaData?: string
}

export interface ICameraUpdateDTO {
    penId?: string
    camName?: string
    account?: string
    passwordCam?: string
    manufacturer?: string
    linkStreaming?: string
    metaData?: string
}