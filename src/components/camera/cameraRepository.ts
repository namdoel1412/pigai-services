import Camera from "../camera/camera.model";
import { ICameraGetDTO } from "./camera.types";

class CameraRepository{
    static GetCameraByPenId = async (penId: string): Promise<ICameraGetDTO[]> => {
        const cameras = await Camera.find({})
            .where('penId').equals(penId)
            .exec();
            if (cameras == null){
                return null;
            }
            let cameraRes = cameras.map(item => {
                return {
                    id: item.id,
                    penId: item.penId,
                    camName: item.camName,
                    account: item.account,
                    passwordCam: item.passwordCam,
                    manufacturer: item.manufacturer,
                    linkStreaming: item.linkStreaming,
                    metaData: item.metaData
                }
            })
            console.log(cameraRes)
            return cameraRes;
    }
}

export default CameraRepository;