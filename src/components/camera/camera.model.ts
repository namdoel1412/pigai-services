import mongoose, { Model, Document } from 'mongoose';
import { ICamera } from './camera.types';

interface CameraDocument extends ICamera, Document { }
interface CameraModel extends Model<CameraDocument> { }

const cameraSchema = new mongoose.Schema<CameraDocument, CameraModel>({
    penId: { type: mongoose.Types.ObjectId, ref: 'Pen', require: true},
    camName: { type: String, default: 0, require: false},
    account: { type: String, default: 0, require: false},
    passwordCam: { type: String, default: 0, require: false},
    manufacturer: { type: String, default: 0, require: false},
    linkStreaming: { type: String, default: 0, require: true},
    metaData: { type: String, default: 0, require: false}
})

cameraSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {   delete ret._id  }
});

export default mongoose.model<CameraDocument, CameraModel>('Camera', cameraSchema);
