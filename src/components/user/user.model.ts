import mongoose, { Model, Document, Schema } from 'mongoose';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { IUser, IUserResponse } from '../../types/user';
import { throwErrorMessage } from '../../utils/collection';
import { failedResponse, FailedResponseType } from '../../utils/http';

export interface UserDocument extends IUser, Document { }

interface UserModel extends Model<UserDocument> {
    checkCredentials: (email: string, password: string) => UserDocument | undefined | FailedResponseType<string>
    generateAccessToken: (user: any, remember?: boolean) => string
}

const userSchema = new Schema<UserDocument, UserModel>({
    username: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
    },
    avatar: String,
    fullName: {
        type: String,
        required: true
    },
    phone: String,
    active: {
        type: Number,
        default: 0
    },
    activeCode: {
        type: String,
        required: true
    },
    createTime: {
        type: Number,
        default: Date.now()
    },
    updateTime: {
        type: Number,
        default: Date.now()
    },
    roleId: {
        type: mongoose.Types.ObjectId,
        ref: 'Role',
        required: true,
    },
    defaultFarm: {
        type: String,
        ref: 'defaultFarm'
    }
});

userSchema.pre<UserDocument>('validate', async function (next) {
    const user = this;
    user.activeCode = await bcrypt.hash(Date.now().toString(), 8);

    next();
});

userSchema.pre<UserDocument>('save', async function (next) {
    const user = this;

    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8);
    }

    next();
});

userSchema.methods.toJSON = function (): Omit<IUserResponse, 'accessToken' | 'avatar'> & { avatar?: string } {
    const user = this;

    return {
        id: user._id,
        username: user.username,
        email: user.email,
        fullName: user.fullName,
        avatar: user?.avatar,
        phone: user.phone,
        active: user.active,
        createTime: user.createTime,
        updateTime: user.updateTime,
        defaultFarm: user?.defaultFarm
    }
};

userSchema.statics.generateAccessToken = function (user: UserDocument, remember = false): string {
    const { id } = user;
    return jwt.sign({ id }, process.env.JWT_SECRET ?? '', { expiresIn: remember ? '3d' : '2h' });
};

userSchema.statics.checkCredentials = async function (email: string, password: string): Promise<UserDocument | undefined | FailedResponseType<string>> {
    const user = await User.findOne({ email });

    if (!user) {
        //throwErrorMessage(failedResponse('Sai tài khoản hoặc mật khẩu', 'WrongCredentials'));
        return failedResponse('Sai tài khoản hoặc mật khẩu', 'WrongCredentials');
    }

    const isValidPassword = await bcrypt.compare(password, user?.password);

    if (!isValidPassword) {
        //throwErrorMessage(failedResponse('Sai tài khoản hoặc mật khẩu', 'WrongCredentials'));
        return failedResponse('Sai tài khoản hoặc mật khẩu', 'WrongCredentials');
    }
    return user;
};

const User = mongoose.model<UserDocument, UserModel>('User', userSchema);

export default User;
