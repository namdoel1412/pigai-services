import { body } from 'express-validator';

export const validationRegisterRequest = [
    body('username')
        .exists().withMessage('Username không được bỏ trống'),
    body('email')
        .exists().withMessage('Email không được bỏ trống')
        .isEmail().withMessage('Không đúng định dạng email'),
    body('password')
        .exists().withMessage('Mật khẩu không được bỏ trống')
        .isLength({ min: 6 }).withMessage('Mật khẩu tối thiểu 6 kí tự'),
    body('passwordConfirmation')
        .exists().withMessage('Xác nhận mật khẩu không được bỏ trống')
        .custom((value, { req }) => {
            if (value !== req.body.password) throw new Error('Xác nhận mật khẩu không trùng khớp');
            return true;
        }),
    body('fullName')
        .exists().withMessage('Họ tên không được bỏ trống'),
    body('phone')
        .matches(/(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/).withMessage('Số điện thoại không đúng định dạng').optional()
];

export const validationLoginRequest = [
    // body('username')
    //     .exists().withMessage('Username không được bỏ trống'),
    body('email')
        .exists().withMessage('Email không được bỏ trống')
        .isEmail().withMessage('Không đúng định dạng email'),
    body('password')
        .exists().withMessage('Mật khẩu không được bỏ trống')
        .isLength({ min: 6 }).withMessage('Mật khẩu tối thiểu 6 kí tự'),
];
