export interface IUserRegisterType {
    username: string,
    email: string
    password: string
    passwordConfirmation: string
    fullName: string
    phone?: string
    roleId?: string
    active?: number,
    defaultFarm?: string
}

export interface IUserLogin {
    email: string
    password: string
    remember: boolean
}

export interface IUserUpdateInformation {
    fullName?: string,
    phone?: string,
    defaultFarm?: string
}

export interface IUserChangePassword {
    oldPassword: string
    password: string
    passwordConfirmation: string
}

export interface IActiveAccount {
    email: string
    activeCode: string
}

export interface IResetPassword {
    email: string
    password: string
    code: string
}

export interface IUserGetDTO{
    id: string,
    username: string,
    email: string,
    fullName: string,
    avatar: string,
    phone: string,
    createTime: number,
    updateTime: number
}
