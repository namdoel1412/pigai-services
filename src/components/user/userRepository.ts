import User from "./user.model";
import { IUserGetDTO } from "./user.types";

class UserRepository{
    static GetUserById = async (foodId: string): Promise<IUserGetDTO> => {
        const user = await User.findOne({})
            .where('_id').equals(foodId)
            .exec();
            if (user == null){
                return null;
            }
            let userRes: IUserGetDTO = {
                id: user.id,
                username: user.username,
                email: user.email,
                fullName: user.fullName,
                avatar: user.avatar,
                phone: user.phone,
                createTime: user.createTime,
                updateTime: user.createTime
            } 
            return userRes;
    }
}

export default UserRepository;