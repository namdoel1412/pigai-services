import { NextFunction } from 'express';
import User, { UserDocument } from './user.model';
import Role from '../role/role.model';
import Farm from '../farm/farm.model';
import { CustomRequest, CustomResponse, failedResponse, FailedResponseType, instanceOfFailedResponseType, successResponse } from '../../utils/http';
import { IUser, IUserResponse, UserActiveStatus } from '../../types/user';
import {
    IUserRegisterType,
    IActiveAccount,
    IUserChangePassword,
    IUserLogin,
    IUserUpdateInformation,
} from './user.types';
import bcrypt from 'bcrypt';
import { ERole } from '../role/role.types';
import { throwErrorMessage } from '../../utils/collection';
import path from 'path';
import { IConfigEmailData, IRegisterMail } from '../../types/email';
import { sendMail } from '../../services/mail';
import { Controller, Route, Get, Post, BodyProp, Put, Delete, Tags, Body, Query } from 'tsoa';
import mongoose from 'mongoose';
import { ActiveStatus } from '../../types/common';

@Route('/users')
@Tags('Users')
export class UserController extends Controller{
    @Post('register')
    public async register(@Body() input: IUserRegisterType): Promise<any>{
        const session = await User.startSession();
        session.startTransaction();
        input.active = ActiveStatus.InActive;
        try {
            if ((await User.find({ email: { $eq: input.email.trim() } })).length > 0) {
                this.setStatus(400);
                return failedResponse('Email này đã được đăng kí', 'UniqueEmail');
            }

            const role = await Role.findOne({ name: { $eq: ERole.OWNER } });

            if (!role) {
                throwErrorMessage({ message: 'Role not defined', statusCode: 'RoleNotDefined' });
                return;
            }

            input.roleId = role?._id;

            // TODO: Bỏ active email khi register
            //input.active = UserActiveStatus.ACTIVATED;

            const user = await (await new User(input).save({ session }));
            console.log(user)
            //createDefaultFarm(user._id);
            let urlUserActiveAccount = process.env.URL_USER_ACTIVE_ACCOUNT.replace(process.env.EmailConfig, user.email).replace(process.env.ActiveCodeConfig, user.activeCode);
            const dataSendMail: IConfigEmailData<IRegisterMail> = {
                user: user,
                subject: 'Đăng ký tài khoản',
                mailDir: path.join(__dirname, '../../../assets/mail/user/register.ejs'),
                mailContent: {
                    fullName: user.fullName,
                    messageRegister: 'Chào mừng bạn đến với Piggin! Xác nhận email để bắt đầu sử dụng dịch vụ của chúng tôi.',
                    // urlActiveAccount: `${ process.env.URL_USER_ACTIVE_ACCOUNT ?? '' }${ user.activeCode }`,
                    urlActiveAccount: `${ urlUserActiveAccount }`,
                },
            };

            // create defaultFarm
            // const defaultFarm = await FarmRepo.createDefaultFarm({name: 'Default', ownerId: user.id, address: 'None'});

            await sendMail(dataSendMail);

            await session.commitTransaction();
            session.endSession();
            // const preUser = await User.findById(user._id);
            // await preUser.updateOne({ defaultFarm: defaultFarm.id });
            // user.defaultFarm = defaultFarm.id;
            return successResponse(user);
        } catch (err) {
            await session.abortTransaction();
            session.endSession();
            this.setStatus(500);
            return failedResponse(`Caught error ${err}`, 'ServiceException');
        }
    }

    @Get('/active-account')
    public async activateAccount(@Query() email: string, @Query() activeCode: string): Promise<any>{
        try {
            const filter = { email, activeCode };
            const updateData = { active: UserActiveStatus.ACTIVATED };
    
            const user = await User.findOne(filter);
    
            if (!user) {
                this.setStatus(404);
                return failedResponse('Không tìm thấy tài khoản', 'UserNotFound');
            }
    
            if (user.active === UserActiveStatus.ACTIVATED) {
                this.setStatus(204)
                return failedResponse('Tài khoản đã được kích hoạt', 'AccountActivated');
            }
    
            await user.updateOne(updateData);
    
            const accessToken = await User.generateAccessToken(user);
    
            return successResponse({ ...user.toJSON(), accessToken } as IUserResponse);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Caught error ${err}`, 'ServiceException');
        }
    }

    @Post('/login')
    public async login(@Body() data: IUserLogin): Promise<any>{
        try {
            const { email, password, remember } = data;
    
            const user = await User.checkCredentials(email, password);

            if(instanceOfFailedResponseType<string>(user)){
                this.setStatus(400);
                return user;
            }
            user as UserDocument;
            if (!user) return;
    
            if (user.active !== UserActiveStatus.ACTIVATED) {
                this.setStatus(400);
                return failedResponse('Tài khoản chưa được kích hoạt', 'UserNotActivated');
            }
    
            // const farm = await Farm.find({ ownerId: user.id });
            // if (farm.length === 0) {
            //     this.setStatus(404);
            //     return failedResponse('Không tìm thấy trang trại', 'FarmNotFound');
            // }
            //const farmId = farm[0]._id;
            // console.log(user);
            const accessToken = await User.generateAccessToken(user, remember);
            console.log(user);
            let defaultFarm = null;
            let ObjectId = mongoose.Types.ObjectId;
            if(!ObjectId.isValid(user?.defaultFarm)){
                return successResponse({ ...user.toJSON(), accessToken, defaultFarm: defaultFarm } as IUserResponse);
            }
            defaultFarm = await Farm.findOne({})
            .where('_id').equals(new ObjectId(user?.defaultFarm))
            .exec();
            return successResponse({ ...user.toJSON(), accessToken, defaultFarm: defaultFarm } as IUserResponse);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Caught error ${err}`, 'ServiceException');
        }
    }

    @Get()
    public async getInfoUser(): Promise<any>{
        try {
            //const user = req.user;
            return successResponse("user");
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Caught error ${err}`, 'ServiceException');
        }
    }
    
    @Put('/{userId}')
    public async updateInfoUser(userId: string, @Body() data: IUserUpdateInformation): Promise<any>{
        try {
            const userDocument = await User.findOneAndUpdate({ _id: userId }, { ...data, updateTime: Date.now() }, { new: true });
            if (!userDocument) {
                this.setStatus(404);
                return failedResponse('not found', 'NotFound');
            }
            let ObjectId = mongoose.Types.ObjectId;
            const defaultFarm = await Farm.findOne({})
            .where('_id').equals(new ObjectId(userDocument?.defaultFarm))
            .exec();
            return successResponse(
                {
                    active: userDocument.active,
                    email: userDocument.email,
                    fullName: userDocument.fullName,
                    id: userDocument?.id,
                    username: userDocument.username,
                    avatar: userDocument?.avatar,
                    createTime: userDocument?.createTime,
                    phone: userDocument?.phone,
                    updateTime: userDocument?.updateTime,
                    defaultFarm: defaultFarm 
                } as IUserResponse);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Caught error ${err}`, 'ServiceException');
        }
    }

    @Put('changePassword/{userId}')
    public async changePassword(userId: string, @Body() data: IUserChangePassword): Promise<any>{
        try {
            const { oldPassword, password } = data;
    
            const user = await User.findById(userId);
    
            if (!user) {
                this.setStatus(404);
                return failedResponse('Không tìm thấy tài khoản', 'UserNotFound');
            }
    
            const isValidPassword = await bcrypt.compare(oldPassword, user.password);
    
            if (!isValidPassword) {
                this.setStatus(400);
                return failedResponse('Mật khẩu cũ không đúng', 'NotValidOldPassword');
            }
    
            const hasPassword = await bcrypt.hashSync(password, 8);
            await user.update({ password: hasPassword });
    
            return successResponse(user as IUserResponse);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Caught error ${err}`, 'ServiceException');
        }
    }
}
