export const getAllHealthStatusQuery = (name: string): any => [
    {
        $match: {
            name: new RegExp(name, 'g'),
        }
    },
    {
        $sort: { name: 1 },
    },
    {
        $project: {
            _id: 0,
            id: '$_id',
            name: 1,
        }
    }
];

export const lookupSingleObjectHealthStatus  = (): any => (
    [
        {
            $lookup: {
                from: 'healthstatuses',
                let: { healthStatus: '$healthStatus' },
                as: 'healthstatus',
                pipeline: [
                    {
                        $match: {
                            $expr: { $eq: [ '$_id', '$$healthStatus' ] }
                        }
                    },
                    dataResponseProject
                ]
            }
        },
        {
            $unwind: {
                path: '$healthstatus',
                preserveNullAndEmptyArrays: true
            } 
        }
    ]
);

const dataResponseProject = {
    $project: {
        _id: 0,
        id: '$_id',
        name: 1
    }
};