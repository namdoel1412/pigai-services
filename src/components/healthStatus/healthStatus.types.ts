export enum EHealthStatus {
    NORMAL = 'Bình thường',
    ABNORMAL = 'Bất thường'
}

export interface IHealthStatus {
    name: string
}
