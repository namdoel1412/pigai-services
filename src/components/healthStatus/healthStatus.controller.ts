import {Body, Controller, Get, Post, Put, Query, Route, Tags} from 'tsoa';
import { failedResponse, successResponse } from '../../utils/http';
import HealthStatus from './healthStatus.model';
import { getAllHealthStatusQuery } from "./healthStatus.queries";
import { IHealthStatus } from './healthStatus.types';

@Route('healthStatuses')
@Tags('HealthStatus')
export class HealthStatusController extends Controller{
    
    @Get('?')
    public async getListHealtStatus(@Query() name?: string): Promise<any>{
        try {
            const result = await HealthStatus.aggregate(getAllHealthStatusQuery(name));
            return successResponse(result);
        }
        catch (err) {
            this.setStatus(500);
            return failedResponse(`Error ${err}`, 'ServiceException');
        }
    }

    /**
     * Create health status
     * @param data
     */
    @Post('/')
    public async create(@Body() data: IHealthStatus): Promise<any>{
        try{
            if (await isExistHealthStatus(data)) {
                this.setStatus(400);
                return failedResponse('Tên tình trạng sức khỏe đã tồn tại', 'UniqueName');
            }
            await new HealthStatus(data).save();
            return successResponse('insertSuccess');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Update health status
     * @param healthStatusId
     * @param data
     */
    @Put('/{healthStatusId}')
    public async update(healthStatusId: string, @Body() data: IHealthStatus): Promise<any>{
        try{
            if (await isExistHealthStatus(data)) {
                this.setStatus(400);
                return failedResponse('Tên tình trạng sức khỏe đã tồn tại', 'UniqueName');
            }
            const healthStatus = await HealthStatus.findByIdAndUpdate(healthStatusId, data);
            if (!healthStatus) {
                this.setStatus(404);
                return failedResponse('Không tìm thấy tình trạng sức khỏe', 'HealthStatusNotFound');
            }
            return successResponse('updateSuccess');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }
}

const isExistHealthStatus = async (input: IHealthStatus): Promise<boolean> => {
    const { name } = input;
    return (await HealthStatus.find({ name })).length > 0;
}