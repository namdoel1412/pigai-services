import mongoose, { Model, Document } from 'mongoose';
import { IHealthStatus } from './healthStatus.types';

interface HealthStatusDocument extends IHealthStatus, Document { }
interface HealthStatusModel extends Model<HealthStatusDocument> { }

const healthStatusSchema = new mongoose.Schema<HealthStatusDocument, HealthStatusModel>({
    name: {
        type: String,
        required: true
    }
})

healthStatusSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) { delete ret._id }
});

export default mongoose.model<HealthStatusDocument, HealthStatusModel>('HealthStatus', healthStatusSchema);
