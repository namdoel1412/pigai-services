import Farm from './farm.model';
import { successResponse, failedResponse } from '../../utils/http';
import { NextFunction } from 'express';
import {IFarm, IFarmPagination, IGetTotalPigInOut, IReqGetFarmByOwnerId, IUpdateFarm} from './farm.types';
import {Controller, Route, Get, Post, BodyProp, Put, Delete, Tags, Body, Security, Header, Query} from 'tsoa';
import { authentication } from '../../middleware/authentication';
import { detailFarmQueryByOwnerId } from './farm.queries';
import * as farmQueries from './farm.queries';
import {EEventType} from "../eventType/eventType.types";

@Route('/farms')
@Tags('Farm')
export class FarmController extends Controller{
    /**
     * Create default farm with ownerId.
     */
    @Get("/{ownerId}")
    public async createDefaultFarm(ownerId: string): Promise<void>{
        try {
            new Farm({ ownerId, name: '', address: '' }).save().catch(err => console.log(err));
        } catch (err) {
            console.log(err);
        }
    }

    /**
     * Create farm
     */
    // @Security("jwt")
    @Post()
    public async createFarm(@Body() data: IFarm): Promise<any>{
        try {
            if (!data.name && !data.address && !data.ownerId) {
                return failedResponse('IdsRequired', 'IdsRequired');
            }
            const result = await new Farm(data).save();
            return successResponse(result);
        } catch (err) {
            this.setStatus(500);
            return failedResponse('Execute service went wrong', 'ServiceException');
        }
    }

    /**
     * Get farm's detail by farmId
     */
    // @Security("jwt")
    @Get('farmId/{farmId}')
    public async getFarmByFarmId(farmId: string): Promise<any>{
        try {
            const result = await Farm.find({})
            .where('_id').equals(farmId)
            .exec();
            return successResponse(result);
        }
        catch (err) {
            this.setStatus(500);
            return failedResponse(err, 'ServiceException');
        }
    }

    /**
     * Get farms by owner id.
     */
    // @Security("jwt")
    @Post('onwerId')
    public async getAllFarmsByOwnerId(@Body() data: IReqGetFarmByOwnerId): Promise<any>{
        try {
            // const tmp = authen02();
            // console.log(tmp);
            // if(tmp !== true) return tmp;
            if (data.offset < 0 || data.size <= 0) {
                this.setStatus(400);
                return failedResponse('Offset and size are required', 'OffsetRequired');
            }
            let pagination: IFarmPagination = {
                offset : data.offset || 0,
                size: data.size || 10
            }
            const result = await Farm.aggregate(detailFarmQueryByOwnerId(data.ownerId, pagination));
            return successResponse(result[0]);
        }
        catch (err) {
            this.setStatus(500);
            return failedResponse('Something went wrong', 'ServiceException');
        }
    }

    /**
     * Update farm
     */
    // @Security("jwt")
    @Put('/{farmId}')
    public async updateFarm(farmId: string, @Body() data: IFarm): Promise<any>{
        try {
            if (!data.name || !data.ownerId) {
                this.setStatus(400);
                return failedResponse('BodyRequired', 'BodyRequired');
            }
            const event = await Farm.updateOne({ _id: { $eq: farmId } }, data);
            return successResponse(event);
        }
        catch (err) {
            this.setStatus(500);
            return failedResponse('Something went wrong', 'ServiceException');
        }
    }

    /**
     * Delete farm
     */
    // @Security("jwt")
    /**
     * Delete food
     * @param foodId
     */
     @Delete('/{farmId}')
     public async deleteFarm(farmId: string): Promise<any>{
         try{
             if (await isExistFarms(farmId)) {
                 let res = await Farm.findByIdAndDelete(farmId);
                 return successResponse(res);
             }
             this.setStatus(404);
             return failedResponse('Food is not found', 'FoodNotFound');
         } catch (err) {
             return failedResponse(`Error: ${err}`, 'ServiceException');
         }
     }

    /**
     * Get all pigs of the farm
     * @param farmId
     */
    @Get('/getAllPigs/{farmId}')
    public async getAllPigs (farmId: string): Promise<any>{
        try {
            const result = await Farm.aggregate(farmQueries.queryGetAllPigs(farmId));
            return successResponse(result[0]);
        } catch (err) {
            this.setStatus(500);
            console.log(err)
            return failedResponse('Something went wrong', 'ServiceException');
        }
    }

    /**
     * Total number of pigs in and out the barn in the year
     * @param input
     */
    @Post('/getTotalPig')
    public async getTotalPigsInAndOut (@Body() input: IGetTotalPigInOut): Promise<any>{
        try {
            const result = [];

            for (let i = 0; i < 12; i++) {
                const resultData = {
                    month: i,
                    pigIn: 0,
                    pigOut: 0
                };
                const farm = await Farm.aggregate(farmQueries.queryGetTotalPigsInAndOut(input));
                if (farm[0]) {
                    const data = farm[0].pigs;

                    for (let j = 0; j < data.length; j++) {
                        const month = new Date(data[j].events.endDate).getMonth();

                        if (month !== i) continue;

                        if (data[j].events.eventType.name === EEventType.pigOut) {
                            resultData.pigOut += 1;
                        }

                        if (data[j].events.eventType.name === EEventType.pigIn) {
                            resultData.pigIn += 1;
                        }
                    }
                }
                result.push(resultData);
            }
            return successResponse(result);

        } catch (err) {
            console.log(err)
            this.setStatus(500);
            return failedResponse('Something went wrong', 'ServiceException');
        }
    }
}

const isExistFarms = async (farmId: string): Promise<boolean> => {
    const food = await Farm.find({})
            .where('_id').equals(farmId)
            .exec();
    return food.length > 0;
}
