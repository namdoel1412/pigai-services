import { IPaginationCommon } from "../../types/common";

export interface IFarm {
    name: string
    address?: string
    ownerId: string
}

export interface IUpdateFarm{
    name: string
    address: string
    ownerId: string
}

export interface IFarmPagination extends IPaginationCommon{}

export interface IReqGetFarmByOwnerId extends IFarmPagination{
    ownerId: string
}
export interface IGetTotalPigInOut {
    farmId: string
    year: number
}

export interface IFarmGetDTO {
    id: string,
    name: string
    address?: string
    ownerId: string
}