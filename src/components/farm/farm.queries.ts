import mongoose from 'mongoose';
import {IFarmPagination, IGetTotalPigInOut} from './farm.types';
import {DEFAULT_OFFSET_PAGINATION, DEFAULT_SIZE_PAGINATION} from '../../utils/constants';
import {EHealthStatus} from '../healthStatus/healthStatus.types';
import {EEventStatus} from "../event/event.types";
import {EEventType} from "../eventType/eventType.types";
import format from 'date-fns/format';

const lookupObject = (collectionName: string, foreignName: string, customPipline: any): any => ({
    $lookup: {
        from: collectionName,
        let: {[foreignName]: `$${foreignName}`},
        as: collectionName,
        ...customPipline
    }
});

export const lookupObjectFarms = (collectionName: string, foreignName: string): any => {
    let pipeline = {
        pipeline: [
            {
                $match: {$expr: {$in: ['$_id', `$$${foreignName}`]}}
            },
            dataResponseProjectFarm
        ]
    }
    return lookupObject(collectionName, foreignName, pipeline);
};

const dataDetailResponseProjectFarm = {
    $project: {
        id: '$_id',
        _id: 0,
        name: 1,
        address: 1,
        owner: '$user'
    }
};

export const lookupSingleObjectUser = (): any => (
    [
        {
            $lookup: {
                from: 'users',
                let: {ownerId: '$ownerId'},
                as: 'user',
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$_id', '$$ownerId']}
                        }
                    },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            name: 1,
                            address: 1,
                            username: 1,
                            email: 1,
                            fullName: 1,
                        }
                    }
                ]
            }
        },
        {
            $unwind: {
                path: '$user',
                preserveNullAndEmptyArrays: true
            }
        }
    ]
);

export const detailFarmQueryByOwnerId = (ownerId: string, data: IFarmPagination): Array<Record<string, any>> => [
    {
        $match: {
            ownerId: {$eq: mongoose.Types.ObjectId(ownerId)}
        },
    },
    ...lookupSingleObjectUser(),
    dataDetailResponseProjectFarm,
    {
        $facet: {
            count: [{$count: 'total'}],
            items: [
                {$skip: +data.offset},
                {$limit: +data.size},
            ],
        },
    },
    {
        $project: {
            items: 1,
            total: {
                $cond: {
                    if: {$eq: [{$size: '$count'}, 0]},
                    then: 0,
                    else: {$arrayElemAt: ['$count.total', 0]}
                },
            },
        },
    },
]

export const lookupSingleObjectFarm = (): any => (
    [
        {
            $lookup: {
                from: 'farms',
                let: {farmId: '$farmId'},
                as: 'farm',
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$_id', '$$farmId']}
                        }
                    },
                    dataResponseProjectFarm
                ]
            }
        },
        {
            $unwind: {
                path: '$farm',
                preserveNullAndEmptyArrays: true
            }
        }
    ]
);

const dataResponseProjectFarm = {
    $project: {
        id: '$_id',
        _id: 0,
        name: 1,
        address: 1
    }
};

export const queryGetAllPigs = (farmId: string): Array<Record<string, any>> => {
    return [
        {
            $match: {
                _id: {$eq: mongoose.Types.ObjectId(farmId)},
            },
        },
        {
            $lookup: {
                from: 'pigs',
                let: {farmId: '$_id'},
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$farmId', '$$farmId']},
                        },
                    },
                    {
                        $lookup: {
                            from: 'healthstatuses',
                            let: {healthStatus: '$healthStatus'},
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {$eq: ['$_id', '$$healthStatus']},
                                    },
                                },
                                {
                                    $project: {
                                        _id: 0,
                                        id: '$_id',
                                        name: 1,
                                    }
                                },
                            ],
                            as: 'healthStatus',
                        },
                    },
                    {
                        $unwind: '$healthStatus',
                    },
                    {
                        $project: {
                            _id: 0,
                            id: '$_id',
                            pigAIId: 1,
                            healthStatus: 1,
                        }
                    }
                ],
                as: 'pigs',
            },
        },
        {
            $addFields: {
                normalPigs: {
                    $map: {
                        input: '$pigs',
                        as: 'pig',
                        in: {
                            $cond: [{$eq: ['$$pig.healthStatus.name', EHealthStatus.NORMAL]}, 1, 0]
                        }
                    }
                },
                abnormalPigs: {
                    $map: {
                        input: '$pigs',
                        as: 'pig',
                        in: {
                            $cond: [{$eq: ['$$pig.healthStatus.name', EHealthStatus.ABNORMAL]}, 1, 0]
                        }
                    }
                },
            }
        },
        {
            $project: {
                _id: 0,
                id: '$_id',
                name: 1,
                address: 1,
                pigs: 1,
                totalPigs: {$size: '$pigs'},
                totalNormalPigs: {$sum: '$normalPigs'},
                totalAbnormalPigs: {$sum: '$abnormalPigs'},
            }
        },
    ]
}

export const queryGetTotalPigsInAndOut = (data: IGetTotalPigInOut): Array<Record<string, any>> => {
    const {farmId, year} = data;
    return [
        {
            $match: {
                _id: {$eq: mongoose.Types.ObjectId(farmId)},
            },
        },
        {
            $lookup: {
                from: 'pigs',
                let: {farmId: '$_id'},
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$farmId', '$$farmId']},
                        },
                    },
                    {
                        $lookup: {
                            from: 'events',
                            let: {pigId: '$_id'},
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {$in: ['$$pigId', '$pigIds']},
                                        status: {$eq: EEventStatus.Done},
                                    }
                                },
                                {
                                    $lookup: {
                                        from: 'eventtypes',
                                        let: {eventTypeId: '$eventTypeId'},
                                        pipeline: [
                                            {
                                                $match: {
                                                    $expr: {$eq: ['$_id', '$$eventTypeId']},
                                                    $or: [{name: new RegExp(EEventType.pigIn, 'g')}, {name: new RegExp(EEventType.pigOut, 'g')}]
                                                },
                                            },
                                            {
                                                $project: {
                                                    _id: 0,
                                                    id: '$_id',
                                                    name: 1,
                                                }
                                            }
                                        ],
                                        as: 'eventType',
                                    },
                                },
                                {
                                    $unwind: '$eventType',
                                },
                                {
                                    $project: {
                                        _id: 0,
                                        id: '$_id',
                                        startDate: {
                                            $toDate: '$startDate',
                                        },
                                        endDate: {
                                            $toDate: '$endDate',
                                        },
                                        eventType: '$eventType',
                                    }
                                }
                            ],
                            as: 'events',
                        }
                    },
                    {$unwind: '$events'},
                    {
                        $match: {
                            $and: [{
                                $or: [
                                    {
                                        $and: [{$expr: {$eq: ['$events.eventType.name', EEventType.pigIn]}}, {$expr: {$eq: [{$year: '$events.startDate'}, year]}}],
                                    },
                                    {
                                        $and: [{$expr: {$eq: ['$events.eventType.name', EEventType.pigOut]}}, {$expr: {$eq: [{$year: '$events.endDate'}, year]}}]
                                    }
                                ]
                            }]
                        }
                    },
                    {
                        $project: {
                            _id: 0,
                            id: 1,
                            pigAIId: 1,
                            events: '$events',
                        }
                    },
                ],
                as: 'pigs',
            },
        },
        {$unwind: '$pigs'},
        {
            $group: {
                _id: '$_id',
                id: {$first: '$_id'},
                name: {$first: '$name'},
                pigs: {$push: '$pigs'},
            }
        },
        {
            $project: {
                _id: 0,
                id: '$_id',
                name: 1,
                pigs: 1,
            }
        },
    ]
}
