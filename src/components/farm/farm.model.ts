import mongoose, { Model, Document } from 'mongoose';
import { IFarm } from './farm.types';

interface FarmDocument extends IFarm, Document { }
interface FarmModel extends Model<FarmDocument> { }

const farmSchema = new mongoose.Schema<FarmDocument, FarmModel>({
    name: String,
    address: String,
    ownerId: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
        required: true
    }
})

farmSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {   delete ret._id  }
});

export default mongoose.model<FarmDocument, FarmModel>('Farm', farmSchema);
