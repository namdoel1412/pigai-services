import Farm from "./farm.model";
import { IFarmGetDTO } from "./farm.types";

class FarmRepository{
    static GetFarmById = async (foodId: string): Promise<IFarmGetDTO> => {
        const farms = await Farm.findOne({})
            .where('_id').equals(foodId)
            .exec();
            if (farms == null){
                return null;
            }
            let foodRes: IFarmGetDTO = {
                id: farms?.id,
                name: farms?.name,
                ownerId: farms.ownerId,
                address: farms.address
            } 
            return foodRes;
    }
}

export default FarmRepository;