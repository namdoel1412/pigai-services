import mongoose, { Model, Document } from 'mongoose';
import { IRole, ERole } from '@components/role/role.types';

interface RoleDocument extends IRole, Document { }
interface RoleModel extends Model<RoleDocument> { }

const roleSchema = new mongoose.Schema<RoleDocument, RoleModel>({
    name: {
        type: String,
        required: true
    }
})

roleSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {   delete ret._id  }
});

const Role = mongoose.model<RoleDocument, RoleModel>('Role', roleSchema);

export default Role;
