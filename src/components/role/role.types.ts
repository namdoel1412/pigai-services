export enum ERole {
    OWNER = 'OWNER',
    EMPLOYEE = 'EMPLOYEE'
}

export interface IRole {
    name: ERole
}
