import mongoose, { Model, Document } from 'mongoose';
import { IPen } from '@components/pen/pen.types';

interface PenDocument extends IPen, Document { }
interface PenModel extends Model<PenDocument> { }

const penSchema = new mongoose.Schema<PenDocument, PenModel>({
    name: {
        type: String,
        required: true,
    },
    capacity: Number,
    area: Number,
    changePenPrediction: String,
    pigOutPrediction: String,
    farmId: {
        type: mongoose.Types.ObjectId,
        ref: 'Farm',
        required: true
    },
    weightTypeId: {
        type: mongoose.Types.ObjectId,
        ref: 'WeightType',
        required: true
    },
    note: String,
})

penSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) { delete ret._id }
});

export default mongoose.model<PenDocument, PenModel>('Pen', penSchema);
