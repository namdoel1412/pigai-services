import { body } from 'express-validator';

export const createPen = [
    body('name')
        .exists().withMessage('Mã chuồng không được để trống'),
    body('area')
        .exists().withMessage('Diện tích chuồng không được để trống')
        .isNumeric().withMessage('Bắt buộc là số'),
    body('capacity')
        .exists().withMessage('Định mức - số cá thể không được để trống')
        .isNumeric().withMessage('Bắt buộc là số'),
    body('weightTypeId')
        .exists().withMessage('Loại heo không được để trống')
];

export const updatePen = [
    body('area')
        .exists().withMessage('Diện tích chuồng không được để trống')
        .isNumeric().withMessage('Bắt buộc là số'),
    body('capacity')
        .exists().withMessage('Định mức - số cá thể không được để trống')
        .isNumeric().withMessage('Bắt buộc là số'),
    body('weightTypeId')
        .exists().withMessage('Loại heo không được để trống')
];
