import { DEFAULT_OFFSET_PAGINATION, DEFAULT_SIZE_PAGINATION } from '../../utils/constants';
import { IListEmptyPen, IListPen } from './pen.types';
import HealthStatus from '@components/healthStatus/healthStatus.model';
import mongoose from 'mongoose';
import { EHealthStatus } from '../healthStatus/healthStatus.types';
import { EEventStatus } from '../event/event.types';

const lookupObjects = (collectionName: string, foreignName: string, customPipline: any): any => ({
    $lookup: {
        from: collectionName,
        let: { [foreignName]: `$${ foreignName }` },
        as: collectionName,
        ...customPipline
    }
});

export const lookupObjectPens = (collectionName: string, foreignName: string): any => {
    let pipeline = {
        pipeline: [
            {
                $match: { $expr: { $in: [ '$_id', `$$${ foreignName }` ] } }
            },
            dataResponseProject
        ]
    }
    return lookupObjects(collectionName, foreignName, pipeline);
};

export const lookupSingleObjectPen = (): any => (
    [
        {
            $lookup: {
                from: 'pens',
                let: { penId: '$penId' },
                as: 'pen',
                pipeline: [
                    {
                        $match: {
                            $expr: { $eq: [ '$_id', '$$penId' ] }
                        }
                    },
                    dataResponseProject
                ]
            }
        },
        { 
            $unwind: {
                path: '$pen',
                preserveNullAndEmptyArrays: true
            } 
        }
    ]
);


const dataResponseProject = {
    $project: {
        _id: 0,
        id: '$_id',
        name: 1,
        capacity: 1,
        area: 1,
        farmId: 1,
        weightTypeId: 1,
        note: 1
    }
};

export const listPenQuery = (input: IListPen): Array<Record<string, any>> => {
    const { offset = DEFAULT_OFFSET_PAGINATION, size = DEFAULT_SIZE_PAGINATION, farmId, name, eventDate,
        eventTypeId, min, max, healthStatus, weightTypeId } = input;

    return [
        {
            $match: {
                farmId: { $eq: mongoose.Types.ObjectId(farmId) },
                name: new RegExp(name, 'g'),
                weightTypeId: weightTypeId ? { $eq: mongoose.Types.ObjectId(weightTypeId) } : { $ne: null },
            },
        },
        // weightType
        {
            $lookup: {
                from: 'weighttypes',
                let: { weightTypeId: '$weightTypeId' },
                pipeline: [
                    {
                        $match: { $expr: { $eq: [ '$_id', '$$weightTypeId' ] } }
                    },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            name: 1,
                        }
                    }
                ],
                as: 'weightType'
            }
        },
        { 
            $unwind: {
                path: '$weightType',
                preserveNullAndEmptyArrays: true 
            }
        },
        // events
        {
            $lookup: {
                from: 'events',
                let: { penId: '$_id' },
                pipeline: [
                    {
                        $match: {
                            $expr: { $in: [ '$$penId', '$penIds' ] },
                            status: { $eq: EEventStatus.Pending },
                            startDate: eventDate ? { $eq: eventDate } : { $ne: null },
                            eventTypeId: eventTypeId ? { $eq: mongoose.Types.ObjectId(eventTypeId) } : { $ne: null },
                        },
                    },
                    {
                        $sort: { startDate: -1 },
                    },
                    {
                        $lookup: {
                            from: 'eventtypes',
                            let: { eventTypeId: '$eventTypeId' },
                            pipeline: [
                                {
                                    $match: { $expr: { $eq: [ '$_id', '$$eventTypeId' ] } },
                                },
                                {
                                    $project: {
                                        _id: 0,
                                        id: '$_id',
                                        name: 1,
                                    }
                                }
                            ],
                            as: 'eventType',
                        },
                    },
                    {
                        //$unwind: '$eventType',
                        $unwind: {
                            path: '$eventType',
                            preserveNullAndEmptyArrays: true 
                        }
                    },
                    {
                        $project: {
                            _id: 0,
                            id: '$_id',
                            name: 1,
                            desc: 1,
                            startDate: 1,
                            endDate: 1,
                            from: 1,
                            to: 1,
                            status: 1,
                            eventType: '$eventType',
                        }
                    }
                ],
                as: 'events',
            }
        },
        // pigs, healthStatus
        {
            $lookup: {
                from: 'pigs',
                let: { penId: '$_id' },
                pipeline: [
                    {
                        $match: {
                            $expr: { $eq: [ '$penId', '$$penId' ] },
                        },
                    },
                    {
                        $lookup: {
                            from: 'healthstatuses',
                            let: { healthStatus: '$healthStatus' },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: { $eq: [ '$_id', '$$healthStatus' ] },
                                    },
                                },
                                {
                                    $project: {
                                        _id: 0,
                                        id: '$_id',
                                        name: 1,
                                    }
                                },
                            ],
                            as: 'healthStatus',
                        },
                    },
                    {
                        //$unwind: '$healthStatus',
                        $unwind: {
                            path: '$healthStatus',
                            preserveNullAndEmptyArrays: true 
                        }
                    },
                    {
                        $lookup: {
                            from: 'behaviours',
                            let: { pigAIId: '$pigAIId' },
                            pipeline: [
                                {
                                    $sort: { startTime: -1 },
                                },
                                {
                                    $match: {
                                        $expr: { 
                                            $eq: [ '$pigAIId', '$$pigAIId' ],
                                        },
                                    },
                                },
                                {
                                    $project: {
                                        _id: 0,
                                        id: '$_id',
                                        farmId: 1,
                                        penId: 1,
                                        startTime: 1,
                                        endTime: 1,
                                        sleepingTimes: 1,
                                        eatingTimes: 1,
                                        lieDownTimes: 1,
                                        standingTimes: 1,
                                        unspecified: 1,
                                        pigAIId: 1
                                    }
                                },
                                {
                                    //$unwind: '$healthStatus',
                                    $unwind: {
                                        path: '$behaviour',
                                        preserveNullAndEmptyArrays: true 
                                    }
                                },
                            ],
                            as: 'behaviour',
                        },
                    },
                    {
                        $project: {
                            _id: 0,
                            id: '$_id',
                            pigAIId: 1,
                            penId: 1,
                            healthStatus: 1,
                            behaviour: {
                                $first: '$behaviour'
                            },
                            size: 10,
                            weight: 1,
                            dateOfBirth: 1,
                            temperature: 1,
                        }
                    }
                ],
                as: 'pigs',
            },
        },
        { 
            $unwind: '$pigs'
            // $unwind: {
            //     path: '$pigs',
            //     preserveNullAndEmptyArrays: true 
            // }
        },
        {
            $group: {
                _id: '$_id',
                id: { $first: '$_id' },
                name: { $first: '$name' },
                area: { $first: '$area' },
                capacity: { $first: '$capacity' },
                weightType: { $first: '$weightType' },
                events: { $first: '$events' },
                pigs: { $push: '$pigs' },
                note: { $first: '$note' },
            }
        },
        { 
            $unwind: {
                path: '$events',
                preserveNullAndEmptyArrays: eventDate || eventTypeId ? false : true
            }
        },
        {
            $group: {
                _id: '$_id',
                id: { $first: '$_id' },
                name: { $first: '$name' },
                area: { $first: '$area' },
                capacity: { $first: '$capacity' },
                weightType: { $first: '$weightType' },
                events: { $push: '$events' },
                pigs: { $first: '$pigs' },
                note: { $first: '$note' },
            }
        },
        {
            $addFields: {
                status: {
                    $map: {
                        input: '$pigs',
                        as: 'pig',
                        in: {
                            $cond: [ { $eq: [ '$$pig.healthStatus.name', EHealthStatus.NORMAL ] }, 0, 1 ]
                        }
                    }
                }
            }
        },
        {
            $project: {
                _id: 0,
                id: 1,
                name: 1,
                area: 1,
                capacity: 1,
                weightType: 1,
                pigs: 1,
                totalPigs: { $size: '$pigs' },
                penStatus: {
                    $cond: [ { $gt: [ { $sum: '$status' }, 0 ] }, EHealthStatus.ABNORMAL, EHealthStatus.NORMAL ],
                },
                events: '$events',
                note: 1,
            }
        },
        {
            $match: {
                $and: [
                    {
                        $or: [
                            { totalPigs: { $gte: min ? +min : 1 } },
                            { totalPigs: max ? { $lte: max } : { $ne: null } }
                        ]
                    }
                ],
                penStatus: healthStatus ? { $eq: healthStatus.name } : { $ne: null },
            }
        },
        {
            $facet: {
                count: [ { $count: 'total' } ],
                items: [
                    { $skip: +offset },
                    { $limit: +size },
                ],
            },
        },
        {
            $project: {
                items: 1,
                total: {
                    $cond: {
                        if: { $eq: [ { $size: '$count' }, 0 ] },
                        then: 0,
                        else: { $arrayElemAt: [ '$count.total', 0 ] }
                    },
                },
            },
        },
    ]
};

export const queryListEmptyPen = (input: IListEmptyPen): Array<Record<string, any>> => {
    const { offset = DEFAULT_OFFSET_PAGINATION, size = DEFAULT_SIZE_PAGINATION, name, area, weightTypeId, farmId } = input;
    return [
        {
            $match: {
                farmId: { $eq: mongoose.Types.ObjectId(farmId) },
                name: new RegExp(name, 'g'),
                area: area ? { $eq: +area } : { $ne: null },
                weightTypeId: weightTypeId ? { $eq: mongoose.Types.ObjectId(weightTypeId) } : { $ne: null },
            }
        },
        {
            $lookup: {
                from: 'pigs',
                let: { penId: '$_id' },
                pipeline: [
                    { $match: { $expr: { $eq: [ '$penId', '$$penId' ] } } }
                ],
                as: 'pigs',
            }
        },
        {
            $lookup: {
                from: 'weighttypes',
                let: { weightTypeId: '$weightTypeId' },
                pipeline: [
                    {
                        $match: { $expr: { $eq: [ '$_id', '$$weightTypeId' ] } }
                    },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            name: 1,
                        }
                    }
                ],
                as: 'weightType',
            }
        },
        {
            $lookup: {
                from: 'cameras',
                let: { penId: '$_id' },
                pipeline: [
                    { $match: { $expr: { $eq: [ '$penId', '$$penId' ] } } },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            camName: 1,
                            account: 1,
                            passwordCam: 1,
                            manufacturer: 1,
                            linkStreaming: 1,
                            metaData: 1,
                            penId: 1,
                        }
                    }
                ],
                as: 'cameras',
            }
        },
        {
            $match: {
                pigs: { $size: 0 },
            }
        },
        { 
            //$unwind: '$weightType' 
            $unwind: {
                path: '$weightType',
                preserveNullAndEmptyArrays: true 
            }
        },
        {
            $group: {
                _id: '$_id',
                id: { $first: '$_id' },
                name: { $first: '$name' },
                area: { $first: '$area' },
                capacity: { $first: '$capacity' },
                note: { $first: '$note'},
                weightType: { $first: '$weightType' },
                camera: { $first: '$cameras' },
            }
        },
        {
            $project: {
                _id: 0,
                id: '$_id',
                name: 1,
                area: 1,
                capacity: 1,
                note: 1,
                weightType: 1,
                camera: 1,
            }
        },
        {
            $facet: {
                count: [ { $count: 'total' } ],
                items: [
                    { $skip: +offset },
                    { $limit: +size },
                ],
            },
        },
        {
            $project: {
                items: 1,
                total: {
                    $cond: {
                        if: { $eq: [ { $size: '$count' }, 0 ] },
                        then: 0,
                        else: { $arrayElemAt: [ '$count.total', 0 ] }
                    },
                },
            },
        },
    ]
}

export const queryPenDetail = (id: string): Array<Record<string, any>> => {
    return [
        {
            $match: { _id: mongoose.Types.ObjectId(id) },
        },
        {
            $lookup: {
                from: 'weighttypes',
                let: { weightTypeId: '$weightTypeId' },
                pipeline: [
                    {
                        $match: { $expr: { $eq: [ '$_id', '$$weightTypeId' ] } }
                    },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            name: 1,
                        }
                    }
                ],
                as: 'weightType'
            }
        },
        { 
            //$unwind: '$weightType' 
            $unwind: {
                path: '$weightType',
                preserveNullAndEmptyArrays: true 
            }
        },
        {
            $lookup: {
                from: 'events',
                let: { penId: '$_id' },
                pipeline: [
                    {
                        $match: {
                            $expr: { $in: [ '$$penId', '$penIds' ] },
                            status: { $ne: EEventStatus.Rejected },
                        },
                    },
                    {
                        $sort: { startDate: -1 },
                    },
                    {
                        $lookup: {
                            from: 'eventtypes',
                            let: { eventTypeId: '$eventTypeId' },
                            pipeline: [
                                {
                                    $match: { $expr: { $eq: [ '$_id', '$$eventTypeId' ] } },
                                },
                                {
                                    $project: {
                                        _id: 0,
                                        id: '$_id',
                                        name: 1,
                                    }
                                }
                            ],
                            as: 'eventType',
                        },
                    },
                    {
                        //$unwind: '$eventType',
                        $unwind: {
                            path: '$eventType',
                            preserveNullAndEmptyArrays: true 
                        }
                    },
                    {
                        $project: {
                            _id: 0,
                            id: '$_id',
                            name: 1,
                            desc: 1,
                            startDate: 1,
                            endDate: 1,
                            from: 1,
                            to: 1,
                            eventType: '$eventType',
                        }
                    }
                ],
                as: 'events',
            }
        },
        {
            $lookup: {
                from: 'pigs',
                let: { penId: '$_id' },
                pipeline: [
                    {
                        $match: { $expr: { $eq: [ '$penId', '$$penId' ] } },
                    },
                    {
                        $lookup: {
                            from: 'healthstatuses',
                            let: { healthStatus: '$healthStatus' },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: { $eq: [ '$_id', '$$healthStatus' ] },
                                    },
                                },
                                {
                                    $project: {
                                        _id: 0,
                                        id: '$_id',
                                        name: 1,
                                    },
                                },
                            ],
                            as: 'healthStatus',
                        }
                    },
                    {
                        //$unwind: '$healthStatus',
                        $unwind: {
                            path: '$healthStatus',
                            preserveNullAndEmptyArrays: true 
                        }
                    },
                    {
                        $project: {
                            _id: 0,
                            id: '$_id',
                            pigAIId: 1,
                            healthStatus: 1,
                        }
                    }
                ],
                as: 'pigs',
            },
        },
        {
            $group: {
                _id: '$_id',
                id: { $first: '$_id' },
                name: { $first: '$name' },
                capacity: { $first: '$capacity' },
                area: { $first: '$area' },
                weightType: { $first: '$weightType' },
                events: { $first: '$events' },
                pigs: { $first: '$pigs' },
                note: { $first: '$note' },
            }
        },
        {
            $addFields: {
                status: {
                    $map: {
                        input: '$pigs',
                        as: 'pig',
                        in: {
                            $cond: [ { $eq: [ '$$pig.healthStatus.name', EHealthStatus.NORMAL ] }, 0, 1 ]
                        }
                    }
                }
            }
        },
        {
            $project: {
                _id: 0,
                id: 1,
                name: 1,
                capacity: 1,
                area: 1,
                weightType: '$weightType',
                events: '$events' ,
                pigs: '$pigs',
                note: '$note',
                totalPigs: { $size: '$pigs' },
                penStatus: {
                    $cond: [ { $gt: [ { $sum: '$status' }, 0 ] }, EHealthStatus.ABNORMAL, EHealthStatus.NORMAL ]
                },
            }
        },
    ]
};

export const listAllPensOfFarm = (farmId: string, offset = DEFAULT_OFFSET_PAGINATION, size = DEFAULT_SIZE_PAGINATION): Array<Record<string, any>> => {
    return [
        {
            $match: {
                farmId: { $eq: mongoose.Types.ObjectId(farmId) },
            },
        },
        {
            $lookup: {
                from: 'weighttypes',
                let: { weightTypeId: '$weightTypeId' },
                pipeline: [
                    {
                        $match: { $expr: { $eq: [ '$_id', '$$weightTypeId' ] } }
                    },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            name: 1,
                        }
                    }
                ],
                as: 'weightType'
            }
        },
        { 
            //$unwind: '$weightType' 
            $unwind: {
                path: '$weightType',
                preserveNullAndEmptyArrays: true 
            }
        },
        {
            $group: {
                _id: '$_id',
                id: { $first: '$_id' },
                name: { $first: '$name' },
                area: { $first: '$area' },
                capacity: { $first: '$capacity' },
                weightType: { $first: '$weightType' },
                note: { $first: '$note' },
            }
        },
        {
            $project: {
                _id: 0,
                id: 1,
                name: 1,
                area: 1,
                capacity: 1,
                weightType: 1,
                note: 1,
            }
        },
        {
            $facet: {
                count: [ { $count: 'total' } ],
                items: [
                    { $skip: +offset },
                    { $limit: +size },
                ],
            },
        },
        {
            $project: {
                items: 1,
                total: {
                    $cond: {
                        if: { $eq: [ { $size: '$count' }, 0 ] },
                        then: 0,
                        else: { $arrayElemAt: [ '$count.total', 0 ] }
                    },
                },
            },
        },
    ]
}