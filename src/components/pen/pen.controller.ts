import { NextFunction } from 'express';
import Pen from './pen.model';
import Farm from '../farm/farm.model';
import Pig from '../pig/pig.model';
import WeightType from '../weightType/weightType.model';
import * as penQueries from './pen.queries';
import { CustomRequest, CustomResponse, failedResponse, IListResponse, successResponse } from '../../utils/http';
import { IListEmptyPen, IListPen, IPen, IUpdatePen } from './pen.types';
import {listAllPensOfFarm, queryPenDetail} from "./pen.queries";
import {Body, Controller, Delete, Get, Post, Put, Query, Route, Tags} from 'tsoa';
import CameraRepository from '../camera/cameraRepository';
import PenRepository from './penRepository';
import BehaviourRepository from './../behaviour/behaviourRepository';

@Route('pens')
@Tags('Pen')
export class PenController extends Controller{

    /**
     * Get pen's detail
     * @param penId
     */
    @Get('{penId}')
    public async getPenDetail(penId: string): Promise<any>{
        try {
            const pen = await Pen.aggregate(queryPenDetail(penId));
            return successResponse(pen[0]);
        }
        catch (err) {
            this.setStatus(500);
            return failedResponse(`Error ${err}`, 'ServiceException');
        }
    }

    @Get('min/{id}')
    public async getPenById(id: string): Promise<any>{
        try {
            const pen = await PenRepository.GetPenById(id);
            if(pen == null){
                return failedResponse('Pen is not found', 'PenNotFound')
            }
            return successResponse(pen);
        }
        catch (err) {
            this.setStatus(500);
            return failedResponse(`Error ${err}`, 'ServiceException');
        }
    }

    /**
     * Create pen
     * @param dataInsert
     */
    @Post()
    public async createPen(@Body() dataInsert: IPen): Promise<any>{
        try {
            if (dataInsert.area && dataInsert.area < 10) {

                    return failedResponse('Diện tích chuồng phải lớn hơn hoặc bằng 10', 'GreaterThan');
            }
            const farm = await Farm.findById(dataInsert.farmId);
            if (!farm) {
                this.setStatus(404);
                return failedResponse('Không tìm thấy trang trại', 'FarmNotFound');
            }
    
            const weightType = await WeightType.findById(dataInsert.weightTypeId);
            if (!weightType) {
                this.setStatus(404);
                return failedResponse('Không tìm thấy loại heo phù hợp', 'WeightTyeNotFound');
            }
    
            if (await isExistPenName(dataInsert, dataInsert.farmId)) {
                this.setStatus(400);
                return failedResponse('Tên chuồng đã tồn tại', 'UniqueName');
            }
    
            const pen = await new Pen(dataInsert).save();
            return successResponse(pen);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error ${err}`, 'ServiceException');
        }
    }

    /**
     * Update pen
     * @param penId
     * @param dataUpdate
     */
    @Put('/{penId}')
    public async updatePen(penId: string, @Body() dataUpdate: IUpdatePen): Promise<any>{
        try {
            if (dataUpdate.weightTypeId) {
                const weightType = await WeightType.findById(dataUpdate.weightTypeId);
                if (!weightType) {
                    this.setStatus(404);
                    return failedResponse('Không tìm thấy loại heo phù hợp', 'WeightTyeNotFound');
                }
            }
    
            const pen = await Pen.findByIdAndUpdate(penId, dataUpdate);
            if (!pen) {
                this.setStatus(404);
                return failedResponse('Không tìm thấy chuồng', 'PenNotFound');
            }
    
            return successResponse('updateSuccess');
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error ${err}`, 'ServiceException');
        }
    }

    /**
     * Get all pen
     * @param data
     */
    @Post('/all')
    public async getAllPens(@Body() data: IListPen): Promise<any>{
        try {
            const result = await Pen.aggregate(penQueries.listPenQuery(data));
            let lstRes = result[0].items;
            let len = lstRes.length;
            for(let i = 0; i < len; i++){
                lstRes[i]['cameras'] = await CameraRepository.GetCameraByPenId(lstRes[i].id)
                if (lstRes[i]['pigs'] != null){
                    let len2 = lstRes[i]['pigs'].length;
                    for(let j = 0; j < len2; j++){
                        const pigAIId = lstRes[i]['pigs'][j]['pigAIId']
                        const penId = lstRes[i]['id']
                        let startDate = new Date();
                        startDate.setUTCHours(0,0,0,0);
                        let endDate = new Date();
                        endDate.setUTCHours(23,59,59,999);
                        lstRes[i]['pigs'][j]['behaviour'] = await BehaviourRepository.GetBehaviourByPenId(pigAIId, penId, startDate, endDate);
                    }
                }
            }
            result[0].items = lstRes;
            return successResponse(result[0]);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error ${err}`, 'ServiceException');
        }
    }

    /**
     * Get all empty pen
     * @param data
     */
    @Post('/emptyPens')
    public async getEmptyPens(@Body() data: IListEmptyPen): Promise<any>{
        try {
            const pens = await Pen.aggregate(penQueries.queryListEmptyPen(data));
            let lstRes = pens[0].items;
            let len = lstRes.length;
            for(let i = 0; i < len; i++){
                lstRes[i]['cameras'] = await CameraRepository.GetCameraByPenId(lstRes[i].id)
            }
            pens[0].items = lstRes;
            return successResponse(pens[0]);
        }
        catch (err) {
            this.setStatus(500);
            return failedResponse(`Error ${err}`, 'ServiceException');
        }
    }

    /**
     * Delete pen
     * @param penId
     */
    @Delete('/{penId}')
    public async removePen(penId: string): Promise<any>{
        try {
            const deletePen = await Pen.findById({ _id: penId });
            if (!deletePen) {
                this.setStatus(404);
                return failedResponse('Không tìm thấy dữ liệu', 'PenNotFound');
            }
    
            const pigs = await Pig.find({ penId: penId });
            if (pigs.length > 0) {
                this.setStatus(400);
                return failedResponse('Không thể xoá chuồng khi có cá thể', 'FailDelete');
            }
            await Pen.findOneAndDelete({ _id: penId });
    
            return successResponse(!!deletePen);
        }
        catch (err) {
            this.setStatus(500);
            return failedResponse(`Error ${err}`, 'ServiceException');
        }
    }

    /**
     * Get all pens of the farm
     */
    @Get('/allPens/{farmId}')
    public async getAllPensOfFarm (farmId: string, @Query() offset?: number, @Query() size?: number) : Promise<any>{
        try {
            const pens = await Pen.aggregate(listAllPensOfFarm(farmId, offset, size));
            return successResponse(pens[0]);
        }
        catch (err) {
            this.setStatus(500);
            return failedResponse(`Error ${err}`, 'ServiceException');
        }
    }
}

const isExistPenName = async (input: IPen, farmId: string): Promise<boolean> => {
    const { name } = input;

    return (await Pen.find({ name, farmId })).length > 0;
}
