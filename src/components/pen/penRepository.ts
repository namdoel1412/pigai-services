import Pen from "./pen.model";
import { IPenGetDTO } from "./pen.types";

class PenRepository{
    static GetPenById = async (penId: string): Promise<IPenGetDTO> => {
        try{
            const pens = await Pen.findOne({})
            .where('_id').equals(penId)
            .exec();
            if (pens == null){
                return null;
            }
            let penRes: IPenGetDTO = {
                id: pens?.id,
                name: pens?.name,
                capacity: pens.capacity,
                area: pens.area,
                farmId: pens.farmId,
                weightTypeId: pens.weightTypeId,
                note: pens.note,
            } 
            return penRes;
        }
        catch(err){
            return null;
        }
        
    }
}

export default PenRepository;