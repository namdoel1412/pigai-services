import { IPaginationCommon } from '../../types/common';

export interface IPen {
    name: string
    capacity?: number
    area?: number
    farmId: string
    weightTypeId?: string
    note?: string
}

export interface IPenGetDTO{
    id: string,
    name: string
    capacity?: number
    area?: number
    farmId: string
    weightTypeId?: string
    note?: string
}

export interface IUpdatePen {
    capacity?: number
    area?: number
    weightTypeId?: string
    note?: string
}

export interface IListPen extends IPaginationCommon {
    farmId: string
    name?: string
    min?: number
    max?: number
    weightTypeId?: string
    healthStatus?: IHealthStatus,
    eventTypeId?: string
    eventDate?: string
}

interface IHealthStatus{
    id: string,
    name: string
}

export interface IListEmptyPen extends IPaginationCommon{
    farmId: string
    name?: string
    area?: number
    weightTypeId?: string
}
