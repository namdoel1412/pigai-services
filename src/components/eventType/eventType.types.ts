export enum EEventType {
    pigIn = 'Nhập chuồng',
    pigOut = 'Xuất chuồng',
    changePen = 'Chuyển chuồng',
}

export interface IEventType {
    name: string
}
