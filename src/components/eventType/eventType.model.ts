import mongoose, { Model, Document } from 'mongoose';
import { IEventType } from './eventType.types';

interface EventTypeDocument extends IEventType, Document { }
interface EventTypeModel extends Model<EventTypeDocument> { }

const eventTypeSchema = new mongoose.Schema<EventTypeDocument, EventTypeModel>({
    name: {
        type: String,
        required: true
    },
})

eventTypeSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {   delete ret._id  }
});

export default mongoose.model<EventTypeDocument, EventTypeModel>('EventType', eventTypeSchema);
