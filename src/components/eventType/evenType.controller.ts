import EventType from './eventType.model';
import { IEventType } from './eventType.types';
import { failedResponse, successResponse } from '../../utils/http'
import { getAllEventTypeQuery } from './eventType.queries';
import {Body, Controller, Get, Post, Put, Query, Route, Tags} from 'tsoa';

@Route('eventTypes')
@Tags('EventType')
export class EventTypeController extends Controller{

    @Get('/allEvent?')
    public async getAllEvents(@Query() name?: string): Promise<any>{
        try {
            const result = await EventType.aggregate(getAllEventTypeQuery(name));
            return successResponse(result);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Create event type
     * @param data
     */
    @Post('/')
    public async create(@Body() data: IEventType): Promise<any>{
        try{
            if (await isExistEventType(data)) {
                this.setStatus(400);
                return failedResponse('Tên loại sự kiện đã tồn tại', 'UniqueName');
            }
            await new EventType(data).save();
            return successResponse('insertSuccess');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    /**
     * Update event type
     * @param eventTypeId
     * @param data
     */
    @Put('/{eventTypeId}')
    public async update(eventTypeId: string, @Body() data: IEventType): Promise<any>{
        try{
            if (await isExistEventType(data)) {
                this.setStatus(400);
                return failedResponse('Tên loại sự kiện đã tồn tại', 'UniqueName');
            }
            const eventType = await EventType.findByIdAndUpdate(eventTypeId, data);
            if (!eventType) {
                this.setStatus(404);
                return failedResponse('Không tìm thấy loại sự kiện', 'EventTypeNotFound');
            }
            return successResponse('updateSuccess');
        } catch (err) {
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

}

export const searchByArrayName = (name: string[]): any => EventType.find({ name: { $in: name } });

const isExistEventType = async (input: IEventType): Promise<boolean> => {
    const { name } = input;
    return (await EventType.find({ name })).length > 0;
}