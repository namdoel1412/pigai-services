export const getAllEventTypeQuery = (name: string): any => [
    {
        $match: {
            name: new RegExp(name, 'g'),
        }
    },
    {
        $sort: { name: 1 },
    },
    {
        $project: {
            _id: 0,
            id: '$_id',
            name: 1,
        }
    }
];

export const lookupObjectEventType = (collectionName: string, foreignName: string): any => ({
    $lookup: {
        from: collectionName,
        let: { [foreignName]: `$${ foreignName }` },
        as: collectionName,
        pipeline: [
            {
                $match: { $expr: { $in: [ '$_id', `$$${ foreignName }` ] } }
            },
            dataResponseProject
        ],
    }
});

const dataResponseProject = {
    $project: {
        _id: 0,
        id: '$_id',
        name: 1
    }
};
