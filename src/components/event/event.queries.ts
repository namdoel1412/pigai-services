import {EEventStatus, IListEvent} from './event.types';
import { DEFAULT_OFFSET_PAGINATION, DEFAULT_SIZE_PAGINATION } from '../../utils/constants';
import mongoose from 'mongoose';
import { lookupObjectPens } from '../pen/pen.queries';
import { lookupObjectPig } from '../pig/pig.queries';
import { lookupObjectFarms } from '../farm/farm.queries';
import exp from "constants";



const lookupEventType = [
    {
        $lookup: {
            from: 'eventtypes',
            let: { eventTypeId: '$eventTypeId' },
            as: 'eventType',
            pipeline: [
                {
                    $match: {
                        $expr: { $eq: [ '$_id', '$$eventTypeId' ] }
                    }
                },
                {
                    $project: {
                        _id: 0,
                        id: '$_id',
                        name: 1,
                    }
                }
            ]
        }
    },
    { $unwind: '$eventType' }
];

const lookupObject = (collectionName: string, foreignName: string): any => ({
    $lookup: {
        from: collectionName,
        let: { [foreignName]: `$${ foreignName }` },
        as: collectionName,
        pipeline: [
            {
                $match: { $expr: { $in: [ '$_id', `$$${ foreignName }` ] } }
            },
            {
                $project:{
                    id: '$_id',
                }
            }
        ],
    }
});

const dataResponseProject = {
    $project: {
        _id: 0,
        id: '$_id',
        eventType: '$eventType',
        farms: '$farms',
        pens: '$pens',
        pigs: '$pigs',
        name: 1,
        desc: 1,
        startDate: 1,
        endDate: 1,
        from: 1,
        to: 1,
    }
};

export const listEventQuery = (params: IListEvent, userId: string): Array<Record<string, any>> => {
    const { offset = DEFAULT_OFFSET_PAGINATION, size = DEFAULT_SIZE_PAGINATION } = params;

    return [
        {
            $match: {
                userId: { $eq: mongoose.Types.ObjectId(userId) }
            }
        },
        lookupObjectFarms('farms', 'farmIds'),
        lookupObjectPens('pens', 'penIds'),
        lookupObjectPig('pigs', 'pigIds'),
        ...lookupEventType,
        dataResponseProject,
        {
            $facet: {
                count: [ { $count: 'total' } ],
                items: [
                    { $skip: +offset },
                    { $limit: +size },
                ],
            },
        },
        {
            $project: {
                items: 1,
                total: {
                    $cond: {
                        if: { $eq: [ { $size: '$count' }, 0 ] },
                        then: 0,
                        else: { $arrayElemAt: [ '$count.total', 0 ] }
                    },
                },
            },
        },
    ]
};

export const detailEventQuery = (id: string, userId: string): Array<Record<string, any>> => [
    {
        $match: {
            _id: { $eq: mongoose.Types.ObjectId(id) },
            userId: { $eq: mongoose.Types.ObjectId(userId) },
        }
    },
    lookupObject('farms', 'farmIds'),
    lookupObject('pens', 'penIds'),
    lookupObject('pigs', 'pigIds'),
    ...lookupEventType,
    dataResponseProject,
]

export const detailEventByPigId = (pigId: mongoose.Types.ObjectId): Array<Record<string, any>> => [
    {
        $match: {
            pigIds: { $all: [pigId] }
        }
    },
    // lookupObject('farms', 'farmIds'),
    // lookupObject('pens', 'penIds'),
    // lookupObject('pigs', 'pigIds'),
    ...lookupEventType,
    dataResponseProject,
]


export const  detailEventQueryByFarmId = (farmId: string, data: IListEvent): Array<Record<string, any>> => [
    {
        $match: {
            farmIds: { $all: [mongoose.Types.ObjectId(farmId)]}
        },
    },
    lookupObjectFarms('farms', 'farmIds'),
    lookupObjectPens('pens', 'penIds'),
    lookupObjectPig('pigs', 'pigIds'),
    ...lookupEventType,
    dataResponseProject,
    {
        $facet: {
            count: [ { $count: 'total' } ],
            items: [
                { $skip: data.offset },
                { $limit: data.size },
            ],
        },
    },
    {
        $project: {
            items: 1,
            total: {
                $cond: {
                    if: { $eq: [ { $size: '$count' }, 0 ] },
                    then: 0,
                    else: { $arrayElemAt: [ '$count.total', 0 ] }
                },
            },
        },
    },
]

export const getEventByNameEventType = (pigId: mongoose.Types.ObjectId, eventType: string): Array<Record<string, any>> => [
    {
        $match: {
            pigIds: { $all: [pigId] },
        }
    },
    {
        $lookup: {
            from: 'eventtypes',
            let: { eventTypeId: '$eventTypeId' },
            pipeline: [
                {
                    $match: {
                        $expr: { $eq: [ '$_id', '$$eventTypeId' ] },
                        name: new RegExp(eventType, 'g'),
                    },
                },
                {
                    $project: {
                        _id: 0,
                        id: '$_id',
                        name: 1,
                    }
                }
            ],
            as: 'eventType',
        },
    },
    {
        $unwind: '$eventType',
    },
    {
        $project: {
            _id: 0,
            id: '$_id',
            name: 1,
            startDate: 1,
            endDate: 1,
            status: 1,
            eventType: '$eventType',
            userId: 1,
        }
    }
]