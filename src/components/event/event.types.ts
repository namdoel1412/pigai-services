import { IPaginationCommon } from '../../types/common';

export enum EEventStatus {
    Rejected = -1,
    Pending,
    Ongoing,
    Done,
}

export interface IEvent {
    name: string
    desc?: string
    startDate?: string
    endDate?: string
    eventTypeId: string
    penIds?: string[]
    pigIds?: string[]
    status?: EEventStatus
    from?: string
    to?: string
    userId: string
    farmIds: string[]
}

export interface IInsertEventWhenUpdatePigInput {
    penId: string
    farmId: string
    pigIds?: string[]
    dateIn?: string
    changePenPrediction?: string
    pigOutPrediction?: string
    userId: string
}

export interface IListEvent extends IPaginationCommon {
}

export interface IReqGetEventByFarmId{
    offset: number,
    size: number,
    farmId: string
}

export interface IReqGetLstEvent{
    offset: number,
    size: number,
    userId: string
}