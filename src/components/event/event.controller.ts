import Event from './event.model';
import { EEventStatus, IEvent, IInsertEventWhenUpdatePigInput, IListEvent, IReqGetEventByFarmId } from './event.types'
import { searchByArrayName } from '../eventType/evenType.controller'
import { EEventType } from '../eventType/eventType.types'
import { CustomRequest, CustomResponse, failedResponse, successResponse } from '../../utils/http'
import { NextFunction } from 'express'
import { detailEventQuery, detailEventQueryByFarmId, listEventQuery } from './event.queries';
import { Body, Controller, Delete, Get, Header, Post, Put, Query, Route, Tags } from 'tsoa';
import {convertFormatDate} from "../../utils/collection";


@Route('events')
@Tags('Event')
export class EventController extends Controller{

    @Post('')
    public async createEvent(@Body() data: IEvent): Promise<any>{
        try {
    
            if (!data.farmIds && !data.penIds && !data.pigIds) {
                this.setStatus(400);
                return failedResponse('IdsRequired', 'IdsRequired');
            }
            data.startDate = convertFormatDate(data.startDate);
            data.endDate = convertFormatDate(data.endDate);
            if (data.startDate.length === 0 || data.endDate.length === 0){
                return failedResponse('Không đúng định dạng dd/mm/yyyy hoặc dd-mm-yyyy hoặc yyyy-mm-dd', 'IncorrectDateFormat');
            }
    
            //data.userId = req.user.id;
            const result = await new Event(data).save();
            return successResponse(result);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    @Put('/{eventId}')
    public async updateEvent(eventId: string, @Body() data: IEvent): Promise<any>{
        try {
            if (!data.farmIds && !data.penIds && !data.pigIds) {
                this.setStatus(400);
                return failedResponse('IdsRequired', 'IdsRequired');
            }
            if (data.startDate) {
                data.startDate = convertFormatDate(data.startDate);
                if (data.startDate.length === 0){
                    return failedResponse('Không đúng định dạng dd/mm/yyyy hoặc dd-mm-yyyy hoặc yyyy-mm-dd', 'IncorrectDateFormat');
                }
            }
            if (data.endDate) {
                data.endDate = convertFormatDate(data.endDate);
                if (data.endDate.length === 0){
                    return failedResponse('Không đúng định dạng dd/mm/yyyy hoặc dd-mm-yyyy hoặc yyyy-mm-dd', 'IncorrectDateFormat');
                }
            }

            const event = await Event.updateOne({ _id: { $eq: eventId } }, data);
            return successResponse(event);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    @Post('/{eventId}')
    public async getEventDetail(eventId: string, @Body() data: IEvent): Promise<any>{
        try {
            const event = await Event.aggregate(detailEventQuery(eventId, data.userId));
            return successResponse(event);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    @Delete('/{eventId}')
    public async removeEvent(eventId: string): Promise<any>{
        try {
            await Event.deleteOne({ _id: { $eq: eventId } });
            return successResponse(true);
        } catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    @Post('/farmId/{farmId}')
    public async getLstEventsByFarmId(farmId: string, @Body() data: IListEvent): Promise<any>{
        try {
            // console.log(farmId);
            // if (!data.offset || !data.size) {
            //     this.setStatus(400);
            //     return failedResponse('Offset and size are required', 'OffsetRequired');
            // }
            //console.log(pagination);
            // const event = await Event.aggregate(detailEventQueryByFarmId(farmId));
            // return successResponse(event);
            const result = await Event.aggregate(detailEventQueryByFarmId(farmId, data));
            // console.log(result);
            return successResponse(result[0]);
        }
        catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    @Get('/')
    public async getLstEvents(@Query() offset?: number, @Query() size?: number, @Query() userId?: string): Promise<any>{
        try {
            var data: IListEvent = {
                offset: offset,
                size: size
            }
            const result = await Event.aggregate(listEventQuery(data, userId));
            return successResponse(result[0]);
        }
        catch (err) {
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }

    @Get('/updateDateInDB')
    public async updateDateInDB (): Promise<any>{
        try {
            const events = await Event.aggregate([
                { $match: {
                        $or: [
                            { startDate: { $regex: '/' }},
                            { endDate: { $regex: '/' }}
                        ]
                    }}
            ]);
            // console.log(events);
            for (let i = 0; i < events.length; i++){
                const startDate = convertFormatDate(events[i].startDate);
                const endDate = convertFormatDate(events[i].endDate);
                console.log(endDate)
                await Event.updateOne({_id: events[i]._id}, { startDate, endDate });
            }
            return successResponse('update success');
        } catch (err){
            this.setStatus(500);
            return failedResponse(`Error: ${err}`, 'ServiceException');
        }
    }
}


const fillValue = (event: any, startDate: string, endDate: string): any => {
    return {
        startDate: startDate,
        endDate: endDate,
        eventTypeId: event._id,
    }
}

// export const insertEventsWhenUpdatePigs = async (data: IInsertEventWhenUpdatePigInput): Promise<void> => {
//     const { pigIds, pigOutPrediction, changePenPrediction, penId, dateIn, userId, farmId } = data;
//     const evenTypes = await searchByArrayName([ EEventType.pigOutPrediction, EEventType.changePenPrediction, EEventType.pigIn ]);
//
//     const dataInsert: any = []
//
//     for (let i = 0; i < evenTypes.length; i++) {
//         let isValid = false;
//         const startDate = dateIn;
//         let endDate = dateIn;
//         let name = '';
//         let status;
//         const v = evenTypes[i];
//
//         if (v.name === EEventType.pigIn && dateIn) {
//             isValid = true;
//             name = v.name;
//             status = EEventStatus.Done;
//         }
//
//         if (v.name === EEventType.pigOutPrediction && pigOutPrediction) {
//             isValid = true;
//             endDate = pigOutPrediction;
//             name = v.name;
//             status = EEventStatus.Pending;
//         }
//
//         if (v.name === EEventType.changePenPrediction && changePenPrediction) {
//             isValid = true;
//             endDate = changePenPrediction;
//             name = v.name;
//             status = EEventStatus.Pending;
//         }
//         if (!isValid) continue;
//
//         dataInsert.push({ name, pigIds, userId, status, farmIds: farmId, penIds: [ penId ], ...fillValue(v, startDate, endDate) })
//     }
//
//     Event.insertMany(dataInsert).catch(err => console.log(err));
// }
