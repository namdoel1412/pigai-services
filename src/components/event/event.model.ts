import mongoose, { Model, Document } from 'mongoose';
import { IEvent } from './event.types';

export interface EventDocument extends IEvent, Document { }
interface EventModel extends Model<EventDocument> { }

const eventSchema = new mongoose.Schema<EventDocument, EventModel>({
    name: String,
    desc: String,
    startDate: String,
    endDate: String,
    eventTypeId: {
        type: mongoose.Types.ObjectId,
        ref: 'EventType',
        required: true
    },
    penIds: [ {
        type: mongoose.Types.ObjectId,
        ref: 'Pen',
        required: false,
        default: []
    } ],
    pigIds: [ {
        type: mongoose.Types.ObjectId,
        ref: 'Pig',
        required: true,
        default: []
    } ],
    from: {
        type: mongoose.Types.ObjectId,
        ref: 'Pen',
    },
    to: {
        type: mongoose.Types.ObjectId,
        ref: 'Pen',
    },
    status: Number, // -1 rejected, 0 pending, 1 ongoing, 2 done,
    userId: {
        type: mongoose.Types.ObjectId,
        ref: 'User'
    },
    farmIds: [
        {
            type: mongoose.Types.ObjectId,
            ref: 'Farm',
            default: []
        }
    ],
})

eventSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {   delete ret._id  }
});

export default mongoose.model<EventDocument, EventModel>('Event', eventSchema);
