import mongoose, { Model, Document } from 'mongoose';
import { IBehaviour } from './behaviour.types';

export interface BehaviourDocument extends IBehaviour, Document { }
interface BehaviourModel extends Model<BehaviourDocument> { }

const behaviourSchema = new mongoose.Schema<BehaviourDocument, BehaviourModel>({
    unspecified: { type: Number, default: 0, require: false},
    sleepingTimes: { type: Number, default: 0, require: false },
    eatingTimes: { type: Number, default: 0, require: false },
    lieDownTimes: { type: Number, default: 0, require: false },
    standingTimes: { type: Number, default: 0, require: false },
    runningTimes: { type: Number, default: 0, require: false },
    pigAIId: {
        type: String,
        required: true
    },
    farmId:{
        type: mongoose.Types.ObjectId,
        ref: 'Farm',
        require: true
    },
    penId:{
        type: mongoose.Types.ObjectId,
        ref: 'Pen',
        require: true
    },
    startTime: { type: Date, require: true },
    endTime: { type: Date, require: true }
})

behaviourSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {   delete ret._id  }
});

export default mongoose.model<BehaviourDocument, BehaviourModel>('Behaviour', behaviourSchema);
