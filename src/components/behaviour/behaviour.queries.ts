import mongoose, { Model, Document } from 'mongoose';
import { IBehaviourPagination } from './behaviour.types';

export const detailBehaviourQueryByPigAIId = (pigAIId: string, penId: string, startTime: Date, endTime: Date): Array<Record<string, any>> => [
    {
        $match: {
            pigAIId: { $eq: pigAIId },
            penId: { $eq: mongoose.Types.ObjectId(penId) },
            startTime: { $gte: startTime },
            endTime: { $lte: endTime }
        }
    },
    dataResponseProject,
    // {
    //     $facet: {
    //         count: [ { $count: 'total' } ],
    //         items: [
    //             { $skip: 0 },
    //             { $limit: 10 },
    //         ],
    //     },
    // },
    // {
    //     $project: {
    //         items: 1,
    //         total: {
    //             $cond: {
    //                 if: { $eq: [ { $size: '$count' }, 0 ] },
    //                 then: 0,
    //                 else: { $arrayElemAt: [ '$count.total', 0 ] }
    //             },
    //         },
    //     },
    // },
]

const dataResponseProject = {
    $project: {
        _id: 0,
        id: '$_id',
        farmId: 1,
        penId: 1,
        startTime: 1,
        endTime: 1,
        sleepingTimes: 1,
        eatingTimes: 1,
        lieDownTimes: 1,
        standingTimes: 1,
        runningTimes: 1,
        unspecified: 1,
        pigAIId: 1
    }
};

export const behaviourStatisticQueryByPen = (penId: string, startTime: Date, endTime: Date): Array<Record<string, any>> => [
    {
        $match: {
            penId: { $eq: mongoose.Types.ObjectId(penId) },
            startTime: { $gte: startTime },
            endTime: { $lte: endTime }
        }
    },
    dataResponseProject
]

