import { IPaginationCommon } from "../../types/common";

export interface IBehaviour {
    unspecified?: number,
    sleepingTimes?: number,
    eatingTimes?: number,
    lieDownTimes?: number,
    standingTimes?: number,
    runningTimes?: number,
    pigAIId: string,
    penId: string,
    farmId: string,
    startTime: Date,
    endTime: Date
}

export interface IBehaviourCreateDTO {
    unspecified?: number,
    sleepingTimes?: number,
    eatingTimes?: number,
    lieDownTimes?: number,
    standingTimes?: number,
    runningTimes?: number,
    pigAIId: string,
    penId: string,
    startTime: Date,
    endTime: Date
}

export interface BehaviourGetDTO {
    unspecified?: number,
    sleepingTimes?: number
    eatingTimes?: number,
    lieDownTimes?: number,
    standingTimes?: number,
    runningTimes?: number,
    pigAIId: string,
    penId: string,
    date: string
}

export interface BehaviourForPenGetDTO {
    unspecified?: number,
    sleepingTimes?: number
    eatingTimes?: number,
    lieDownTimes?: number,
    standingTimes?: number,
    runningTimes?: number,
    penId: string,
    date: string
}

export interface BehaviourPeriod{
    startDate: Date,
    endTime: Date
}

export interface IUpdateBehaviour {
    pigAIId?: string,
    penId?: string,
    farmId?: string
    startTime?: Date,
    endTime?: Date,
    sleepingTimes?: number
    eatingTimes?: number,
    lieDownTimes?: number,
    standingTimes?: number,
    runningTimes?: number
}


// export interface IUpdateFarm{
//     name: string
//     address: string
//     ownerId: string
// }

export interface IBehaviourPagination extends IPaginationCommon{}
