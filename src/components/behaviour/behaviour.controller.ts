import Behaviour from './behaviour.model';
import { successResponse, failedResponse } from '../../utils/http';
import { NextFunction } from 'express';
import { BehaviourForPenGetDTO, BehaviourGetDTO, IBehaviour, IBehaviourCreateDTO, IBehaviourPagination, IUpdateBehaviour } from './behaviour.types';
import { Controller, Route, Get, Post, BodyProp, Put, Delete, Tags, Body, Security, Header, Query } from 'tsoa';
import { authentication } from '../../middleware/authentication';
import { behaviourStatisticQueryByPen, detailBehaviourQueryByPigAIId } from './behaviour.queries';
import ExtendedService from '../../utils/extended-services';
import PenRepository from './../pen/penRepository';
import { cloneDeep, groupBy } from 'lodash';

@Route('/behaviours')
@Tags('Behaviour')
export class BehaviourController extends Controller{
    /**
     * Create farm
     */
    // @Security("jwt")

    @Post()
    public async createBehaviour(@Body() data: IBehaviourCreateDTO[]): Promise<any>{
        const session = await Behaviour.startSession();
        try {
            session.startTransaction();
            if (data == null) {
                return failedResponse('PigIdsRequired', 'PigIdsRequired');
            }
            let res: any = [];
            // using for let of to synchronize session
            for(let item of data){
                let pen = await PenRepository.GetPenById(item.penId)
                if (pen == null){
                    this.setStatus(400);
                    await session.abortTransaction();
                    return failedResponse('Pen not found', 'PenNotFound');
                }
                let subData: IBehaviour = {
                    ...item,
                    farmId: pen.farmId
                }
                const subRes = await Behaviour.create(subData);
                res.push(subRes);
            }
            await session.commitTransaction();
            return successResponse("InsertSuccess");
        } catch (err) {
            await session.abortTransaction();
            this.setStatus(500);
            return failedResponse('Execute service went wrong', 'ServiceException');
        }
    }

    /**
     * Get farm's detail by farmId
     */
    // @Security("jwt")
    @Get('/{behaviourId}')
    public async getBehaviourByBehaviourId(behaviourId: string): Promise<any>{
        try {
            const result = await Behaviour.find({})
            .where('_id').equals(behaviourId)
            .exec();
            return successResponse(result);
        }
        catch (err) {
            this.setStatus(500);
            return failedResponse(err, 'ServiceException');
        }
    }

    /**
     * Get farms by owner id.
     */
    // @Security("jwt")
    @Get('pigAIId/{pigAIId}')
    public async getAllBehavioursByPigId(pigAIId: string, @Query() penId: string, @Query() startDate: Date, @Query() endDate: Date): Promise<any>{
        try {
            const result = await Behaviour.aggregate(detailBehaviourQueryByPigAIId(pigAIId, penId, startDate, endDate));
            // console.log(result);
            // console.log(startDate.toISOString().slice(0, 10).split("-").join(""));
            // console.log(endDate.toISOString());
            // const numStartDate = parseInt(startDate.toISOString().slice(0, 10).split("-").join(""));
            // const numEndDate = parseInt(endDate.toISOString().slice(0, 10).split("-").join(""));
            // console.log(numStartDate);
            // console.log(numEndDate);
            let map = new Map<string, BehaviourGetDTO>();
            let k = 0;
            let res: BehaviourGetDTO[] = [];
            for(let i = startDate; i <= endDate; i.setDate(i.getDate() + 1)){
                const preDate = ExtendedService.convertDateTimeToyyyyMMdd(i);
                const tmp: BehaviourGetDTO = {
                    penId: penId,
                    pigAIId: pigAIId,
                    eatingTimes: 0,
                    lieDownTimes: 0,
                    sleepingTimes: 0,
                    standingTimes: 0,
                    runningTimes: 0,
                    unspecified: 0,
                    date: preDate
                };
                map.set(preDate, tmp);
            }
            console.log(map);
            console.log(result);
            if(result.length > 0){
                result.forEach(item => {
                    console.log(item)
                    const indx = item.startTime.toISOString().slice(0, 10);
                    let brid = map.get(indx);
                    item.lieDownTimes && (brid.lieDownTimes += item?.lieDownTimes);
                    item.sleepingTimes && (brid.sleepingTimes += item?.sleepingTimes);
                    item.eatingTimes && (brid.eatingTimes += item?.eatingTimes);
                    item.standingTimes && (brid.standingTimes += item?.standingTimes);
                    item.runningTimes && (brid.runningTimes += item?.runningTimes);
                    item.unspecified && (brid.unspecified += item.unspecified);
                    map.set(indx, brid);
                })
            }
            for (let value of map.values()){
                console.log(value);
                res.push(value);
            }
            // for(let i = numStartDate; i <= numEndDate; i++){
            //     res.push(map.get(i));
            // }
            return successResponse(res);
        }
        catch (err) {
            this.setStatus(500);
            console.log(err)
            return failedResponse('Something went wrong', 'ServiceException');
        }
    }

    /**
     * Behaviour Statistic.
     */
    @Get('/statistic/{penId}')
    public async getAllBehaviourStatisticForPen(penId: string, @Query() startDate: Date, @Query() endDate: Date): Promise<any>{
        try {
            const result = await Behaviour.aggregate(behaviourStatisticQueryByPen(penId, startDate, endDate));
            let dic = groupBy(result, x => x.pigAIId);
            const numberOfSpecies = Object.keys(dic).length;
            let map = new Map<string, BehaviourForPenGetDTO>();
            let res: BehaviourForPenGetDTO[] = [];
            for(let i = startDate; i <= endDate; i.setDate(i.getDate() + 1)){
                const preDate = ExtendedService.convertDateTimeToyyyyMMdd(i);
                const tmp: BehaviourForPenGetDTO = {
                    penId: penId,
                    eatingTimes: 0,
                    lieDownTimes: 0,
                    sleepingTimes: 0,
                    standingTimes: 0,
                    runningTimes: 0,
                    unspecified: 0,
                    date: preDate
                };
                map.set(preDate, tmp);
            }
            console.log(map);
            console.log(result);
            if(result.length > 0){
                result.forEach(item => {
                    console.log(item)
                    const indx = item.startTime.toISOString().slice(0, 10);
                    let brid = map.get(indx);
                    item.lieDownTimes && (brid.lieDownTimes += item?.lieDownTimes);
                    item.sleepingTimes && (brid.sleepingTimes += item?.sleepingTimes);
                    item.eatingTimes && (brid.eatingTimes += item?.eatingTimes);
                    item.standingTimes && (brid.standingTimes += item?.standingTimes);
                    item.runningTimes && (brid.runningTimes += item?.runningTimes);
                    item.unspecified && (brid.unspecified += item.unspecified);
                    map.set(indx, brid);
                })
            }
            for (let value of map.values()){
                console.log(value);
                value.lieDownTimes = value.lieDownTimes/numberOfSpecies;
                value.sleepingTimes = value.sleepingTimes/numberOfSpecies;
                value.eatingTimes = value.eatingTimes/numberOfSpecies;
                value.standingTimes = value.standingTimes/numberOfSpecies;
                value.runningTimes = value.runningTimes/numberOfSpecies;
                value.unspecified = value.unspecified/numberOfSpecies;
                res.push(value);
            }
            // for(let i = numStartDate; i <= numEndDate; i++){
            //     res.push(map.get(i));
            // }
            return successResponse(res);
        }
        catch (err) {
            this.setStatus(500);
            console.log(err)
            return failedResponse('Something went wrong', 'ServiceException');
        }
    }

    /**
     * Update farm
     */
    // @Security("jwt")
    @Put('/{behaviourId}')
    public async updateBehaviour(behaviourId: string, @Body() data: IUpdateBehaviour): Promise<any>{
        try {
            if (!data.sleepingTimes && !data.eatingTimes && !data.lieDownTimes && !data.standingTimes && !data.runningTimes) {
                this.setStatus(400);
                return failedResponse('BodyRequired', 'BodyRequired');
            }
            const event = await Behaviour.updateOne({ _id: { $eq: behaviourId } }, data);
            return successResponse(event);
        }
        catch (err) {
            this.setStatus(500);
            return failedResponse('Something went wrong', 'ServiceException');
        }
    }

    /**
     * Delete farm
     */
    // @Security("jwt")
    @Delete('/{behaviourId}')
    public async removeBehaviour(behaviourId: string): Promise<any>{
        try {
            await Behaviour.deleteOne({ _id: { $eq: behaviourId } });
            return successResponse(true);
        } catch (err) {
            this.setStatus(500);
            return failedResponse('Something went wrong', 'ServiceException');
        }
    }
}
