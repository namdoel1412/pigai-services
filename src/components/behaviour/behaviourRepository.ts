import { BehaviourGetDTO } from "./behaviour.types";
import ExtendedService from '../../utils/extended-services';
import { detailBehaviourQueryByPigAIId } from "./behaviour.queries";
import Behaviour from './behaviour.model';

class BehaviourRepository{
    static GetBehaviourByPenId = async (pigAIId: string, penId: string, startDate: Date, endDate: Date): Promise<any> => {
        const result = await Behaviour.aggregate(detailBehaviourQueryByPigAIId(pigAIId, penId, startDate, endDate));
        let map = new Map<string, BehaviourGetDTO>();
        let k = 0;
        let res: BehaviourGetDTO[] = [];
        for(let i = startDate; i <= endDate; i.setDate(i.getDate() + 1)){
            const preDate = ExtendedService.convertDateTimeToyyyyMMdd(i);
            const tmp: BehaviourGetDTO = {
                penId: penId,
                pigAIId: pigAIId,
                eatingTimes: 0,
                lieDownTimes: 0,
                sleepingTimes: 0,
                standingTimes: 0,
                runningTimes: 0,
                unspecified: 0,
                date: preDate
            };
            map.set(preDate, tmp);
        }
        console.log(map);
        console.log(result);
        if(result.length > 0){
            result.forEach(item => {
                console.log(item)
                const indx = item.startTime.toISOString().slice(0, 10);
                let brid = map.get(indx);
                item.lieDownTimes && (brid.lieDownTimes += item?.lieDownTimes);
                item.sleepingTimes && (brid.sleepingTimes += item?.sleepingTimes);
                item.eatingTimes && (brid.eatingTimes += item?.eatingTimes);
                item.standingTimes && (brid.standingTimes += item?.standingTimes);
                item.runningTimes && (brid.runningTimes += item?.runningTimes);
                item.unspecified && (brid.unspecified += item.unspecified);
                map.set(indx, brid);
            })
        }
        for (let value of map.values()){
            console.log(value);
            res.push(value);
        }
        return res;
    }
}

export default BehaviourRepository;