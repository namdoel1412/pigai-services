import { IUser } from '@type/user';

export interface IEmail {
    to: string
    from: string
    subject: string
    text: string
    html: any
}

export type TConfigEmail = Omit<IEmail, 'from'>;

export interface IMailTemplate<IMail> {
    dir: string
    fields: IMail
}

export interface IRegisterMail {
    fullName: string
    messageRegister: string
    urlActiveAccount: string
}

export interface IConfigEmailData<MailContent = any> {
    user: IUser
    subject: string
    mailDir: string
    mailContent: MailContent
}

export interface IResetPasswordMail {
    fullName: string
    urlResetPassword: string
}

export interface ISendMailNotification {
    fullName: string
    messages: string
    approvedAt: string
}
