export enum UserActiveStatus {
    DEACTIVATED = -1,
    INACTIVATED = 0,
    ACTIVATED = 1
}

export interface IUser {
    username: string
    email: string
    password: string
    fullName: string
    avatar?: string
    active: UserActiveStatus
    activeCode: string
    phone?: string
    createTime?: number
    updateTime?: number,
    defaultFarm?: any
}

export interface IUserResponse extends Omit<IUser, 'password' | 'activeCode'> {
    id: string
    accessToken?: string
    refreshToken?: string
}
