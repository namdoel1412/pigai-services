declare namespace Express {
    export interface Request {
        messageError: string
        user: import('@type/user').IUserResponse
    }
    namespace Multer {
        interface File {
            urlThumbnailFile: string
        }
    }
}
