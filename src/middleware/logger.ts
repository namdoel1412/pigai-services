import { Request } from 'express';
import morgan from 'morgan';
import path from 'path';
import fs from 'fs';

interface LoggerRequest extends Request {
    messageError: string
}

const currentDate = new Date().toISOString().slice(0, 10);
const dir = `${ process.cwd() }/logs`;
const fileName = `${ currentDate }.log`;

if (!fs.existsSync(dir)) fs.mkdirSync(dir);

morgan.token('message', (req: LoggerRequest, _) => req.messageError);

const logger = morgan(':date :status :method :url :message', {
    stream: fs.createWriteStream(path.join(dir, fileName), { flags: 'a' }),
});

export default logger;
