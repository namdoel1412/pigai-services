import { NextFunction, Response } from 'express';
import { validationResult } from 'express-validator';
import { failedResponse, FailedResponseType, CustomRequest } from '../utils/http';

interface ErrorValidationType { [key: string]: string[] }

export const handleValidationError = (req: CustomRequest, res: Response<FailedResponseType<ErrorValidationType>>, next: NextFunction): void => {
    const errors = validationResult(req);
    if (errors.isEmpty()) return next();

    const extractedErrors: ErrorValidationType = {};
    errors.array().map(err => { if (!(err.param in extractedErrors)) extractedErrors[err.param] = err.msg });

    res.json(failedResponse(extractedErrors, 'RequiredField'))
};
