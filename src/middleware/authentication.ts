import { Request, Response, NextFunction } from 'express';
import passport from 'passport';
import { failedResponse } from '../utils/http';

export const authentication = (req: Request, res: Response, next: NextFunction): void => {
    return passport.authenticate('authentication', { session: false }, async (error, user) => {
        if (error || !user) return res.json(failedResponse('Token không hợp lệ', '401'));

        req.user = user;

        return next();
    })(req, res, next)
};

// export const authen02 = () : any => {
//     return passport.authenticate('authentication', { session: false }, async (error, user) => {
//         if (error || !user) return failedResponse('Token không hợp lệ', '401');
//         return true;
//     })
// }
