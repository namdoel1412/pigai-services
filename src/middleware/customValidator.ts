// import { check, param } from 'express-validator';
import { multerImageType, multerVideoType } from '../utils/collection';
import mongoose from 'mongoose';
import { param } from 'express-validator';

export enum MediaValidationRequest {
    IMAGE,
    VIDEO,
    ALL
}

// export const mediaValidator = (fieldName: string, type: MediaValidationRequest = MediaValidationRequest.ALL): any => {
//     return check(fieldName).custom((value, { req }) => {
//         const files = req.file ? [ req.file ] : req.files;
//         let isValidUpload = true;
//         let validationType;

//         if (!files) return true;

//         switch (type) {
//             case MediaValidationRequest.IMAGE:
//                 validationType = multerImageType;
//                 break;
//             case MediaValidationRequest.VIDEO:
//                 validationType = multerVideoType;
//                 break;
//             default:
//                 validationType = [ ...multerImageType, ...multerVideoType ];
//                 break;
//         }

//         for (let i = 0; i < files.length; i++) {
//             const file = files[i];
//             if (!validationType.includes(file.mimetype) && isValidUpload) isValidUpload = false;

//             if (!isValidUpload) break
//         }

//         if (!isValidUpload) {
//             removeFilesFromStorage(files);
//             throw new Error(`Chỉ chập thuận những file ${ validationType.join(', ') }`);
//         }

//         return true;
//     })
// }

export const validationIdParamRequest = [
    param('id').custom(value => {
        if (!mongoose.Types.ObjectId.isValid(value)) throw new Error('ID không hơp lệ')

        return true;
    }),
];
