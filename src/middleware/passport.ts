import passport from 'passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import User from '../components/user/user.model';
import { pick } from '../utils/collection';

const pickedLoginUserFields = [ 'email', 'active', 'fullName', 'phone', 'createTime', 'updateTime', 'accessToken', 'avatar' ];

passport.use('authentication', new Strategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.JWT_SECRET
}, async (jwtPayload, cb) => {
    try {
        const { id } = jwtPayload;
        const user = await User.findById(id);

        if (!user) return cb(null, false);

        return cb(null, { id, ...pick(user.toJSON(), pickedLoginUserFields) });
    } catch (e) {
        return cb(null, false)
    }
}));
