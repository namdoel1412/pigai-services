import { Request, Response, NextFunction } from 'express';
import * as utils from '../utils/collection';

const handleError = (err: any, req: Request, res: Response, next: NextFunction): void => {
    if (err.stack && err.message) {
        req.messageError = err;
    } else {
        req.messageError = JSON.stringify(err);
    }

    console.log(err);

    res.json(utils.commonFailedResponse(err));
    next();
};

export default handleError;
