class ExtendedService {
    static convertYYYYMMDDToDateString(value: number): string{
        var req = value.toString();
        console.log(req)
        var res = [req.slice(0, 4), "-", req.slice(4, 6), "-", req.slice(6, 8)].join('');
        return res;
    }

    static convertDateTimeToyyyyMMdd(value: Date): string{
        var d = value,
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
    
        return [year, month, day].join('-');
    }
}

export default ExtendedService