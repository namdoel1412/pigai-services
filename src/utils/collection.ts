import { failedResponse, FailedResponseType } from './http';
import fs from 'fs';

interface IFailedResponse {
    message: string
    statusCode: string
}

const translateFields: { [ key: string ]: string } = {
    name: 'Tên',
    email: 'Email',
    serialId: 'SerialId'
};

export const multerImageType = [ 'image/png', 'image/jpg', 'image/jpeg' ];
export const multerVideoType = [ 'video/mp3', 'video/mp4', 'video/mov' ];
export const imageType = [ 'jpg', 'jpeg', 'gif', 'png' ];
export const videoType = [ 'mp3', 'mp4', 'mov' ];

const getFailedMessage = (error: any): string => {
    let errorMessage = 'Đã xảy ra lỗi';
    if (typeof error.message === 'string' && !error.message.includes(error.code)) errorMessage = error.message;

    // console.log(error);

    if (error.keyValue) {
        const keyValue = Object.keys(error.keyValue)[0];

        if (error.code === 11000) errorMessage = `${ translateFields[keyValue] } đã tồn tại`; // 11000 = mongoose unique error code
    }

    return capitalizeFirstLetter(errorMessage.trim());
};

const isValidJsonToParse = (string: string): boolean => {
    try {
        JSON.parse(string);
    } catch {
        return false
    }

    return true;
}

export const commonFailedResponse = (error: any): FailedResponseType<string> | any => {
    if (error.message && isValidJsonToParse(error.message)) {
        return JSON.parse(error.message);
    }

    const errorMessage = getFailedMessage(error);

    return failedResponse(errorMessage, error.codeName || 'Error')
};

export const throwErrorMessage = (message: IFailedResponse): void => {
    throw new Error(JSON.stringify(message));
}

export const capitalizeFirstLetter = (string: string): string => {
    return string.charAt(0).toUpperCase() + string.slice(1);
};

export const mix = <FirstObject, SecondObject>(first: FirstObject, second: SecondObject): FirstObject & SecondObject => {
    const result: Partial<FirstObject & SecondObject> = { ...first, ...second };

    return result as FirstObject & SecondObject;
};

export const pick = <Obj>(data: { [ key: string ]: any }, pickedFields: string[]): Obj => {
    const result: { [ key: string ]: any } = {};

    for (const field of pickedFields) {
        if (field in data) {
            result[field] = data[field];
        }
    }

    return result as Obj;
};

export const omit = <Obj>(data: { [ key: string ]: any }, hiddenFields: string[]): Obj => {
    const result: { [ key: string ]: any } = {};

    for (const field in data) {
        if (!hiddenFields.includes(field)) result[field] = data[field];
    }

    return result as Obj;
};

export const removeFileFromStorage = (filePath: string): void => {
    if (fs.existsSync(filePath)) fs.unlinkSync(filePath);
}

export const fileType = (file: any): string => {
    return imageType.includes(file.type) ? 'image' : 'video';
};

export const getStringAfterLastSlash = (str: string): string => {
    const splitStr = str.split('/');
    const n = splitStr.length === 1 ? str.lastIndexOf('\\') : str.lastIndexOf('/');

    return str.substring(n + 1);
}

export const convertFormatDate = (data: string): string => {
    if (data.substr(2, 1) === '/' && data.substr(5, 1) === '/'){
        return data.substr(6,4) + '-' + data.substr(3,2) + '-' + data.substr(0,2)
    }
    if (data.substr(2, 1) === '-' && data.substr(5, 1) === '-'){
        return data.substr(6,4) + '-' + data.substr(3,2) + '-' + data.substr(0,2)
    }
    if (data.substr(4, 1) === '-' && data.substr(7, 1) === '-') {
        return data;
    }
    return '';
}
