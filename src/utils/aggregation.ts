export const lookupSingleObjectFoodTypeAggregation = (): any => (
    [
        {
            $lookup: {
                from: 'foodtypes',
                let: {foodTypeId: '$foodTypeId'},
                as: 'foodtype',
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$_id', '$$foodTypeId']}
                        }
                    },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            name: 1
                        }
                    }
                ]
            }
        },
        {
            $unwind: {
                path: '$foodtype',
                preserveNullAndEmptyArrays: true
            }
        }
    ]
);

export const lookupSingleObjectFoodSupplier = (): any => (
    [
        {
            $lookup: {
                from: 'foodsuppliers',
                let: {foodSupplierId: '$foodSupplierId'},
                as: 'foodsupplier',
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$_id', '$$foodSupplierId']}
                        }
                    },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            name: 1,
                            address: 1,
                            farmId: 1
                        }
                    }
                ]
            }
        },
        {
            $unwind: {
                path: '$foodsupplier',
                preserveNullAndEmptyArrays: true
            }
        }
    ]
);

export const lookupSingleObjectFoodUnitAggregation = (): any => (
    [
        {
            $lookup: {
                from: 'foodunits',
                let: {foodUnitId: '$foodUnitId'},
                as: 'foodunit',
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$_id', '$$foodUnitId']}
                        }
                    },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            name: 1
                        }
                    }
                ]
            }
        },
        {
            $unwind: {
                path: '$foodunit',
                preserveNullAndEmptyArrays: true
            }
        }
    ]
);

export const lookupSingleObjectFoodAggregation = (): any => (
    [
        {
            $lookup: {
                from: 'foods',
                let: {foodId: '$foodId'},
                as: 'food',
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$_id', '$$foodId']}
                        }
                    },
                    ...lookupSingleObjectFoodTypeAggregation(),
                    ...lookupSingleObjectFoodUnitAggregation(),
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            name: 1,
                            foodUnit: '$foodunit',
                            foodType: '$foodtype'
                        },
                    }
                ]
            }
        },
        {
            $unwind: {
                path: '$food',
                preserveNullAndEmptyArrays: true
            }
        }
    ]
);

export const lookupSingleObjectFarm = (): any => (
    [
        {
            $lookup: {
                from: 'farms',
                let: {farmId: '$farmId'},
                as: 'farm',
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$_id', '$$farmId']}
                        }
                    },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            name: 1,
                            address: 1
                        }
                    }
                ]
            }
        },
        {
            $unwind: {
                path: '$farm',
                preserveNullAndEmptyArrays: true
            }
        }
    ]
);

export const lookupSingleObjectUser = (): any => (
    [
        {
            $lookup: {
                from: 'users',
                let: {userId: '$userId'},
                as: 'user',
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$_id', '$$userId']}
                        }
                    },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            username: 1,
                            email: 1,
                            fullName: 1,
                            avatar: 1,
                            phone: 1,
                            createTime: 1,
                            updateTime: 1
                        }
                    }
                ]
            }
        },
        {
            $unwind: {
                path: '$user',
                preserveNullAndEmptyArrays: true
            }
        }
    ]
);

export const lookupSingleObjectPen = (): any => (
    [
        {
            $lookup: {
                from: 'pens',
                let: { penId: '$penId' },
                as: 'pen',
                pipeline: [
                    {
                        $match: {
                            $expr: { $eq: [ '$_id', '$$penId' ] }
                        }
                    },
                    dataResponseProjectPen
                ]
            }
        },
        { 
            $unwind: {
                path: '$pen',
                preserveNullAndEmptyArrays: true
            } 
        }
    ]
);


const dataResponseProjectPen = {
    $project: {
        _id: 0,
        id: '$_id',
        name: 1,
        capacity: 1,
        area: 1,
        farmId: 1,
        weightTypeId: 1,
        note: 1
    }
};

export const lookupSingleObjectFoodConsume = (): any => (
    [
        {
            $lookup: {
                from: 'foodconsumes',
                let: {foodConsumeId: '$foodConsumeId'},
                as: 'foodConsume',
                pipeline: [
                    {
                        $match: {
                            $expr: {$eq: ['$_id', '$$foodConsumeId']}
                        }
                    },
                    {
                        $project: {
                            id: '$_id',
                            _id: 0,
                            name: 1,
                            date: 1,
                            farmId: 1,
                            userId: 1
                        }
                    }
                ]
            }
        },
        {
            $unwind: {
                path: '$foodConsume',
                preserveNullAndEmptyArrays: true
            }
        }
    ]
);